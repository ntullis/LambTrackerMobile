package com.weyr_associates.lambtracker;

import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class SheepManagementMenu extends Activity {
	public Button btn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sheep_management_menu);
		// make the sort sheep button normal and disabled
		btn = (Button) findViewById( R.id.sort_sheep_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
		btn.setEnabled(false);
		// make the group sheep management button normal and disabled
		btn = (Button) findViewById( R.id.group_sheep_management_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
		btn.setEnabled(false);
	}
	
	public void sheepManagement (View v){
		Intent i = null;
		i = new Intent(SheepManagementMenu.this, SheepManagement.class);
		SheepManagementMenu.this.startActivity(i);
	}
	public void lambingSheep (View v){
		Intent i = null;
		i = new Intent(SheepManagementMenu.this, LambingSheep.class);
		SheepManagementMenu.this.startActivity(i);
	} 
	public void groupSheepManagement (View v){
		Intent i = null;
		i = new Intent(SheepManagementMenu.this, GroupSheepManagement.class);
		SheepManagementMenu.this.startActivity(i);
	}
	public void drawBlood (View v){
		Intent i = null;
		i = new Intent(SheepManagementMenu.this, DrawBlood.class);
		SheepManagementMenu.this.startActivity(i);
	}
	public void sortSheep (View v){
		Intent i = null;
		i = new Intent(SheepManagementMenu.this, SortSheep.class);
		SheepManagementMenu.this.startActivity(i);
	}
	public void evaluateSheep (View v){
		Intent i = null;
		i = new Intent(SheepManagementMenu.this, EvaluateSheep2.class);
		SheepManagementMenu.this.startActivity(i);
	}
	public void removeSheep (View v){
		Intent i = null;
		i = new Intent(SheepManagementMenu.this, RemoveSheep.class);
		SheepManagementMenu.this.startActivity(i);
	}
	public void sellSheep (View v){
		Intent i = null;
		i = new Intent(SheepManagementMenu.this, SellSheep.class);
		SheepManagementMenu.this.startActivity(i);
	}
	public void removeDrug (View v){
		Intent i = null;
		i = new Intent(SheepManagementMenu.this, RemoveDrug.class);
		SheepManagementMenu.this.startActivity(i);
	}
}
