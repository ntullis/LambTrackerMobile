package com.weyr_associates.lambtracker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.AlertDialog;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.text.TextUtils;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class SellSheep extends ListActivity  {

    private DatabaseHandler dbh;
    public Cursor 	cursor, cursor2;
    public Object	crsr, crsr2;
    public int 		nRecs, tempint, nrecs2, numsheep ;
    public String mytoday;
    public Spinner seller_spinner, buyer_spinner, breed_spinner, transfer_reason_spinner, units_spinner;
    public int which_remove_reason, which_seller, which_buyer, which_units, which_breed;
    public List<String> breed_name, sheep_breed_id_list, sheep_breed_display_order, sell_units, sell_units_id_order;
    public List<String> sell_units_display_order;
    public List<String> transfer_reasons, transfer_reasons_id_order, transfer_reasons_display_order;
    public int 		thissheep_id;
    public Float 	sell_price = 0.0f;
    String     	cmd;
    Button button;
    public Button btn;
    public SimpleCursorAdapter myadapter;
    ArrayAdapter<String> dataAdapter;
    private TextView Output;
    private Button changeDate;
    static final int DATE_PICKER_ID = 1111;
    private int year;
    private int month;
    private int day;
    public String removedate, sellerid, locationid;
    public List<String> sheep_name_list, seller_name_list, seller_id_list, buyer_name_list, buyer_id_list;
    public List<Integer> sheep_id_list;
    public SparseBooleanArray sp;
    ListView sheep_listview;
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sell_sheep);
        String 	dbfile = getString(R.string.real_database_file) ;
        Log.i("SellSheep", " after get database file");
        dbh = new DatabaseHandler( this, dbfile );
//    	Get the date and time to add to the sheep record these are strings not numbers
        mytoday = Utilities.TodayIs();

        //  Fill the breed spinner
        breed_spinner = (Spinner) findViewById(R.id.breed_spinner);
        breed_name = new ArrayList<String>();
        sheep_breed_display_order = new ArrayList<String>();
        sheep_breed_id_list = new ArrayList<String>();
        // Select All fields from sheep_breeds_table to build the spinner
        cmd = "select * from sheep_breed_table order by sheep_breed_display_order";
        Log.i("fill breed spinner", "command is " + cmd);
        crsr = dbh.exec( cmd );
        cursor   = ( Cursor ) crsr;
        dbh.moveToFirstRecord();
        breed_name.add("Select a Breed");
        sheep_breed_id_list.add("sheep_breed_id_list");
        sheep_breed_display_order.add("sheep_breed_display_order");
        Log.i("fill breed spinner", "added first option ");
        // looping through all rows and adding to list
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
            sheep_breed_id_list.add(cursor.getString(0));
            Log.i("LookUpSheep", "sheep_breed_id_order " + cursor.getString(0));
            breed_name.add(cursor.getString(1));
            Log.i("LookUpSheep", "breed_name " + cursor.getString(1));
            sheep_breed_display_order.add(cursor.getString(3));
            Log.i("LookUpSheep", "sheep_breed_display_order " + cursor.getString(3));
        }
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, breed_name);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        breed_spinner.setAdapter (dataAdapter);
        //	set initial breed to look for to be Black Welsh it's based on the display order
        // TODO: 2/25/19 adjust for a preference file instead of fixed
        breed_spinner.setSelection(1);

      //    Fill the seller and buyer spinners

        seller_spinner = (Spinner) findViewById(R.id.seller_spinner);
        buyer_spinner = (Spinner) findViewById(R.id.buyer_spinner);
        seller_name_list = new ArrayList<String>();
        buyer_name_list = new ArrayList<String>();
        seller_id_list = new ArrayList<String>();
        buyer_id_list = new ArrayList<String>();
        // Select name fields from contacts to build the spinner
        cmd = "select id_contactsid, contact_first_name, contact_middle_name, contact_last_name " +
                  ",contact_company, contact_physical_city from contacts_table order by contact_last_name " ;

        Log.i("fill owner spinners ", "command is " + cmd);
        crsr = dbh.exec( cmd );
        cursor   = ( Cursor ) crsr;
        dbh.moveToFirstRecord();
        seller_name_list.add("Select Seller");
        buyer_name_list.add("Select Buyer");
        seller_id_list.add("seller_id_list");
        buyer_id_list.add("buyer_id_list");
        Log.i("fill owner spinners ", "added first option ");
        // looping through all rows and adding to list
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
            seller_id_list.add(cursor.getString(0));
            buyer_id_list.add(cursor.getString(0));
            Log.i("LookUpSheep", "owner id order " + cursor.getString(0));
            seller_name_list.add(cursor.getString(1)+" " +cursor.getString(2)+" " +cursor.getString(3)+" " +cursor.getString(4)+" " +cursor.getString(5));
            buyer_name_list.add(cursor.getString(1)+" " +cursor.getString(2)+" " +cursor.getString(3)+" " +cursor.getString(4)+" " +cursor.getString(5));
            Log.i("LookUpSheep", "owner_name " + cursor.getString(1)+" " +cursor.getString(2)+" " +cursor.getString(3)+" " +cursor.getString(4)+" " +cursor.getString(5));
        }
        // Creating adapter for spinner
        Log.i("before array adapters ", "first one ");
        dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, seller_name_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        seller_spinner.setAdapter (dataAdapter);
        Log.i("in array adapters ", "second one ");
        dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, buyer_name_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        buyer_spinner.setAdapter (dataAdapter);
        Log.i("after array adapters ", "all done ");
        //	set initial to be selection message
        // TODO: 2/25/19 adjust for a preference file instead of fixed
        seller_spinner.setSelection(0);
        buyer_spinner.setSelection(0);

        // Fill the Units Spinner
        sell_units = new ArrayList<String>();
        sell_units_id_order = new ArrayList<String>();
        sell_units_display_order = new ArrayList<String>();
        Log.i("before units spinner ", " ready to fill units spinner ");
        units_spinner = (Spinner) findViewById(R.id.units_spinner);
        // Select All fields from unit  to build the spinner
        cmd = "select * from units_table order by units_display_order";
        Log.i("fill units ", "command is " + cmd);
        crsr = dbh.exec( cmd );
        cursor   = ( Cursor ) crsr;
        dbh.moveToFirstRecord();
        sell_units.add("Select Currency Unit");
        sell_units_id_order.add("units id order");
        sell_units_display_order.add("units display order");
        // looping through all rows and adding to list
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
            sell_units_id_order.add(cursor.getString(0));
            Log.i("LookUpSheep", "transfer id order " + cursor.getString(0));
            sell_units.add(cursor.getString(1));
            Log.i("LookUpSheep", "transfer reason " + cursor.getString(1));
            sell_units_display_order.add(cursor.getString(2));
            Log.i("LookUpSheep", "transfer display order " + cursor.getString(2));
        }
// Creating adapter for spinner
        Log.i("before array adapter ", " for currency units ");
        dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, sell_units);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        units_spinner.setAdapter (dataAdapter);
           //	set initial to be selection message
        // TODO: 2/25/19 adjust for a preference file instead of fixed
        units_spinner.setSelection(0);

        Log.i("before xfer spinner ", " ready to fill transfer reasons spinner ");
        // Fill the Remove Reason Spinner
        transfer_reason_spinner = (Spinner) findViewById(R.id.transfer_reason_spinner);
        transfer_reasons = new ArrayList<String>();
        transfer_reasons_id_order = new ArrayList<String>();
        transfer_reasons_display_order = new ArrayList<String>();
        // Select All fields from remove reasons to build the spinner
        cmd = "select * from transfer_reason_table order by transfer_reason_display_order";
        Log.i("fill transfer reasons ", "command is " + cmd);
        crsr = dbh.exec( cmd );
        cursor   = ( Cursor ) crsr;
        dbh.moveToFirstRecord();
        transfer_reasons.add("Select Transfer Reason");
        transfer_reasons_id_order.add("Transfer reasons id order");
        transfer_reasons_display_order.add("Transfer reasons display order");
        // looping through all rows and adding to list
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
            transfer_reasons_id_order.add(cursor.getString(0));
            Log.i("LookUpSheep", "transfer id order " + cursor.getString(0));
            transfer_reasons.add(cursor.getString(1));
            Log.i("LookUpSheep", "transfer reason " + cursor.getString(1));
            transfer_reasons_display_order.add(cursor.getString(2));
            Log.i("LookUpSheep", "transfer display order " + cursor.getString(2));
        }

        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, transfer_reasons);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        transfer_reason_spinner.setAdapter (dataAdapter);
        transfer_reason_spinner.setSelection(0);

        //	Set the date picker stuff here
        Output = (TextView) findViewById(R.id.Output);

        // Get current date by calender
        final Calendar c = Calendar.getInstance();
        year  = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day   = c.get(Calendar.DAY_OF_MONTH);

        // Show current date
        Output.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(year).append("-").append(Utilities.Make2Digits(month + 1)).append("-").append(Utilities.Make2Digits(day)));
        removedate = String.valueOf(Output.getText());

//      Set the look up sheep button green as in ready
        btn = (Button) findViewById( R.id.look_up_sheep_btn );
        btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));
    }
    public void changeDatePicker (View v) {
        // On button click show datepicker dialog
        showDialog(DATE_PICKER_ID);

    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listener to date picker
                return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;

            // Show selected date
            Output.setText(new StringBuilder()
                    // Month is 0 based, just add 1
                    .append(year).append("-").append(Utilities.Make2Digits(month + 1)).append("-").append(Utilities.Make2Digits(day)));
            removedate = String.valueOf(Output.getText());
        }
    };
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id){
        Log.d(getLocalClassName(), "onItemClick(" + arg1 + ","
                + position + "," + id + ")");
        ListView lv = (ListView) arg0;
        if (lv.isItemChecked(position)){
            Log.i("SellSheep", "Got a checked item");}
        else{
            Log.i("SellSheep", "removed a check");
        }
    }
    public void lookUpSheep (View v){
        Integer temp1;
        TextView 	TV;
        String 		temp_string;
        String temp2;
        //  go get the contacts_id of the seller
        Log.i("lookUpSheep ", " getting seller ");
        seller_spinner = (Spinner) findViewById(R.id.seller_spinner);
        which_seller = seller_spinner.getSelectedItemPosition();
        Log.i("lookUpSheep ", "seller display id is "+ which_seller);
        temp1 = Integer.valueOf(which_seller);
        // if no seller selected must require one
        if (temp1 == 0){
                //	Need to require a seller here
                //  Missing data so  display an alert
                AlertDialog.Builder builder = new AlertDialog.Builder( this );
                Log.i("in alert", " need to get seller ");
                builder.setMessage( R.string.sell_sheep_get_seller )
                        .setTitle( R.string.sell_sheep_get_seller );
                builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idx) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return;
            }
            else {
                which_seller = Integer.valueOf(seller_id_list.get (temp1));
                Log.i("lookUpSheep ", "seller id is "+ which_seller);
                Log.i("sell ", String.valueOf(temp1));
            }

        //  go get the breed
        Log.i("lookUpSheep ", " getting breed ");
        breed_spinner = (Spinner) findViewById(R.id.breed_spinner);
        which_breed = breed_spinner.getSelectedItemPosition();
        Log.i("lookUpSheep ", "breed display id is "+ which_breed);
        temp1 = Integer.valueOf(which_breed);
        which_breed = Integer.valueOf(sheep_breed_id_list.get (temp1));
        Log.i("lookUpSheep ", "breed id is "+ which_breed);

        //  go get the contacts_id of the buyer
        Log.i("lookUpSheep ", " getting buyer ");
        buyer_spinner = (Spinner) findViewById(R.id.buyer_spinner);
        which_buyer = buyer_spinner.getSelectedItemPosition();
        Log.i("lookUpSheep ", "buyer display id is "+ which_buyer);
        temp1 = Integer.valueOf(which_buyer);

        if (temp1 == 0){
            //	Need to require a buyer here
            //  Missing data so  display an alert
            AlertDialog.Builder builder = new AlertDialog.Builder( this );
            Log.i("in alert", " need to get buyer ");
            builder.setMessage( R.string.sell_sheep_get_buyer )
                    .setTitle( R.string.sell_sheep_get_buyer );
            builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int idx) {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return;
        }
        else {
            which_buyer = Integer.valueOf(buyer_id_list.get (temp1));
            Log.i("lookUpSheep ", "buyer id is "+ which_buyer);
            Log.i("sell ", String.valueOf(temp1));
        }

//	Get the selected transfer reason from the spinner
        transfer_reason_spinner = (Spinner) findViewById(R.id.transfer_reason_spinner);
        which_remove_reason = transfer_reason_spinner.getSelectedItemPosition();
        temp1 = Integer.valueOf(which_remove_reason);

        if (temp1 == 0){
            //	Need to require a transfer reason here
            //  Missing data so  display an alert
            AlertDialog.Builder builder = new AlertDialog.Builder( this );
            Log.i("in alert", " need to get transfer reason ");
            builder.setMessage( R.string.sell_sheep_get_transfer_reason)
                    .setTitle( R.string.sell_sheep_get_transfer_reason );
            builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int idx) {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return;
        }
        else {
            temp2 = transfer_reasons_id_order.get(temp1);
            Log.i("SellSheep", " got to SellSheep with remove id order number of " + temp2);
            Log.i("sell ", String.valueOf(temp1));
        }




        // go get the units
        Log.i("lookUpSheep ", " getting units ");
        units_spinner = (Spinner) findViewById(R.id.units_spinner);
        which_units = units_spinner.getSelectedItemPosition();
        Log.i("lookUpSheep ", "units display id is  "+ which_units);
        temp1 = Integer.valueOf(which_units);

        if (temp1 == 0){
            //	Need to require a buyer here
            //  Missing data so  display an alert
            AlertDialog.Builder builder = new AlertDialog.Builder( this );
            Log.i("in alert", " need to get currency units ");
            builder.setMessage( R.string.sell_sheep_get_currency_units )
                    .setTitle( R.string.sell_sheep_get_currency_units );
            builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int idx) {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return;
        }
        else {
            which_units = Integer.valueOf(sell_units_id_order.get (temp1));
            Log.i("lookUpSheep ", "units id is "+ which_units);
            Log.i("sell ", String.valueOf(temp1));
        }


        // Go get the sell price
        Log.i("lookUpSheep ", " getting sell price ");
        TV = (TextView) findViewById(R.id.sell_price);
        temp_string = TV.getText().toString();
        if(TextUtils.isEmpty(temp_string)){
            // EditText was empty
            // so no real data collected just break out
            sell_price = 0.0f;
        }
        else {
            sell_price = Float.valueOf(TV.getText().toString());
            Log.i("sell price ", "float data is " + String.valueOf(sell_price));
        }

        Log.i("get sheep ", " getting sheep list ");
        sheep_listview = (ListView) findViewById(android.R.id.list);

//		Now go get all the current sheep names for the seller and format them
        cmd = String.format( "SELECT sheep_table.sheep_id as _id, flock_prefix_table.flock_name, sheep_table.sheep_name " +
                " FROM sheep_table " +
                " INNER JOIN flock_prefix_table ON sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
                " INNER JOIN sheep_location_history_table ON sheep_location_history_table.sheep_id = sheep_table.sheep_id " +
                " WHERE sheep_table.death_date = '' AND sheep_location_history_table.to_id_contactsid = '%s' " +
                " AND sheep_table.id_sheepbreedid = '%s' " +
                " ORDER BY sheep_table.sheep_name ASC ", which_seller, which_breed ) ;

        Log.i("format record", " command is  " + cmd);
        crsr = dbh.exec( cmd );
        cursor   = ( Cursor ) crsr;
        nRecs    = cursor.getCount();
        Log.i("SellSheep", " nRecs is " + String.valueOf(nRecs));
        cursor.moveToFirst();
        sheep_name_list = new ArrayList<String>();
        sheep_id_list = new ArrayList<Integer>();
//        numsheep = 0;
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
            Log.i("SellSheep",cursor.getString(2) );
//            // Now go get the last location
            cmd = String.format( "select sheep_id, to_id_contactsid from sheep_location_history_table " +
                    "where sheep_id = '%s' order by movement_date ", cursor.getInt(0));
            Log.i("get movement record ", " command is  " + cmd);
            crsr2 = dbh.exec( cmd );
            cursor2   = ( Cursor ) crsr2;
            nrecs2 = cursor2.getCount();
            if (nrecs2 > 0) {
                cursor2.moveToLast();
                tempint = cursor2.getInt(1);
                if (tempint == which_seller) {
                    sheep_name_list.add(cursor.getString(1) + " " + cursor.getString(2));
                    sheep_id_list.add(cursor.getInt(0));
                    numsheep = numsheep + 1;
                }
                ;
            };
        };
        Log.i("got ", " sheep here " + String.valueOf(numsheep));

        if (nRecs > 0) {
            //     lv1.setAdapter(new ArrayAdapter<String> (this, android.R.layout.simple_list_item_1, s1));
            sheep_listview.setAdapter (new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice,sheep_name_list));
            sheep_listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            sheep_listview.setOnItemClickListener(new OnItemClickListener(){
                public void onItemClick(AdapterView<?> parent, View view,int position,long id) {
                    View v = sheep_listview.getChildAt(position);
                    Log.i("in click","I am inside onItemClick and position is:"+String.valueOf(position));
                }
            });

            getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            sp=getListView().getCheckedItemPositions();
            //	make the update database button green 0x0000FF00, 0xff00ff00
            Button btn = (Button) findViewById( R.id.update_database_btn );
            btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));
        }
        else {
            // No sheep data - publish an empty list to clear sheep names
            Log.i("SellSheep", "no current sheep");
//			myadapter = new SimpleCursorAdapter(this, R.layout.list_entry_names, null, null, null, 0);
//			setListAdapter(myadapter);
        }

    }
    public void updateDatabase( View v ){
        Integer temp1;
        String temp2, from_id_contactsid , to_id_contactsid;
        //	Get the selected transfer reason from the spinner
        transfer_reason_spinner = (Spinner) findViewById(R.id.transfer_reason_spinner);
        which_remove_reason = transfer_reason_spinner.getSelectedItemPosition();
//	    	removedate has the string of the remove date I want to use
        Log.i("Update Database ", "seller id is "+ which_seller);
        from_id_contactsid = String.valueOf(which_seller);
        Log.i("Update Database ", "buyer id is "+ which_buyer);
        to_id_contactsid = String.valueOf(which_buyer);

        Log.i("SellSheep", " got to SellSheep with remove reason of " + String.valueOf(which_remove_reason));
        Log.i("SellSheep", " got to SellSheep with remove position of " + transfer_reason_spinner.getSelectedItemPosition());
        temp2 = String.valueOf(transfer_reason_spinner.getSelectedItemPosition());
        Log.i("SellSheep", " got to SellSheep with remove spinner number of " + temp2);
        temp1 = Integer.valueOf(temp2);

        temp2 = transfer_reasons_id_order.get(temp1);
        Log.i("SellSheep", " got to SellSheep with remove id order number of " + temp2);
        Log.i("sell ", String.valueOf(temp1));

        temp1 = Integer.valueOf(temp2);
        Log.i("SellSheep", " got to SellSheep with remove id number of " + String.valueOf(temp1));

        Log.i ("before loop", "remove date  " + removedate);

        //	Now need to loop through all the sheep and update the sheep_table
        //	Set the remove date and clear all alerts for this sheep
        boolean temp_value;
        int temp_location, temp_size;
        temp_size = sp.size();
        Log.i ("before loop", "the sp size is " + String.valueOf(temp_size));
        for (int i=0; i<temp_size; i++){
            temp_value = sp.valueAt(i);
            temp_location = sp.keyAt(i);
            if (temp_value){
                Log.i ("for loop", "the sheep " + " " + sheep_name_list.get(temp_location)+ " is checked");

                Log.i ("for loop", "the sheep id is " + String.valueOf(sheep_id_list.get(temp_location)));
                cmd = String.format("update sheep_table set alert01 = '', management_group = ''   " +
                                "  where sheep_id = '%s' ", sheep_id_list.get(temp_location) ) ;
                Log.i("Sell Sheep ", "before cmd " + cmd);
                dbh.exec( cmd );
                Log.i("sell sheep ", "after cmd " + cmd);
                // move to new location
                cmd = String.format("insert into sheep_location_history_table (sheep_id, movement_date,  " +
                                "from_id_contactsid, to_id_contactsid ) values ('%s', '%s', '%s','%s') ", sheep_id_list.get(temp_location)
                        ,removedate,from_id_contactsid , to_id_contactsid ) ;
                Log.i("sell sheep ", "before cmd " + cmd);
                dbh.exec( cmd );
                Log.i("sell sheep ", "after cmd " + cmd);
                // move to new owner
                cmd = String.format("insert into sheep_ownership_history_table (sheep_id, transfer_date,  " +
                                "from_id_contactsid, to_id_contactsid, id_transferreasonid, sell_price, sell_price_units ) " +
                                " values (%s, '%s', %s, %s, %s, %s, %s) ", sheep_id_list.get(temp_location),removedate,
                        from_id_contactsid , to_id_contactsid, temp1, sell_price, which_units ) ;
                Log.i("sell sheep ", "before cmd " + cmd);
                dbh.exec( cmd );
                Log.i("sell sheep ", "after cmd " + cmd);
            }
        }// for loop
        // Now need to go back
        try {
            cursor.close();
        }
        catch (Exception e) {
//				Log.i("Back Button", " In catch stmt cursor");
            // In this case there is no adapter so do nothing
        }
        dbh.closeDB();
        finish();
    }
}
