package com.weyr_associates.lambtracker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.weyr_associates.lambtracker.LambingSheep.IncomingHandler;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

public class SheepManagement extends ListActivity {
	private DatabaseHandler dbh;
	int id;
	public int thissheep_id, thissire_id, thisdam_id;
	int fedtagid, farmtagid, eidtagid;

	public String eidText, alert_text;
	public String thissire_name, thisdam_name, last_weight_date, last_weight, drug_dose_note;
	public List<String> tag_types_display_order, tag_types_id_order;
	public Cursor cursor, sirecursor, damcursor, tempcursor, cursor4, cursor5, cursor6;
	public Object sheepcrsr, sirecrsr, damcrsr, tempcrsr, crsr, crsr6;
	public Spinner tag_type_spinner, tag_location_spinner, tag_color_spinner;
	public Cursor sheepcursor, tagcursor, weightcursor;
	public TextView drugdose;
	public Spinner wormer_spinner, vaccine_spinner, vaccine_spinner2, drug_spinner, drug_location_spinner, blood_spinner;
	public Spinner vaccine_loc_spinner, vaccine_loc_spinner2, worm_loc_spinner;
	public List<String> tag_types, tag_locations, tag_colors;
	public List<String> wormers, vaccines, vaccines2, drugs, drug_location, blood_tests, wormer_location_id_order;
	public List<String> drug_location_display_order, drug_location_id_order, drug_user_dosage;
	public List<Integer> wormer_id_drugid, vaccine_id_drugid, vaccine_id_drugid2, drug_id_drugid, blood_test_id;
	public int shot_loc, shot_loc2, wormer_loc, drug_loc, sheep_birth_record, lambs_weaned;
	public int drug_gone; // 0 = false 1 = true
	public int drug_type, which_wormer, which_vaccine, which_vaccine2, which_drug, which_blood, id_sheepdrugid;
	public RadioGroup radioGroup, radioGroup2;
	public CheckBox boxtrimtoes, boxwormer, boxvaccine, boxvaccine2, boxweight, boxblood;
	public CheckBox boxdrug, boxweaned, boxshear, boxremovedrug, boxprintlabel;
	public String note_text, empty_string_field;
	public int predefined_note01;
	public int nRecs, nRecs4;
	private int recNo;
	private String LabelText = "";
	private String EID = "";
	private String SheepName = "";
	private Boolean AutoPrint = false;

	ArrayAdapter<String> dataAdapter;
	String cmd;
	Integer i, i2;
	public Button btn, btn2;

	public SimpleCursorAdapter myadapter;

	/////////////////////////////////////////////////////
	Messenger mService = null;
	Messenger mScaleService = null;
	boolean mIsBound;
	boolean mIsScaleBound;

	final Messenger mMessenger = new Messenger(new IncomingHandler());
	// variable to hold the string
	public String LastEID;
	private String Weight;

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case eidService.MSG_UPDATE_STATUS:
					Bundle b1 = msg.getData();

					break;
				case eidService.MSG_NEW_EID_FOUND:
					Bundle b2 = msg.getData();
					LastEID = (b2.getString("info1"));
//				We have a good whole EID number	
					gotEID(null);
					break;
				case scaleService.MSG_NEW_SCALE_FOUND:
					Bundle b3 = msg.getData();
					Weight = (b3.getString("info1"));
//				Log.i("SheepManagement", "Got Weight.");
//				We have a good whole EID number
					gotWeight(null);
					break;

				case eidService.MSG_UPDATE_LOG_APPEND:
//				Bundle b3 = msg.getData();
//				Log.i("Evaluate ", "Add to Log.");

					break;
				case eidService.MSG_UPDATE_LOG_FULL:
//				Log.i("Evaluate ", "Log Full.");

					break;
				case eidService.MSG_THREAD_SUICIDE:
//				Log.i("Evaluate", "Service informed Activity of Suicide.");
					doUnbindService();
					stopService(new Intent(SheepManagement.this, eidService.class));

					break;
				default:
					super.handleMessage(msg);
			}
		}
	}

	private void LoadPreferences(Boolean NotifyOfChanges) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//		Log.i("SheepManagement", "Load Pref.");
		try {
			String TextForLabel = preferences.getString("label", "text");
			LabelText = TextForLabel;
			AutoPrint = preferences.getBoolean("autop", false);
		} catch (NumberFormatException nfe) {
		}

	}

	public ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);
//			Log.i("SheepMgmt", "At Service.");
			try {
				//Register client with service
				Message msg = Message.obtain(null, eidService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been unexpectedly disconnected - process crashed.
			mService = null;

		}
	};

	public ServiceConnection mScaleConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mScaleService = new Messenger(service);
//			Log.i("SheepMgmt", "At ScaleService.");
			try {
				//Register client with service
				Message msg = Message.obtain(null, scaleService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mScaleService.send(msg);
			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been unexpectedly disconnected - process crashed.
			mScaleService = null;
		}
	};

	private void CheckIfServiceIsRunning() {
		//If the service is running when the activity starts, we want to automatically bind to it.
//		Log.i("SheepMgmt", "At isRunning?.");
		if (eidService.isRunning()) {
//			Log.i("SheepMgmt", "is.");
			doBindService();
		} else {
//			Log.i("SheepMgmt", "is not, start it");
			startService(new Intent(SheepManagement.this, eidService.class));
			doBindService();
		}
		if (scaleService.isScaleRunning()) {
//			Log.i("SheepMgmt", "is.");
			doBindScaleService();
		} else {
//			Log.i("SheepMgmt", "is not, start it");
			startService(new Intent(SheepManagement.this, scaleService.class));
			doBindScaleService();
		}
//		Log.i("SheepMgmt", "Done isRunning.");
	}

	void doBindService() {
		// Establish a connection with the service.  We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.
//		Log.i("SheepMgmt", "At doBind1.");
		bindService(new Intent(this, eidService.class), mConnection, Context.BIND_AUTO_CREATE);
//		Log.i("SheepMgmt", "At doBind2.");

		mIsBound = true;

		if (mService != null) {
//			Log.i("SheepMgmt", "At doBind3.");
			try {
				//Request status update
				Message msg = Message.obtain(null, eidService.MSG_UPDATE_STATUS, 0, 0);
				msg.replyTo = mMessenger;
				mService.send(msg);
//				Log.i("SheepMgmt", "At doBind4.");
				//Request full log from service.
				msg = Message.obtain(null, eidService.MSG_UPDATE_LOG_FULL, 0, 0);
				mService.send(msg);
			} catch (RemoteException e) {
			}
		}
//		Log.i("Evaluate", "At doBind5.");
	}

	void doBindScaleService() {
		// Establish a connection with the service.  We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.
//		Log.i("SheepMgmt", "At doBind1.");
		bindService(new Intent(this, scaleService.class), mScaleConnection, Context.BIND_AUTO_CREATE);
//		Log.i("SheepMgmt", "At doBind2.");

		mIsScaleBound = true;
	}

	void doUnbindService() {
		Log.i("SheepMgmt", "At DoUnbindservice");
		if (mService != null) {
			try {
				//Stop eidService from sending tags
				Message msg = Message.obtain(null, eidService.MSG_NO_TAGS_PLEASE);
				msg.replyTo = mMessenger;
				mService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
		if (mIsBound) {
			// If we have received the service, and hence registered with it, then now is the time to unregister.
			if (mService != null) {
				try {
					Message msg = Message.obtain(null, eidService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service has crashed.
				}
			}
			// Detach our existing connection.
			unbindService(mConnection);
			mIsBound = false;
		}
	}

	void doUnbindScaleService() {
		Log.i("SheepMgmt", "At DoUnbindservice");
		if (mScaleService != null) {
			try {
				//Stop eidService from sending tags
				Message msg = Message.obtain(null, scaleService.MSG_NO_TAGS_PLEASE);
				msg.replyTo = mMessenger;
				mScaleService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
		if (mIsScaleBound) {
			// If we have received the service, and hence registered with it, then now is the time to unregister.
			if (mScaleService != null) {
				try {
					Message msg = Message.obtain(null, scaleService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mScaleService.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service has crashed.
				}
			}
			// Detach our existing connection.
			unbindService(mScaleConnection);
			mIsScaleBound = false;
		}
	}

	// use EID reader to look up a sheep
	public void gotEID(View v) {
		//	make the scan eid button red
		btn = (Button) findViewById(R.id.scan_eid_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
//		String eid = this.getIntent().getExtras().getString("com.weyr_associates.lambtracker.LastEID");
//    	Log.i("SheepMgmt", " before input text " + eid);  
//    	Log.i("SheepMgmt", " before input text " + LastEID);  
		// 	Display the EID number
		TextView TV = (TextView) findViewById(R.id.inputText);
		TV.setText(LastEID);
		Log.i("in gotEID ", "with LastEID of " + LastEID);
		btn2 = (Button) findViewById(R.id.look_up_sheep_btn);
		btn2.performClick();
	}

	// use scale to weigh the sheep
	public void gotWeight(View v) {
		// make the scan weight button red
		btn = (Button) findViewById(R.id.scan_weight_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));

		// 	Display the Weight number
		TextView TV = (TextView) findViewById(R.id.trait11_data);
		TV.setText(Weight);
		Log.i("in gotWeight ", "with weight of " + Weight);
	}

	/////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sheep_management);
		Log.i("SheepMgmt", " after set content view");
//        View v = null;
		String dbfile = getString(R.string.real_database_file);
		Log.i("SheepMgmt", " after get database file");
		dbh = new DatabaseHandler(this, dbfile);
//		Added the variable definitions here    	
		String cmd;
		////////////////////////////////////
//		CheckIfServiceIsRunning();
		LoadPreferences(true);
		Log.i("SheepMgmt", "back from isRunning");
		////////////////////////////////////    	
		thissheep_id = 0;
		empty_string_field = "";
		LastEID = "000_000000000000"; // in case no eid tag on this sheep
		// Fill the Tag Type Spinner
		tag_type_spinner = (Spinner) findViewById(R.id.tag_type_spinner);
		tag_types = new ArrayList<String>();
		tag_types_id_order = new ArrayList<String>();
		tag_types_display_order = new ArrayList<String>();
		// Select All fields from id types to build the spinner
		cmd = "select * from id_type_table order by id_type_display_order";
		Log.i("fill tag spinner", "command is " + cmd);
		crsr = dbh.exec(cmd);
		cursor5 = (Cursor) crsr;
		dbh.moveToFirstRecord();
		tag_types.add("Select a Type");
		tag_types_id_order.add("Tag_types_id");
		tag_types_display_order.add("tag_type_display_order");
		Log.i("fill tag spinner", "added first option ");
		// looping through all rows and adding to list
		for (cursor5.moveToFirst(); !cursor5.isAfterLast(); cursor5.moveToNext()) {
			tag_types_id_order.add(cursor5.getString(0));
			Log.i("LookUpSheep", "tag_types_id_order " + cursor5.getString(0));
			tag_types.add(cursor5.getString(1));
			Log.i("LookUpSheep", "tag_type name " + cursor5.getString(1));
			tag_types_display_order.add(cursor5.getString(3));
			Log.i("LookUpSheep", "tag_types_display_order " + cursor5.getString(3));
		}

		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tag_types);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tag_type_spinner.setAdapter(dataAdapter);
		//	set initial tag type to look for to be name it's based on the display order
		tag_type_spinner.setSelection(4);

		Log.i("SheepMgmt", " before filling drug spinners");
		//	Get set up to fill drug spinners
		drug_gone = 0;
//		 Fill the Wormer Spinner
		wormer_spinner = (Spinner) findViewById(R.id.wormer_spinner);
		wormers = new ArrayList<String>();
		wormer_id_drugid = new ArrayList<Integer>();
		drug_type = 1;
		cmd = String.format("select id_drugid, user_task_name, drug_lot from drug_table where " +
				"drug_gone = %s and (drug_type = 1 or drug_type = 5) ", drug_gone);
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		wormers.add("Select a Dewormer");
		wormer_id_drugid.add(0);
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			wormer_id_drugid.add(cursor.getInt(0));
			Log.i("SheepMgmt", " adding wormer " + cursor.getString(0) + " " + cursor.getString(1) + " lot " + cursor.getString(2));
			wormers.add(cursor.getString(1) + " lot " + cursor.getString(2));
		}
		Log.i("SheepMgmt", " after filling wormer spinner");
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, wormers);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		wormer_spinner.setAdapter(dataAdapter);
		if (cursor.getCount() > 0) {
			wormer_spinner.setSelection(0);
		}
		Log.i("SheepMgmt", " after display wormer spinner");

		worm_loc_spinner = (Spinner) findViewById(R.id.worm_loc_spinner);

		// Create the array for locations
		drug_location = new ArrayList<String>();
		drug_location_id_order = new ArrayList<String>();
		drug_location_display_order = new ArrayList<String>();
		// Select All fields from drug locations to build the spinner
		cmd = "select * from drug_location_table order by drug_location_display_order";
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		drug_location.add("Select a Location");
		drug_location_id_order.add("drug_location_id_order");
		drug_location_display_order.add("drug_location_display_order");
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			drug_location.add(cursor.getString(1));
			Log.i("SheepMgmt", " drug location spinner " + cursor.getString(1));
			drug_location_id_order.add(cursor.getString(0));
			Log.i("SheepMgmt", " drug location id spinner " + cursor.getString(0));
			drug_location_display_order.add(cursor.getString(3));
			Log.i("SheepMgmt", " drug location display spinner " + cursor.getString(3));
		}

		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, drug_location);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		worm_loc_spinner.setAdapter(dataAdapter);
		worm_loc_spinner.setSelection(0);

		// Fill the Blood Sample Spinner
		blood_spinner = (Spinner) findViewById(R.id.blood_spinner);
		blood_tests = new ArrayList<String>();
		blood_test_id = new ArrayList<Integer>();
		// Select All fields from blood test types to build the spinner
		Log.i("SheepMgmt", " before filling blood test spinner");
		cmd = String.format("select evaluation_trait_table.trait_name, evaluation_trait_table.id_traitid, " +
				"custom_evaluation_name_table.custom_eval_number " +
				"from evaluation_trait_table " +
				" inner join custom_evaluation_name_table on evaluation_trait_table.id_traitid = " +
				" custom_evaluation_name_table.id_traitid where evaluation_trait_table.id_traitid = 26 ");
//		Log.i("evaluate2", " cmd is " + cmd);
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
//		Integer num_blood_tests = (cursor.getInt(2));
		Log.i("SheepMgmt", " after filling blood tests spinner");
		//	Get the text for the spinner
		cmd = String.format("select custom_evaluation_traits_table.id_traitid, " +
				"custom_evaluation_traits_table.custom_evaluation_item " +
				" from custom_evaluation_traits_table " +
				" where custom_evaluation_traits_table.id_traitid = 26 " +
				" order by custom_evaluation_traits_table.custom_evaluation_order ASC ");
		Log.i("fill blood", " ready to get spinner text cmd is " + cmd);
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		nRecs4 = cursor.getCount();
		Log.i("getting tests", " we have " + String.valueOf(nRecs4) + " tests");
		dbh.moveToFirstRecord();
		blood_tests.add("Select a Blood Test");
		blood_test_id.add(0);
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			blood_test_id.add(cursor.getInt(0));
			blood_tests.add(cursor.getString(1));
		}
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, blood_tests);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		blood_spinner.setAdapter(dataAdapter);
		if (cursor.getCount() > 0) {
			blood_spinner.setSelection(0);
		}

		// Fill the Vaccine Spinner
		vaccine_spinner = (Spinner) findViewById(R.id.vaccine_spinner);
		vaccines = new ArrayList<String>();
		vaccine_id_drugid = new ArrayList<Integer>();
		drug_type = 2;
		Log.i("SheepMgmt", " before filling vaccine spinner");
		// Select All fields from vaccines to build the spinner
		cmd = String.format("select drug_table.id_drugid, drug_table.user_task_name, drug_table.drug_lot " +
				"from drug_table where drug_gone = %s and drug_type = %s", drug_gone, drug_type);
//		   	Log.i("SheepMgmt", " command is " + cmd);
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		vaccines.add("Select a Vaccine");
		vaccine_id_drugid.add(0);
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			vaccine_id_drugid.add(cursor.getInt(0));
			vaccines.add(cursor.getString(1) + " lot " + cursor.getString(2));
//		   		Log.i("SheepMgmt", " for loop vaccine to add is " + cursor.getString(1) + " lot " + cursor.getString(2));
		}
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, vaccines);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		vaccine_spinner.setAdapter(dataAdapter);
		if (cursor.getCount() > 0) {
			vaccine_spinner.setSelection(0);
		}

		Log.i("SheepMgmt", " after display vaccine spinner");

		// Fill the first vaccine location Spinner
		vaccine_loc_spinner = (Spinner) findViewById(R.id.vaccine_loc_spinner);
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, drug_location);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		vaccine_loc_spinner.setAdapter(dataAdapter);
		vaccine_loc_spinner.setSelection(0);

		// Fill the second vaccine spinner
		Log.i("SheepMgmt", " before filling 2nd vaccine spinner");
		vaccine_spinner2 = (Spinner) findViewById(R.id.vaccine_spinner2);
		vaccines2 = new ArrayList<String>();
		vaccine_id_drugid2 = new ArrayList<Integer>();
		drug_type = 2;
		Log.i("SheepMgmt", " before filling vaccine spinner 2");
		// Select All fields from vaccines to build the spinner
		cmd = String.format("select drug_table.id_drugid, drug_table.user_task_name, drug_table.drug_lot " +
				"from drug_table where drug_gone = %s and drug_type = %s", drug_gone, drug_type);
		Log.i("SheepMgmt", " command is " + cmd);
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		vaccines2.add("Select a Vaccine");
		vaccine_id_drugid2.add(0);
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			vaccine_id_drugid2.add(cursor.getInt(0));
			vaccines2.add(cursor.getString(1) + " lot " + cursor.getString(2));
			Log.i("SheepMgmt", " for loop vaccine to add is " + cursor.getString(1) + " lot " + cursor.getString(2));
		}
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, vaccines2);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		vaccine_spinner2.setAdapter(dataAdapter);
		Log.i("in vaccine2", " after set adapter");
		//	Set the vaccine to use to be the first one  Should come from the defaults
		if (cursor.getCount() > 0) {
			vaccine_spinner2.setSelection(0);
		}

		Log.i("SheepMgmt", " after display 2nd vaccine spinner");

		// Fill the second vaccine location Spinner
		vaccine_loc_spinner2 = (Spinner) findViewById(R.id.vaccine_loc_spinner2);
//		drug_location = new ArrayList<String>();

		//Todo   Need to figure out how to do multiple drugs
		// Select All fields from drug locations to build the spinner

		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, drug_location);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		vaccine_loc_spinner2.setAdapter(dataAdapter);
		vaccine_loc_spinner2.setSelection(0);

//				 Fill the Drug Spinner
		Log.i("SheepMgmt", "before filling drug spinner");
		drug_spinner = (Spinner) findViewById(R.id.drug_spinner);
		drugs = new ArrayList<String>();
		drug_id_drugid = new ArrayList<Integer>();
		drug_user_dosage = new ArrayList<String>();
		// Select All fields from drug types to build the spinner
		cmd = String.format("select id_drugid, user_task_name, drug_lot, user_drug_dosage from drug_table where " +
				"drug_gone = %s and (drug_type = 3 or drug_type = 4 or " +
				"drug_type = 5 or drug_type = 6 or drug_type = 7) ", drug_gone);
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		drugs.add("Select a Drug");
		drug_id_drugid.add(0);
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			drug_id_drugid.add(cursor.getInt(0));
			drugs.add(cursor.getString(1) + " lot " + cursor.getString(2));
			drug_user_dosage.add(cursor.getString(3));
		}
		Log.i("SheepMgmt", " after filling drug spinner");
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, drugs);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		drug_spinner.setAdapter(dataAdapter);
		//	Set the drug to select a drug
		drug_spinner.setSelection(0);
		// Fill the drug location Spinner
		drug_location_spinner = (Spinner) findViewById(R.id.drug_location_spinner);
		// Create the array for locations
		drug_location = new ArrayList<String>();
		drug_location_id_order = new ArrayList<String>();
		drug_location_display_order = new ArrayList<String>();
		// Select All fields from drug locations to build the spinner
		cmd = "select * from drug_location_table order by drug_location_display_order";
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		drug_location.add("Select a Location");
		drug_location_id_order.add("drug_location_id_order");
		drug_location_display_order.add("drug_location_display_order");
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			drug_location.add(cursor.getString(1));
			Log.i("SheepMgmt", " drug location spinner " + cursor.getString(1));
			drug_location_id_order.add(cursor.getString(0));
			Log.i("SheepMgmt", " drug location id spinner " + cursor.getString(0));
			drug_location_display_order.add(cursor.getString(3));
			Log.i("SheepMgmt", " drug location display spinner " + cursor.getString(3));
		}
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, drug_location);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		drug_location_spinner.setAdapter(dataAdapter);
		drug_location_spinner.setSelection(0);

		// make the alert button normal and disabled
		btn = (Button) findViewById(R.id.alert_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
		btn.setEnabled(false);

		//	Disable the Next Record and Prev. Record buttons until we have multiple records
		btn = (Button) findViewById(R.id.next_rec_btn);
		btn.setEnabled(false);
		btn = (Button) findViewById(R.id.prev_rec_btn);
		btn.setEnabled(false);

		//	Disable the Print Label and Update Database buttons until we have a sheep selected
		btn = (Button) findViewById(R.id.print_label_btn);
		btn.setEnabled(false);
		btn = (Button) findViewById(R.id.update_database_btn);
		btn.setEnabled(false);

		//	make the scan eid button red
		btn = (Button) findViewById(R.id.scan_eid_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));

		// make the scan weight button red
		btn = (Button) findViewById(R.id.scan_weight_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
	}

	public void lookForSheep(View v) {
		Boolean exists;
		Integer temp1;
		String temp2;
		TextView TV;
		exists = true;
		// Hide the keyboard when you click the button
		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
		//	Disable the Next Record and Prev. Record button until we have multiple records
		btn = (Button) findViewById(R.id.next_rec_btn);
		btn.setEnabled(false);
		btn = (Button) findViewById(R.id.prev_rec_btn);
		btn.setEnabled(false);

		TV = (EditText) findViewById(R.id.inputText);
		String tag_num = TV.getText().toString();

		Log.i("LookForSheep", " got to lookForSheep with Tag Number of " + String.valueOf(tag_num));
		Log.i("LookForSheep", " got to lookForSheep with Tag position of " + tag_type_spinner.getSelectedItemPosition());
		temp2 = String.valueOf(tag_type_spinner.getSelectedItemPosition());
		Log.i("LookForSheep", " got to lookForSheep with Tag spinner number of " + temp2);
		temp1 = Integer.valueOf(temp2);
		temp2 = tag_types_id_order.get(temp1);
		Log.i("LookForSheep", " got to lookForSheep with Tag type id order number of " + temp2);

		temp1 = Integer.valueOf(temp2);
		Log.i("LookForSheep", " got to lookForSheep with Tag type id number of " + String.valueOf(temp1));
		exists = tableExists("sheep_table");
		if (exists) {
			switch (temp1) {
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					if (tag_num != null && tag_num.length() > 0) {
						// Get the sheep id from the id table for this tag number and selected tag type
						cmd = String.format("select sheep_id from id_info_table where tag_number='%s' " +
										"and id_info_table.tag_type='%s' "
								, tag_num, temp1);
						Log.i("searchByNumber", "command is " + cmd);
						sheepcrsr = dbh.exec(cmd);
						sheepcursor = (Cursor) sheepcrsr;
						recNo = 1;
						nRecs = sheepcursor.getCount();
						Log.i("searchByNumber", " nRecs = " + String.valueOf(nRecs));
						dbh.moveToFirstRecord();
						Log.i("searchByNumber", " the cursor is of size " + String.valueOf(dbh.getSize()));
						if (dbh.getSize() == 0) {
							// no sheep with that  tag in the database so clear out and return
							clearBtn(v);
							TV = (TextView) findViewById(R.id.sheepnameText);
							TV.setText("Cannot find this sheep.");
							return;
						}
						thissheep_id = dbh.getInt(0);
						Log.i("searchByNumber", "This sheep is record " + String.valueOf(thissheep_id));
						if (nRecs > 1) {
							//	Have multiple sheep with this tag so enable next button
							btn = (Button) findViewById(R.id.next_rec_btn);
							btn.setEnabled(true);
						}
						Log.i("searchByNumber", " Before formatting the record");
						//	We need to call the format the record method
						formatSheepRecord(v);
					}
					break;
				case 6:
					//	got a split
					if (tag_num != null && tag_num.length() < 1) {
						// Get the sheep id from the id table for split ears
						cmd = String.format("select sheep_id from id_info_table where tag_number='%s' " +
										"and id_info_table.tag_type='%s' "
								, tag_num, temp1);
						Log.i("searchBysplit", "command is " + cmd);
						sheepcrsr = dbh.exec(cmd);
						sheepcursor = (Cursor) sheepcrsr;
						recNo = 1;
						nRecs = sheepcursor.getCount();
						Log.i("searchByNumber", " nRecs = " + String.valueOf(nRecs));
						dbh.moveToFirstRecord();
						Log.i("searchByNumber", " the cursor is of size " + String.valueOf(dbh.getSize()));
						if (dbh.getSize() == 0) {
							// no sheep with that  tag in the database so clear out and return
							clearBtn(v);
							TV = (TextView) findViewById(R.id.sheepnameText);
							TV.setText("Cannot find this sheep.");
							return;
						}
						thissheep_id = dbh.getInt(0);
						Log.i("searchByNumber", "This sheep is record " + String.valueOf(thissheep_id));
						if (nRecs > 1) {
							//	Have multiple sheep with this tag so enable next button
							btn = (Button) findViewById(R.id.next_rec_btn);
							btn.setEnabled(true);
						}
						Log.i("searchByNumber", " Before formatting the record");
						//	We need to call the format the record method
						formatSheepRecord(v);
					}
					break;
				case 7:
					//	got a notch
					if (tag_num != null && tag_num.length() < 1) {
						// Get the sheep id from the id table for split ears
						cmd = String.format("select sheep_id from id_info_table where tag_number='%s' " +
										"and id_info_table.tag_type='%s' "
								, tag_num, temp1);
						Log.i("searchBysplit", "command is " + cmd);
						sheepcrsr = dbh.exec(cmd);
						sheepcursor = (Cursor) sheepcrsr;
						recNo = 1;
						nRecs = sheepcursor.getCount();
						Log.i("searchByNumber", " nRecs = " + String.valueOf(nRecs));
						dbh.moveToFirstRecord();
						Log.i("searchByNumber", " the cursor is of size " + String.valueOf(dbh.getSize()));
						if (dbh.getSize() == 0) {
							// no sheep with that  tag in the database so clear out and return
							clearBtn(v);
							TV = (TextView) findViewById(R.id.sheepnameText);
							TV.setText("Cannot find this sheep.");
							return;
						}
						thissheep_id = dbh.getInt(0);
						Log.i("searchByNumber", "This sheep is record " + String.valueOf(thissheep_id));
						if (nRecs > 1) {
							//	Have multiple sheep with this tag so enable next button
							btn = (Button) findViewById(R.id.next_rec_btn);
							btn.setEnabled(true);
						}
						Log.i("searchByNumber", " Before formatting the record");
						//	We need to call the format the record method
						formatSheepRecord(v);
					}
					break;
				case 8:
					//	got a name
					tag_num = "%" + tag_num + "%";
					// Modified this so I can look up removed sheep as well
					cmd = String.format("select sheep_id, sheep_name from sheep_table where sheep_name like '%s'"
							, tag_num);
					Log.i("searchByName", "command is " + cmd);
					sheepcrsr = dbh.exec(cmd);
					sheepcursor = (Cursor) sheepcrsr;
					recNo = 1;
					nRecs = sheepcursor.getCount();
					Log.i("searchByName", " nRecs = " + String.valueOf(nRecs));
					dbh.moveToFirstRecord();
					Log.i("searchByName", " the cursor is of size " + String.valueOf(dbh.getSize()));
					if (dbh.getSize() == 0) { // no sheep with that name in the database so clear out and return
						clearBtn(v);
						TV = (TextView) findViewById(R.id.sheepnameText);
						TV.setText("Cannot find this sheep.");
						return;
					}
					thissheep_id = dbh.getInt(0);
					if (nRecs > 1) {
						//	Have multiple sheep with this name so enable next button
						btn = (Button) findViewById(R.id.next_rec_btn);
						btn.setEnabled(true);
					}
					//	We need to call the format the record method
					formatSheepRecord(v);
					break;
			} // end of case switch
		} else {
			clearBtn(null);
			TV = (TextView) findViewById(R.id.sheepnameText);
			TV.setText("Sheep Database does not exist.");
		}
	}

	public void formatSheepRecord(View v) {
		TextView TV;
		thissheep_id = sheepcursor.getInt(0);
		Log.i("format record", "This sheep is record " + String.valueOf(thissheep_id));
		// we have a sheep so enable buttons and make green

		btn = (Button) findViewById(R.id.print_label_btn);
		btn.setEnabled(true);
		btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));
		btn = (Button) findViewById(R.id.update_database_btn);
		btn.setEnabled(true);
		btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));

//		Log.i("format record", " recNo = "+ String.valueOf(recNo));
		cmd = String.format("select sheep_table.sheep_name, sheep_table.sheep_id,  " +
				" sheep_table.alert01,  " +
				"sheep_table.sire_id, sheep_table.dam_id, sheep_table.birth_date " +
				"from sheep_table  " +
				"where sheep_table.sheep_id ='%s' " +
				" ", thissheep_id);

		Log.i("format record x", " cmd is " + cmd);
		crsr6 = dbh.exec(cmd);
		cursor6 = (Cursor) crsr6;
		cursor6.moveToFirst();
		TV = (TextView) findViewById(R.id.sheepnameText);
		TV.setText(dbh.getStr(0));
		SheepName = dbh.getStr(0);
		alert_text = dbh.getStr(2);
		//	Get the sire and dam id numbers
		thissire_id = dbh.getInt(3);
		Log.i("format record", " Sire is " + String.valueOf(thissire_id));
		thisdam_id = dbh.getInt(4);
		Log.i("format record", " Dam is " + String.valueOf(thisdam_id));

		//	Go get the sire name
		if (thissire_id != 0) {
			cmd = String.format("select sheep_table.sheep_name from sheep_table where sheep_table.sheep_id = '%s'", thissire_id);
			Log.i("format record", " cmd is " + cmd);
			sirecrsr = dbh.exec(cmd);
//	        Log.i("format record", " after second db lookup");
			sirecursor = (Cursor) sirecrsr;
			sirecursor.moveToFirst();
			TV = (TextView) findViewById(R.id.sireName);
			thissire_name = dbh.getStr(0);
			TV.setText(thissire_name);
			Log.i("format record", " Sire is " + thissire_name);
			Log.i("format record", " Sire is " + String.valueOf(thissire_id));
		}
		if (thisdam_id != 0) {
			cmd = String.format("select sheep_table.sheep_name from sheep_table where sheep_table.sheep_id = '%s'", thisdam_id);
			damcrsr = dbh.exec(cmd);
			damcursor = (Cursor) damcrsr;
			damcursor.moveToFirst();
			TV = (TextView) findViewById(R.id.damName);
			thisdam_name = dbh.getStr(0);
			TV.setText(thisdam_name);
			Log.i("format record", " Dam is " + thisdam_name);
			Log.i("format record", " Dam is " + String.valueOf(thisdam_id));
		}

		//	Tag code From lookupsheep
		cmd = String.format("select sheep_table.sheep_name,  id_type_table.idtype_name, " +
				"tag_colors_table.tag_color_name, id_info_table.tag_number, id_location_table.id_location_abbrev, " +
				"id_info_table.id_infoid as _id, id_info_table.tag_date_off, id_info_table.tag_type  " +
				"from sheep_table inner join id_info_table on sheep_table.sheep_id = id_info_table.sheep_id " +
				"left outer join tag_colors_table on id_info_table.tag_color_male = tag_colors_table.id_tagcolorsid " +
				"left outer join id_location_table on id_info_table.tag_location = id_location_table.id_locationid " +
				"inner join id_type_table on id_info_table.tag_type = id_type_table.id_typeid " +
				"where id_info_table.sheep_id ='%s' and (id_info_table.tag_date_off is null or " +
				"id_info_table.tag_date_off is '')order by idtype_name asc", thissheep_id);

		Log.i("format record", " comand is " + cmd);
		crsr = dbh.exec(cmd);
		Log.i("format record", " after run the command");
		tagcursor = (Cursor) crsr;
		tagcursor.moveToFirst();
		Log.i("format record", " the cursor is of size " + String.valueOf(dbh.getSize()));
		LastEID = "000_000000000000";
		try {
			for (tagcursor.moveToFirst(); !tagcursor.isAfterLast(); tagcursor.moveToNext()) {
				if ((dbh.getInt(7)) == 2) {
					Log.i("format record", " if loop tag number is  " + dbh.getStr(3));
					Log.i("format record", " if loop tag type is  " + dbh.getStr(1));
					Log.i("format record", " if loop tag type number  is  " + dbh.getStr(7));
					LastEID = dbh.getStr(3);
				}
			}
		} catch (Exception r) {
			LastEID = "000_000000000000";
			Log.v("fill LAST EID ", " in sheep management RunTimeException: " + r);
		}

		//	Get set up to try to use the CursorAdapter to display all the tag data
		//	Select only the columns I need for the tag display section
		String[] fromColumns = new String[]{"tag_number", "tag_color_name", "id_location_abbrev", "idtype_name"};
		Log.i("FormatRecord", "after setting string array fromColumns");
		//	Set the views for each column for each line. A tag takes up 1 line on the screen
		int[] toViews = new int[]{R.id.tag_number, R.id.tag_color_name, R.id.id_location_abbrev, R.id.idtype_name};
		Log.i("FormatRecord", "after setting string array toViews");
		myadapter = new SimpleCursorAdapter(this, R.layout.list_entry, tagcursor, fromColumns, toViews, 0);
		Log.i("FormatRecord", "after setting myadapter");
		setListAdapter(myadapter);
		Log.i("FormatRecord", "after setting list adapter");

		//	Add in the last weight code here
		cmd = String.format("SELECT \n" +
				"\tsheep_evaluation_table.sheep_id\n" +
				"\t, sheep_evaluation_table.eval_date\n" +
				"\t, sheep_evaluation_table.age_in_days\n" +
				"\t, sheep_evaluation_table.trait_score11\n" +
				"        , sheep_evaluation_table.trait_score12\n" +
				"        , sheep_evaluation_table.trait_score13\n" +
				"        , sheep_evaluation_table.trait_score14\n" +
				"        , sheep_evaluation_table.trait_score15\n" +
				"FROM sheep_evaluation_table\n" +
				"\n" +
				"  Where  sheep_id = %s\n" +
				"AND\n" +
				"\t(sheep_evaluation_table.trait_name11 = 16 OR \n" +
				"\t\tsheep_evaluation_table.trait_name12 = 16 OR \n" +
				"\t\tsheep_evaluation_table.trait_name13 = 16 OR \n" +
				"\t\tsheep_evaluation_table.trait_name14 = 16 OR \n" +
				"\t\tsheep_evaluation_table.trait_name15 = 16 )\n" +
				"order by sheep_evaluation_table.eval_date DESC", thissheep_id);

		Log.i("format record", " comand is " + cmd);
		crsr = dbh.exec(cmd);
		Log.i("format record", " after run the command");
		weightcursor = (Cursor) crsr;
		weightcursor.moveToFirst();
		Log.i("format record", " the cursor is of size " + String.valueOf(dbh.getSize()));
		TV = (TextView) findViewById(R.id.last_weight_date);
		try {
			last_weight_date = dbh.getStr(1);
			TV.setText(last_weight_date);
			Log.i("format record", " Weight date is " + last_weight_date);
			//TODO 	need to find the location of the weight data here should be a loop with error fall out
			TV = (TextView) findViewById(R.id.last_weight);
			last_weight = dbh.getStr(3);
			TV.setText(last_weight);
		} catch (Exception e) {
			//	set last_weight to nothing found
			last_weight = "No Records";
			TV.setText(last_weight);
		}

		// Now we need to check and see if there is an alert for this sheep
//	   	Log.i("Alert Text is " , alert_text);
//		Now to test of the sheep has an alert and if so then display the alert & set the alerts button to red
		if (alert_text != null && !alert_text.isEmpty() && !alert_text.trim().isEmpty()) {
			// make the alert button red
			Button btn = (Button) findViewById(R.id.alert_btn);
			btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
			btn.setEnabled(true);
			//	testing whether I can put up an alert box here without issues
			showAlert(v);
		}
	}

	public void updateDatabase(View v) {
		TextView TV;
		String temp_string;
		Float trait11_data = 0.0f;
		int temp_integer;
		// Disable Update Database button and make it red to prevent getting 2 records at one time
		btn = (Button) findViewById(R.id.update_database_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
		btn.setEnabled(false);
		temp_string = "";
		int[] temp_date = new int[3];
		int new_year, new_month, new_day ;
		double temp_double = 0.0f;
		// If there is no sheep ID then drop out completely
		// thissheep_id is 0 if no sheep has been selected.

		if (thissheep_id != 0) {

			//	Get the date and time to enter into the database.
			String mytoday = Utilities.TodayIs();
			String mytime = Utilities.TimeIs();
			boxvaccine = (CheckBox) findViewById(R.id.checkBoxGiveVaccine);
			if (boxvaccine.isChecked()) {
				//	Go get which vaccine was selected in the spinner
				Log.i("Update Database ", "in 1st vaccine ");
				vaccine_spinner = (Spinner) findViewById(R.id.vaccine_spinner);
				which_vaccine = vaccine_spinner.getSelectedItemPosition();
				Log.i("Update Database ", "1st vaccine spinner position " + which_vaccine);
				which_vaccine = Integer.valueOf(which_vaccine);
				if (which_vaccine == 0) {
					//	Need to require a value for vaccine here
					//  Missing data so  display an alert 					
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.vaccine_fill_fields)
							.setTitle(R.string.vaccine_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					Log.i("vaccine ", String.valueOf(which_vaccine));
				}

				//	go update the database with a drug record for this vaccine and this sheep
				Log.i("vaccine spinner", " position is " + String.valueOf(which_vaccine));
				//	go update the database with a drug record for this vaccine and this sheep
				i = vaccine_id_drugid.get(which_vaccine);
				Log.i("vaccine id", " value is " + String.valueOf(i));
				// Go get drug location for the shot
				shot_loc = vaccine_loc_spinner.getSelectedItemPosition();
				Log.i("vaccine loc ", "spinner value is " + String.valueOf(shot_loc));
				shot_loc = Integer.valueOf(drug_location_id_order.get(shot_loc));
				Log.i("vaccine loc ", "id value is " + String.valueOf(shot_loc));
				if (shot_loc == 0) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.drug_loc_fill_fields)
							.setTitle(R.string.drug_loc_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					cmd = String.format("insert into sheep_drug_table (sheep_id, drug_id, drug_date_on," +
									" drug_time_on, drug_date_off, drug_time_off, drug_dosage, drug_location) values " +
									" (%s, '%s', '%s', '%s', '%s', '%s', '%s', %s) ", thissheep_id, i, mytoday, mytime,
							empty_string_field, empty_string_field, empty_string_field, shot_loc);
					Log.i("vaccine 1 ", "add to sheep_drug_table db cmd is " + cmd);
					dbh.exec(cmd);
					Log.i("vaccine 1 ", "after insert into sheep_drug_table");
					//	Need to update the alert to include the slaughter withdrawal for this vaccine
					cmd = String.format("Select units_table.units_name, user_meat_withdrawal from drug_table " +
							"inner join units_table on drug_table.meat_withdrawal_units = units_table.id_unitsid where id_drugid = %s ", i);
					Log.i("vaccine 1 ", "drug withdrawal db cmd is " + cmd);
					crsr = dbh.exec(cmd);
					cursor = (Cursor) crsr;
					// If withdrawal data query fails, we can't do this
					if (cursor.getCount() > 0) {
						cursor.moveToFirst();
						// If no slaughter date then don't need to update the alert
						if (dbh.getInt(1)!= 0) {
							//	Test for slaughter in days
//						Log.i("slaughter",  " unit is " + dbh.getStr(0));
							if (dbh.getStr(0).equals("Days")) {
//							Log.i("today is ", mytoday);
//							Log.i("days to add ", dbh.getStr(1));
//								temp_double = (int) Utilities.GetJulianDate();
//							Log.i("julian of today ",  String.valueOf(temp_double));
								//	have to add aplus 1 to get to the next day after the withdrawal has passed
								temp_double = (int) Utilities.GetJulianDate() + (dbh.getInt(1)) + 1;
//							Log.i("julian of slaughter ", String.valueOf(temp_double));
								temp_date = Utilities.fromJulian(temp_double);
								temp_string = temp_date[0] + "-" + Utilities.Make2Digits(temp_date[1]) + "-" + Utilities.Make2Digits(temp_date[2]);
//							Log.i("slaughter", " next slaughter time will be " + temp_string);
								temp_string = "No Slaughter until " + temp_string + " for vaccine.";
								Log.i("drug withdrawal ", " new alert to add is " + temp_string);
							}
							// TODO: 2020-05-21 Need to add in how to calculate for other units like weeks or months or even hours
							if (alert_text != null) {
								// modified to put the new alert on top and add a newline character.
								temp_string = temp_string + "\n" + alert_text;
								alert_text = temp_string;
							}
							cmd = String.format("update sheep_table set alert01 = '%s' where sheep_id =%d ", temp_string, thissheep_id);
							Log.i("vaccine 1 ", "update alerts before cmd " + cmd);
							dbh.exec(cmd);
							Log.i("vaccine 1 ", "update alerts after cmd " + cmd);
						}
					} else {
						Toast.makeText(getBaseContext(), "Vaccine withdrawal data not set", Toast.LENGTH_SHORT).show();
					}
				}
			}

			// second vaccine

			boxvaccine2 = (CheckBox) findViewById(R.id.checkBoxGiveVaccine2);
			if (boxvaccine2.isChecked()) {
				//	Go get which vaccine was selected in the spinner
				vaccine_spinner2 = (Spinner) findViewById(R.id.vaccine_spinner2);
				which_vaccine2 = vaccine_spinner2.getSelectedItemPosition();
				if (which_vaccine2 == 0) {
					//	Need to require a value for vaccine here
					//  Missing data so  display an alert
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.vaccine_fill_fields)
							.setTitle(R.string.vaccine_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					Log.i("vaccine ", String.valueOf(which_vaccine2));
				}

				//	go update the database with a drug record for this vaccine and this sheep
				i2 = vaccine_id_drugid2.get(which_vaccine2);
//				Log.i("vaccine id", " value is " + String.valueOf(i));
				// Go get drug location for the shot
				Log.i("vaccine id 2", " value is " + String.valueOf(i2));
				// Go get drug location for the shot
				shot_loc2 = vaccine_loc_spinner2.getSelectedItemPosition();
				Log.i("vaccine loc 2 ", "spinner value is " + String.valueOf(shot_loc2));
				shot_loc2 = Integer.valueOf(drug_location_id_order.get(shot_loc2));
				Log.i("vaccine loc 2 ", "id value is " + String.valueOf(shot_loc2));
				if (shot_loc2 == 0) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.drug_loc_fill_fields)
							.setTitle(R.string.drug_loc_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					cmd = String.format("insert into sheep_drug_table (sheep_id, drug_id, drug_date_on," +
									" drug_time_on, drug_date_off, drug_time_off, drug_dosage, drug_location) values " +
									" (%s, '%s', '%s', '%s', '%s', '%s', '%s', %s) ", thissheep_id, i2, mytoday, mytime,
							empty_string_field, empty_string_field, empty_string_field, shot_loc2);
					Log.i("add drug to ", "db cmd is " + cmd);
					dbh.exec(cmd);
					Log.i("add tag ", "after insert into sheep_drug_table");
					//	Need to update the alert to include the slaughter withdrawal for this vaccine
					cmd = String.format("Select units_table.units_name, user_meat_withdrawal from drug_table " +
							"inner join units_table on drug_table.meat_withdrawal_units = units_table.id_unitsid where id_drugid = %s ", i);
					Log.i("vaccine 2 ", "drug withdrawal db cmd is " + cmd);
					crsr = dbh.exec(cmd);
					cursor = (Cursor) crsr;
					// If withdrawal data query fails, we can't do this
					if (cursor.getCount() > 0) {
						cursor.moveToFirst();
						// If no slaughter date then don't need to update the alert
						if (dbh.getInt(1)!= 0) {
							//	Test for slaughter in days
//							Log.i("slaughter",  " unit is " + dbh.getStr(0));
							if (dbh.getStr(0).equals("Days")) {
//							Log.i("today is ", mytoday);
//							Log.i("days to add ", dbh.getStr(1));
								temp_double = (int) Utilities.GetJulianDate();
//							Log.i("julian of today ",  String.valueOf(temp_double));
								temp_double = (int) Utilities.GetJulianDate() + (dbh.getInt(1)) + 1;
//							Log.i("julian of slaughter ", String.valueOf(temp_double));
								temp_date = Utilities.fromJulian(temp_double);
								temp_string = temp_date[0] + "-" + Utilities.Make2Digits(temp_date[1]) + "-" + Utilities.Make2Digits(temp_date[2]);
//							Log.i("slaughter", " next slaughter time will be " + temp_string);
								temp_string = "No Slaughter until " + temp_string + " for vaccine.";
								Log.i("drug withdrawal ", " new alert to add is " + temp_string);
							}
							// TODO: 2020-05-21 Need to add in how to calculate for other units like weeks or months or even hours
							if (alert_text != null) {
								// modified to put the new alert on top and add a newline character.
								temp_string = temp_string + "\n" + alert_text;
								alert_text = temp_string;
							}
							cmd = String.format("update sheep_table set alert01 = '%s' where sheep_id =%d ", temp_string, thissheep_id);
							Log.i("vaccine 1 ", "update alerts before cmd " + cmd);
							dbh.exec(cmd);
							Log.i("vaccine 1 ", "update alerts after cmd " + cmd);
						}
					} else {
						Toast.makeText(getBaseContext(), "Vaccine withdrawal data not set", Toast.LENGTH_SHORT).show();
					}
				}
			}

			//	Need to figure out the id_drugid for what we are giving this sheep for wormer
			boxwormer = (CheckBox) findViewById(R.id.checkBoxGiveWormer);
			if (boxwormer.isChecked()) {
				//	Go get which wormer was selected in the spinner
				wormer_spinner = (Spinner) findViewById(R.id.wormer_spinner);
				which_wormer = wormer_spinner.getSelectedItemPosition();
				if (which_wormer == 0) {
					//	Need to require a value for wormer here
					//  Missing data so  display an alert 					
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.wormer_fill_fields)
							.setTitle(R.string.wormer_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					Log.i("wormer ", String.valueOf(which_wormer));
				}
//				Log.i("wormer spinner", " position is" + String.valueOf(which_wormer));
				//	go update the database with a drug record for this wormer and this sheep
				i = wormer_id_drugid.get(which_wormer);
				Log.i("wormer id", " value is " + String.valueOf(i));
//added must have location
				// Go get drug location for the wormer
				wormer_loc = worm_loc_spinner.getSelectedItemPosition();
				Log.i("wormer loc ", "spinner value is " + String.valueOf(wormer_loc));
//				wormer_loc = Integer.valueOf(drug_location_id_order.get(wormer_loc));
//				Log.i("wormer loc ", "id value is " + String.valueOf(wormer_loc));

				if (wormer_loc == 0) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.wormer_loc_fill_fields)
							.setTitle(R.string.wormer_loc_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					wormer_loc = Integer.valueOf(drug_location_id_order.get(wormer_loc));
					Log.i("wormer loc ", "id value is " + String.valueOf(wormer_loc));
					cmd = String.format("insert into sheep_drug_table (sheep_id, drug_id, drug_date_on," +
									" drug_time_on, drug_date_off, drug_time_off, drug_dosage, drug_location) values " +
									" (%s, '%s', '%s', '%s', '%s', '%s', '%s', %s) ", thissheep_id, i, mytoday, mytime,
							empty_string_field, empty_string_field, empty_string_field, wormer_loc);
					Log.i("add drug to ", "db cmd is " + cmd);
					dbh.exec(cmd);
					Log.i("add tag ", "after insert into sheep_drug_table");
					//	Need to update the alert to include the slaughter withdrawal for this wormer
					cmd = String.format("Select units_table.units_name, user_meat_withdrawal from drug_table " +
							"inner join units_table on drug_table.meat_withdrawal_units = units_table.id_unitsid where id_drugid = %s", i);
					Log.i("drug withdrawal ", "db cmd is " + cmd);
					crsr = dbh.exec(cmd);
					cursor = (Cursor) crsr;
					if (cursor.getCount() > 0) {
						cursor.moveToFirst();
						// If no slaughter date then don't need to update the alert
						if (dbh.getInt(1)!= 0) {
							//	Test for slaughter in days
//							Log.i("slaughter",  " unit is " + dbh.getStr(0));
							if (dbh.getStr(0).equals("Days")) {
//							Log.i("today is ", mytoday);
//							Log.i("days to add ", dbh.getStr(1));
								temp_double = (int) Utilities.GetJulianDate();
//							Log.i("julian of today ",  String.valueOf(temp_double));
								temp_double = (int) Utilities.GetJulianDate() + (dbh.getInt(1)) + 1 ;
//							Log.i("julian of slaughter ", String.valueOf(temp_double));
								temp_date = Utilities.fromJulian(temp_double);
								temp_string = temp_date[0] + "-" + Utilities.Make2Digits(temp_date[1]) + "-" + Utilities.Make2Digits(temp_date[2]);
//							Log.i("slaughter", " next slaughter time will be " + temp_string);
								temp_string = "No Slaughter until " + temp_string + " for wormer.";
								Log.i("drug withdrawal ", " new alert to add is " + temp_string);
							}
							// TODO: 2020-05-21 Need to add in how to calculate for other units like weeks or months or even hours
							if (alert_text != null) {
								// modified to put the new alert on top and add a newline character.
								temp_string = temp_string + "\n" + alert_text;
								alert_text = temp_string;
							}
							cmd = String.format("update sheep_table set alert01 = '%s' where sheep_id =%d ", temp_string, thissheep_id);
							Log.i("vaccine 1 ", "update alerts before cmd " + cmd);
							dbh.exec(cmd);
							Log.i("vaccine 1 ", "update alerts after cmd " + cmd);
						}
					} else {
						Toast.makeText(getBaseContext(), "Wormer withdrawal data not set", Toast.LENGTH_SHORT).show();
					}
				}
			}

//			Get the value of the checkbox for shearing
			Log.i("before checkbox", " getting ready to get shorn or not ");
			boxshear = (CheckBox) findViewById(R.id.checkBoxShorn);
			// TODO
			if (boxshear.isChecked()) {
				//	go update the database with a shearing date and time add that as a note
				note_text = "";
				predefined_note01 = 23; // hard coded the code for shorn
				// TODO
				//	This will have to be changed for the general case where shearing is not item 23 in the list
				cmd = String.format("insert into sheep_note_table (sheep_id, note_text, note_date, note_time, id_predefinednotesid01) " +
						"values ( %s, '%s', '%s', '%s', %s )", thissheep_id, note_text, mytoday, mytime, predefined_note01);
				Log.i("update notes ", "before cmd " + cmd);
				dbh.exec(cmd);
				Log.i("update notes ", "after cmd exec");
				Log.i("shorn ", String.valueOf(boxshear));
			}

			//	Get the value of the checkbox for trim toes
			Log.i("before checkbox", " getting ready to get trim toes or not ");
			boxtrimtoes = (CheckBox) findViewById(R.id.checkBoxTrimToes);
			if (boxtrimtoes.isChecked()) {
				//	go update the database with a toe trimming date and time add that as a note
				note_text = "";
				predefined_note01 = 14; // hard coded the code for toes trimmed
				// TODO
				//	This will have to be changed for the general case where toes is not item 14 in the list
				cmd = String.format("insert into sheep_note_table (sheep_id, note_text, note_date, note_time, id_predefinednotesid01) " +
						"values ( %s, '%s', '%s', '%s', %s )", thissheep_id, note_text, mytoday, mytime, predefined_note01);
				Log.i("update notes ", "before cmd " + cmd);
				dbh.exec(cmd);
				Log.i("update notes ", "after cmd exec");
				Log.i("toes trimmed ", String.valueOf(boxtrimtoes));
			}

			//	Get the value of the checkbox for weaned
			Log.i("before checkbox", " getting ready to get weaned or not ");
			boxweaned = (CheckBox) findViewById(R.id.checkBoxWeaned);
			if (boxweaned.isChecked()) {
				//	go update the database with a weaned date
				cmd = String.format("update sheep_table set weaned_date = '%s' where sheep_id = %d ", mytoday, thissheep_id);
				Log.i("update sheep table ", "before cmd " + cmd);
				dbh.exec(cmd);
				Log.i("update sheep table ", "after cmd exec");
				// TODO: 2020-05-22
				//  	This will crash if the sheep to be weaned was not born here and
				//  	does not have a lambing history record. Not sure how it should be fixed.
				cmd = String.format("select id_lambinghistoryid from lambs_born_table where sheep_id = '%s'", thissheep_id);
				Log.i("format record", " cmd is " + cmd);
				crsr6 = dbh.exec(cmd);
				cursor6 = (Cursor) crsr6;
				cursor6.moveToFirst();
				sheep_birth_record = dbh.getInt(0);
				Log.i("format record", " Birth Record is " + String.valueOf(sheep_birth_record));
				// update the lambing history table number lambs weaned
				cmd = String.format("Select lambing_history_table.lambs_weaned from lambing_history_table " +
						" where id_lambinghistoryid = %s", sheep_birth_record);
				Log.i("lambing history ", "db cmd is " + cmd);
				crsr = dbh.exec(cmd);
				cursor = (Cursor) crsr;
				cursor.moveToFirst();
				lambs_weaned = dbh.getInt(0);
				Log.i("update lambing history ", String.valueOf(lambs_weaned));
				lambs_weaned = lambs_weaned + 1;
				Log.i("update lambing history ", String.valueOf(lambs_weaned));
				cmd = String.format("update lambing_history_table set lambs_weaned = %d " +
						" where id_lambinghistoryid =%d ", lambs_weaned, sheep_birth_record);
				dbh.exec(cmd);
			}

//			//	Get the value of the checkbox for take  blood
//			Log.i("before checkbox", " getting ready to see if we collected blood or not ");
			boxblood = (CheckBox) findViewById(R.id.checkBoxBlood);
			if (boxblood.isChecked()) {
				blood_spinner = (Spinner) findViewById(R.id.blood_spinner);
				which_blood = blood_spinner.getSelectedItemPosition();
				if (which_blood == 0) {
					//	Need to require a value for blood here
					//  Missing data so  display an alert
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.blood_fill_fields)
							.setTitle(R.string.blood_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					Log.i("blood ", String.valueOf(which_blood));
				}
				//	go update the database with blood pull date and time as a note
				note_text = "Draw Blood for ";
				// Go get the reason for the blood test
				String temp_text = blood_tests.get(which_blood);
				Log.i("blood test", " value is " + temp_text);
				note_text = note_text + temp_text;
				predefined_note01 = 47; // hard coded the code for blood sample taken
				cmd = String.format("insert into sheep_note_table (sheep_id, note_text, note_date, note_time, id_predefinednotesid01) " +
						"values ( %s, '%s', '%s', '%s', %s )", thissheep_id, note_text, mytoday, mytime, predefined_note01);
				Log.i("update notes ", "before cmd " + cmd);
				dbh.exec(cmd);
				Log.i("update notes ", "after cmd exec");
				Log.i("blood taken ", String.valueOf(boxblood));
				try {
					//	Update the sheep record to remove the scrapie blood in alert
					alert_text = alert_text.replace("Scrapie Blood Codon 171", "");
					cmd = String.format("update sheep_table set alert01 = '%s' where sheep_id =%d ", alert_text, thissheep_id);
//				Log.i("update alerts ", "before cmd " + cmd);
					dbh.exec(cmd);
//				Log.i("update alerts ", "after cmd " + cmd);
				} catch (Exception e) {
					Log.w("scrapie", "No Codon 171 scrapie alert");
				}
				try {
					//	Update the sheep record to remove the scrapie blood in alert
					alert_text = alert_text.replace("Scrapie Blood Codon 154", "");
					cmd = String.format("update sheep_table set alert01 = '%s' where sheep_id =%d ", alert_text, thissheep_id);
//				Log.i("update alerts ", "before cmd " + cmd);
					dbh.exec(cmd);
//				Log.i("update alerts ", "after cmd " + cmd);
				} catch (Exception e) {
					Log.w("scrapie", "No Codon 154 scrapie alert");
				}
				try {
					//	Update the sheep record to remove the scrapie blood in alert
					alert_text = alert_text.replace("Scrapie Blood Codon 136", "");
					cmd = String.format("update sheep_table set alert01 = '%s' where sheep_id =%d ", alert_text, thissheep_id);
//				Log.i("update alerts ", "before cmd " + cmd);
					dbh.exec(cmd);
//				Log.i("update alerts ", "after cmd " + cmd);
				} catch (Exception e) {
					Log.w("scrapie", "No Codon 136 scrapie alert");
				}
				try {
					//	Update the sheep record to remove the Brucellosis blood in alert
					alert_text = alert_text.replace("Brucellosis Blood", "");
					cmd = String.format("update sheep_table set alert01 = '%s' where sheep_id =%d ", alert_text, thissheep_id);

//					Log.i("update alerts ", "before cmd " + cmd);
					dbh.exec(cmd);
//					Log.i("update alerts ", "after cmd " + cmd);
				} catch (Exception e) {
					Log.w("scrapie", "No Brucellosis alert");
				}
				try {
					//	Update the sheep record to remove the OPP blood in alert
					alert_text = alert_text.replace("OPP Blood", "");
					cmd = String.format("update sheep_table set alert01 = '%s' where sheep_id =%d ", alert_text, thissheep_id);
//					Log.i("update alerts ", "before cmd " + cmd);
					dbh.exec(cmd);
//					Log.i("update alerts ", "after cmd " + cmd);
				} catch (Exception e) {
					Log.w("scrapie", "No OPP alert");
				}
			}

			//	Give a drug if checked
			boxdrug = (CheckBox) findViewById(R.id.checkBoxGiveDrug);
			if (boxdrug.isChecked()) {
				//	Go get which drug was selected in the spinner
				drug_spinner = (Spinner) findViewById(R.id.drug_spinner);
				which_drug = drug_spinner.getSelectedItemPosition();
				if (which_drug == 0) {
					//	Need to require a value for drug here
					//  Missing data so  display an alert 					
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.drug_fill_fields)
							.setTitle(R.string.drug_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					Log.i("drug ", String.valueOf(which_drug));
				}
				//	go update the database with a drug record for this drug and this sheep
				// The drug_id is at the same position in the drug list as the spinner position
				i = drug_id_drugid.get(which_drug);
				Log.i("drug id", " value is " + String.valueOf(i));
				//	Go get a Drug location 
				drug_location_spinner = (Spinner) findViewById(R.id.drug_location_spinner);
				drug_loc = drug_location_spinner.getSelectedItemPosition();

				Log.i("drug loc ", "spinner value is " + String.valueOf(drug_loc));
				drug_loc = Integer.valueOf(drug_location_id_order.get(drug_loc));
				Log.i("drug loc ", "id value is " + String.valueOf(drug_loc));
				if (drug_loc == 0) {
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.drug_loc_fill_fields)
							.setTitle(R.string.drug_loc_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					cmd = String.format("insert into sheep_drug_table (sheep_id, drug_id, drug_date_on," +
									" drug_time_on, drug_date_off, drug_time_off, drug_dosage, drug_location) values " +
									" (%s, '%s', '%s', '%s', '%s', '%s', '%s', %s) ", thissheep_id, i, mytoday, mytime,
							empty_string_field, empty_string_field, empty_string_field, drug_loc);
					Log.i("add drug to ", "db cmd is " + cmd);
					dbh.exec(cmd);
					Log.i("add tag ", "after insert into sheep_drug_table");
					//	Need to update the alert to include the slaughter withdrawal for this drug
					cmd = String.format("Select units_table.units_name, user_meat_withdrawal from drug_table " +
							"inner join units_table on drug_table.meat_withdrawal_units = units_table.id_unitsid where id_drugid = %s", i);
					Log.i("drug withdrawal ", "db cmd is " + cmd);
					crsr = dbh.exec(cmd);
					if (cursor.getCount() > 0) {
						cursor = (Cursor) crsr;
						cursor.moveToFirst();
						// If no slaughter date then don't need to update the alert
						if (dbh.getInt(1)!= 0) {
							//	Test for slaughter in days
//							Log.i("slaughter",  " unit is " + dbh.getStr(0));
							if (dbh.getStr(0).equals("Days")) {
//							Log.i("today is ", mytoday);
//							Log.i("days to add ", dbh.getStr(1));
								temp_double = (int) Utilities.GetJulianDate();
//							Log.i("julian of today ",  String.valueOf(temp_double));
								temp_double = (int) Utilities.GetJulianDate() + (dbh.getInt(1)) + 1;
//							Log.i("julian of slaughter ", String.valueOf(temp_double));
								temp_date = Utilities.fromJulian(temp_double);
								temp_string = temp_date[0] + "-" + Utilities.Make2Digits(temp_date[1]) + "-" + Utilities.Make2Digits(temp_date[2]);
//							Log.i("slaughter", " next slaughter time will be " + temp_string);
								temp_string = "No Slaughter until " + temp_string + " for drug.";
								Log.i("drug withdrawal ", " new alert to add is " + temp_string);
							}
							// TODO: 2020-05-21 Need to add in how to calculate for other units like weeks or months or even hours
							if (alert_text != null) {
								// modified to put the new alert on top and add a newline character.
								temp_string = temp_string + "\n" + alert_text;
								alert_text = temp_string;
							}
							cmd = String.format("update sheep_table set alert01 = '%s' where sheep_id =%d ", temp_string, thissheep_id);
							Log.i("vaccine 1 ", "update alerts before cmd " + cmd);
							dbh.exec(cmd);
							Log.i("vaccine 1 ", "update alerts after cmd " + cmd);
						}
					} else {
						Log.w("Withdrawal: Drug", "No withdrawal data in db");
					}
// TODO: 2020-05-21  get and add Drug Reason
					// Put this back into the sheep_management.xml file when I add in reasons
					// Go get the drug dose if there is one and add as a sheep note
					TV = findViewById(R.id.drug_dose_data);
					String drug_dose_note = TV.getText().toString();
					cmd = String.format("insert into sheep_note_table (sheep_id, note_text, note_date, note_time, id_predefinednotesid01) " +
							"values ( %s, '%s', '%s', '%s', ' ' )", thissheep_id, drug_dose_note, mytoday, mytime);
					Log.i("update notes ", "before cmd " + cmd);
					dbh.exec(cmd);
					Log.i("update notes ", "after cmd exec");
				}
			}

			//	Remove a drug if checked
			//	Only valid for things like CIDRs and Sponges
			// TODO: 2020-05-21 this isn't working with the new display order options
			boxremovedrug = (CheckBox) findViewById(R.id.checkBoxRemoveDrug);
			if (boxremovedrug.isChecked()) {
				//	Go get which drug was selected in the spinner
				drug_spinner = (Spinner) findViewById(R.id.drug_spinner);
				which_drug = drug_spinner.getSelectedItemPosition();

				// The drug_id is at the same position in the drug_id_drugid list as the spinner position			
				i = drug_id_drugid.get(which_drug);
				Log.i("drug id", " value is " + String.valueOf(i));

				//	Go find the instance of this drug with no remove date for this sheep
				cmd = String.format("select id_sheepdrugid from sheep_drug_table where " +
						" sheep_id = %s and drug_id = %s and drug_date_off = '' ", thissheep_id, i);
				Log.i("remove drug to ", "db cmd is " + cmd);
				crsr = dbh.exec(cmd);
				cursor = (Cursor) crsr;
				if (cursor.getCount() > 0) {
					cursor.moveToFirst();
					id_sheepdrugid = dbh.getInt(0);
					Log.i("drug record is ", String.valueOf(id_sheepdrugid));
					Log.i("today is ", mytoday);
					Log.i("remove drug ", "before update the sheep_drug_table");
					cmd = String.format("update sheep_drug_table set drug_date_off = '%s', " +
							"drug_time_off = '%s' where id_sheepdrugid = %s", mytoday, mytime, id_sheepdrugid);
					Log.i("remove drug to ", "db cmd is " + cmd);
					dbh.exec(cmd);
				} else {
					Toast.makeText(getBaseContext(), "This drug is not eligible for removal", Toast.LENGTH_SHORT).show();
					Log.w("Removal: Drug", "No removable instance of drug found");
				}
				Log.i("add tag ", "after update sheep_drug_table with remove date");
			}

			//	Take a weight if checked
			boxweight = (CheckBox) findViewById(R.id.checkBoxTakeWeight);
			if (boxweight.isChecked()) {
				//	get a sheep weight
				TV = (TextView) findViewById(R.id.trait11_data);
				temp_string = TV.getText().toString();
				if (TextUtils.isEmpty(temp_string)) {
					// EditText was empty
					// so no real data collected just break out
					trait11_data = 0.0f;
//			    			Log.i("save trait11", "float data is " + String.valueOf(trait11_data));
				} else {
					trait11_data = Float.valueOf(TV.getText().toString());
					Log.i("save trait11", "float data is " + String.valueOf(trait11_data));
				}
				// Calculate the age in days for this sheep for this evaluation to fill the age_in_days field
				cmd = String.format("Select julianday(birth_date) from sheep_table where sheep_id = '%s'", thissheep_id);
				Log.i("get birthdate eval ", cmd);
				dbh.exec(cmd);
				tempcrsr = dbh.exec(cmd);
				tempcursor = (Cursor) tempcrsr;
				dbh.moveToFirstRecord();
				temp_integer = (int) Utilities.GetJulianDate() - (dbh.getInt(0));
				Log.i("get age in days ", String.valueOf(temp_integer));

				//	go update the database with a sheep evaluation record for this weight and this sheep
				cmd = String.format("insert into sheep_evaluation_table (sheep_id, " +
								"trait_name01, trait_score01, trait_name02, trait_score02, trait_name03, trait_score03, " +
								"trait_name04, trait_score04, trait_name05, trait_score05, trait_name06, trait_score06," +
								"trait_name07, trait_score07, trait_name08, trait_score08, trait_name09, trait_score09, " +
								"trait_name10, trait_score10, trait_name11, trait_score11, trait_name12, trait_score12, " +
								"trait_name13, trait_score13, trait_name14, trait_score14, trait_name15, trait_score15, " +
								"trait_name16, trait_score16, trait_name17, trait_score17, trait_name18, trait_score18, " +
								"trait_name19, trait_score19, trait_name20, trait_score20, " +
								"trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, eval_date, eval_time, age_in_days) " +
								"values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s," +
								"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'%s','%s', %s) ",
						thissheep_id, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0,
						0, 0, 16, trait11_data, 0, 0,
						0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0,
						0, 0, 0, 0,
						1, 0, 0, 0, 0, mytoday, mytime, temp_integer);
				Log.i("add evaluation ", "cmd is " + cmd);
				dbh.exec(cmd);
				Log.i("add evaluation ", "after insert into sheep_evaluation_table");
			}
//			clearBtn( null );
		} else {
//	    		no sheep ID need to throw up an alert box
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.no_sheep_id)
					.setTitle(R.string.no_sheep_id);
			builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int idx) {
					// User clicked OK button
					// make update database button normal and enabled so we can try again
					btn = (Button) findViewById(R.id.update_database_btn);
					btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
					btn.setEnabled(true);
					return;
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
			return;

		}
		boxprintlabel = (CheckBox) findViewById(R.id.checkBoxPrintLabel);
		if (boxprintlabel.isChecked()) {
			btn = (Button) findViewById(R.id.print_label_btn);
			btn.performClick();
		}
	}

	public void printLabel(View v) {
		try {
			String[] lines = EID.split("\n"); // works for both
			String contents = LastEID.substring(0, 3) + LastEID.substring(4, 16);
			Log.i("PrintLabel btn ", " contents " + contents);
			Intent encodeIntent = new Intent("weyr.LT.ENCODE");
			encodeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			encodeIntent.addCategory(Intent.CATEGORY_DEFAULT);
			encodeIntent.putExtra("ENCODE_FORMAT", "CODE_128");
			encodeIntent.putExtra("ENCODE_SHOW_CONTENTS", false);
			encodeIntent.putExtra("ENCODE_DATA", contents);
			encodeIntent.putExtra("ENCODE_AUTOPRINT", "false");

			if (AutoPrint) {
				encodeIntent.putExtra("ENCODE_AUTOPRINT", "true");
				Log.i("PrintLabel btn ", " autoprint is true ");
			}
			;
			encodeIntent.putExtra("ENCODE_DATA1", LabelText);
			encodeIntent.putExtra("ENCODE_DATE", Utilities.TodayIs() + "  " + Utilities.TimeIs());
			Log.i("PrintLabel btn ", " before put extra sheepName ");
			encodeIntent.putExtra("ENCODE_SHEEPNAME", SheepName);
			Log.i("PrintLabel btn ", " after put extra sheepName " + SheepName);
			startActivity(encodeIntent);
			Log.i("PrintLabel btn ", " after start activity encode ");
		} catch (Exception r) {
			Log.v("PrintLabel ", " in sheep management RunTimeException: " + r);
		}
	}

	public void backBtn(View v) {
		doUnbindService();
		stopService(new Intent(SheepManagement.this, eidService.class));
		doUnbindScaleService();
		stopService(new Intent(SheepManagement.this, scaleService.class));
		// Added this to close the database if we go back to the main activity
		try {
			cursor.close();
		} catch (Exception r) {
			Log.i("back btn", "cursor RunTimeException: " + r);
		}
		try {
			sirecursor.close();
		} catch (Exception r) {
			Log.i("back btn", "sirecursor RunTimeException: " + r);
		}
		try {
			damcursor.close();
		} catch (Exception r) {
			Log.i("back btn", " cursor3 RunTimeException: " + r);
		}
		dbh.closeDB();
		//Go back to main
		this.finish();
	}

	public void showAlert(View v) {
		String alert_text;
		String dbname = getString(R.string.real_database_file);
		String cmd;
		Object crsr;
		// Display alerts here   	
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		cmd = String.format("select sheep_table.alert01 from sheep_table where sheep_id =%d", thissheep_id);
		Log.i("SheepMgmntGetAlert ", cmd);
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		alert_text = (dbh.getStr(0));
		Log.i("SheepMgmntShowAlert ", alert_text);
		builder.setMessage(alert_text)
				.setTitle(R.string.alert_warning);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int idx) {
				// User clicked OK button
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public void clearBtn(View v) {
		TextView TV;
		TV = (TextView) findViewById(R.id.inputText);
		TV.setText("");
		TV = (TextView) findViewById(R.id.sheepnameText);
		TV.setText("");
		TV = (TextView) findViewById(R.id.sireName);
		TV.setText("");
		TV = (TextView) findViewById(R.id.damName);
		TV.setText("");
		//	Need to clear out the rest of the tags here 
		Log.i("clear btn", "before changing myadapter");
		try {
			myadapter.changeCursor(null);
		} catch (Exception e) {
			// In this case there is no adapter so do nothing
		}
		boxvaccine = (CheckBox) findViewById(R.id.checkBoxGiveVaccine);
		boxvaccine.setChecked(false);
		boxvaccine2 = (CheckBox) findViewById(R.id.checkBoxGiveVaccine2);
		boxvaccine2.setChecked(false);
//		Log.i("clear btn", "after clear vaccine checkbox");
		boxwormer = (CheckBox) findViewById(R.id.checkBoxGiveWormer);
		boxwormer.setChecked(false);
		boxshear = (CheckBox) findViewById(R.id.checkBoxShorn);
		boxshear.setChecked(false);
//		Log.i("clear btn", "after clear wormer checkbox");
		boxtrimtoes = (CheckBox) findViewById(R.id.checkBoxTrimToes);
		boxtrimtoes.setChecked(false);
//		Log.i("clear btn", "after clear trim toes checkbox");
		boxblood = (CheckBox) findViewById(R.id.checkBoxBlood);
		boxblood.setChecked(false);
//		Log.i("clear btn", "after blood checkbox");	
		boxweaned = (CheckBox) findViewById(R.id.checkBoxWeaned);
		boxweaned.setChecked(false);
		boxremovedrug = (CheckBox) findViewById(R.id.checkBoxRemoveDrug);
		boxremovedrug.setChecked(false);
		boxdrug = (CheckBox) findViewById(R.id.checkBoxGiveDrug);
		boxdrug.setChecked(false);
		drugdose = (TextView) findViewById(R.id.drug_dose_data);

//		Log.i("clear btn", "after clear drug checkbox");
		boxweight = (CheckBox) findViewById(R.id.checkBoxTakeWeight);
		boxweight.setChecked(false);
		TV = (TextView) findViewById(R.id.trait11_data);
		TV.setText("");

		TV = (TextView) findViewById(R.id.last_weight_date);
		TV.setText("");
		TV = (TextView) findViewById(R.id.last_weight);
		TV.setText("");

		// Enable Update Database button and make it normal and red
		btn = (Button) findViewById(R.id.update_database_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFFCC0000));
		btn.setEnabled(true);

		// make the alert button normal and disabled and red
		btn = (Button) findViewById(R.id.alert_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
		btn.setEnabled(false);

		// make the print label button normal and disabled and red
		btn = (Button) findViewById(R.id.print_label_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
		btn.setEnabled(false);
	}

	@Override
	public void onResume() {
		super.onResume();
		CheckIfServiceIsRunning();
		Log.i("SheepMgmt", " OnResume");
//		scanEid(null);
//		Log.i("SheepMgmt", " OnResume after scanEID(null)");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.i("SheepMgmt", " OnPause");
		doUnbindService();
		doUnbindScaleService();
	}

	public void helpBtn(View v) {
		// Display help here
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.help_management)
				.setTitle(R.string.help_warning);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int idx) {
				// User clicked OK button

			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public boolean tableExists(String table) {
		try {
			dbh.exec("select * from " + table);
			return true;
		} catch (SQLiteException e) {
			return false;
		}
	}

	private void addRadioButtons(int numButtons, String[] radioBtnText) {
		int i;

		for (i = 0; i < numButtons; i++) {
			//instantiate...
			RadioButton radioBtn = new RadioButton(this);

			//set the values that you would otherwise hardcode in the xml...
			radioBtn.setLayoutParams
					(new LayoutParams
							(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			//label the button...
			radioBtn.setText(radioBtnText[i]);
//			  	  	Log.i("addradiobuttons", radioBtnText[i]);
			radioBtn.setId(i);

			//add it to the group.
			radioGroup.addView(radioBtn, i);
		}
	}

	private void addRadioButtons2(int numButtons, String[] radioBtnText) {
		int i;

		for (i = 0; i < numButtons; i++) {
			//instantiate...
			RadioButton radioBtn = new RadioButton(this);

			//set the values that you would otherwise hardcode in the xml...
			radioBtn.setLayoutParams
					(new LayoutParams
							(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			//label the button...
			radioBtn.setText(radioBtnText[i]);
//			  	  	Log.i("addradiobuttons", radioBtnText[i]);
			radioBtn.setId(i);

			//add it to the group.
			radioGroup2.addView(radioBtn, i);
		}
	}

	//  user clicked 'Scan' button
	public void scanEid(View v) {
		// Here is where I need to get a tag scanned and put the data into the variable LastEID
		clearBtn(v);
		tag_type_spinner.setSelection(1);
		Log.i("in ScanEID", " after set tag_type_spinner ");
		if (mService != null) {
			try {
				//Start eidService sending tags
				Message msg = Message.obtain(null, eidService.MSG_SEND_ME_TAGS);
				msg.replyTo = mMessenger;
				mService.send(msg);
				//	make the scan eid button  0x0000FF00, 0xff00ff00
				Button btn = (Button) findViewById(R.id.scan_eid_btn);
				btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		} else {
//						Log.i("in ScanEID", " mService is null " );
		}
	}

	public void scanWeight(View v) {

		if (mScaleService != null) {
			try {
				//Start scaleService sending tags
				Message msg = Message.obtain(null, scaleService.MSG_SEND_ME_TAGS);
				msg.replyTo = mMessenger;
				mScaleService.send(msg);
				// Here is where to get the scanned weight
				//	make the scan weight button  0x0000FF00, 0xff00ff00
				Button btn = (Button) findViewById(R.id.scan_weight_btn);
				btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));
				boxweight = (CheckBox) findViewById(R.id.checkBoxTakeWeight);
				boxweight.setChecked(true);
			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		} else {
//					 Log.i("in ScanWeight", " ready for data " );
		}
	}

	// user clicked the "next record" button
	public void nextRecord(View v) {
		//	Clear out the display first
		clearBtn(v);
		//	Go get the sheep id of this record
		Log.i("in next record", "this sheep ID is " + String.valueOf(thissheep_id));
		sheepcursor.moveToNext();
		Log.i("in next record", "after moving the cursor ");
		thissheep_id = sheepcursor.getInt(0);
		Log.i("in next record", "this sheep ID is " + String.valueOf(thissheep_id));
		recNo += 1;
		formatSheepRecord(v);
//		    		// I've moved forward so I need to enable the previous record button
		Button btn3 = (Button) findViewById(R.id.prev_rec_btn);
		btn3.setEnabled(true);
		if (recNo == (nRecs)) {
			// at end so disable next record button
			Button btn2 = (Button) findViewById(R.id.next_rec_btn);
			btn2.setEnabled(false);
		}
	}

	// user clicked the "previous record" button
	public void prevRecord(View v) {
//			    	Clear out the display first
		clearBtn(v);
		Log.i("in prev record", "this sheep ID is " + String.valueOf(thissheep_id));
		sheepcursor.moveToPrevious();
		Log.i("in prev record", "after moving the cursor5 ");
		thissheep_id = sheepcursor.getInt(0);
		Log.i("in prev record", "this sheep ID is " + String.valueOf(thissheep_id));
		recNo -= 1;
		formatSheepRecord(v);
		// I've moved back so enable the next record button
		Button btn2 = (Button) findViewById(R.id.next_rec_btn);
		btn2.setEnabled(true);
		if (recNo == 1) {
			// at beginning so disable prev record button
			Button btn3 = (Button) findViewById(R.id.prev_rec_btn);
			btn3.setEnabled(false);
		}
	}

	public void doNote(View v) {
		Utilities.takeNote(v, thissheep_id, this);
	}
}
