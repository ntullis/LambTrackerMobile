package com.weyr_associates.lambtracker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import android.widget.ArrayAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.LightingColorFilter;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class AddLamb extends Activity {
	private DatabaseHandler dbh;
	String     	cmd;
	String		tempLabel, tempText;
	Integer 	i;	
	public Button btn;
	public String alert_text;
	public Cursor 	cursor, cursor2;
	public Cursor 	 tagcursor;
	public Object 	crsr, crsr2;
	public Spinner tag_type_spinner, tag_type_spinner2, tag_location_spinner, tag_color_spinner, lamb_ease_spinner;
//	public List<String> user_evaluation_traits;
//	public List <Integer> user_trait_numbers, user_trait_number_items;
	public Spinner	rear_type_spinner;
	public List<String> tag_types, tag_locations, tag_colors, lambing_ease, rear_types;
	public List<Integer> lambeaseid;
	ArrayAdapter<String> dataAdapter;
//	public int nRecs3;
	public int nRecs4;
	public int 		thissheep_id;
	public int	rear_type, birth_type, sex, lambease;
	public String dam_name, sire_name, lamb_name;
	public String service_abbrev;
	public String lamb_alert_text, death_date ;
	public String nsip_id;
	public String weaned_date, inbreeding;
	public String tag_country_code, death_reason;
	public int   official_id, id_breederid, lambing_contact;
	public int dam_id, dam_codon171, dam_codon154, dam_codon136, dam_sheepbreed;
	public int sire_id, sire_codon171, sire_codon154, sire_codon136, sire_sheepbreed;
	public int lamb_id, lamb_codon171, lamb_codon154, lamb_codon136, lamb_sheepbreed ;
	public int flock_prefix, id_locationid, id_ownerid, management_group;
	public int	service_type, birth_weight_units, lamb_birth_record, correct_breeding_record;
	public CheckBox 	stillbornbox, markewebox;
	public boolean stillborn, markewe;
	public Float birth_weight;
	public int	real_gestation_length;
	public RadioGroup radioGroup, radioGroup2;
	public String mytoday;
	public String mytime;
	public Integer lambing_historyid, lambs_born;
	public String lambing_notes, lambing_date, lambing_time, sex_abbrev;
	int		fedtagid, farmtagid, eidtagid ; // These are record IDs not sheep IDs
	public int new_tag_type, new_tag_color, new_tag_location;
	public int farm_tag_color_male, farm_tag_color_female, eid_tag_color_male, eid_tag_color_female, federal_tag_color_male, federal_tag_color_female, paint_tag_color;
	public int federal_tag_location, eid_tag_location, farm_tag_location;
	public int farm_tag_on_eid_tag, farm_tag_number_digits_from_eid;
	public List<String> tag_types_display_order, tag_types_id_order;
	public List<String> tag_color_display_order, tag_color_id_order;
	public String tag_type_label, tag_color_label, tag_location_label, new_tag_number ;
	public String  eidText;
	public double	gestation_length, first_gestation_possible, last_gestation_possible, early_gestation_length, late_gestation_length;
	private int		nRecs;
	
	/////////////////////////////////////////////////////
	Messenger mService = null;
	boolean mIsBound;

	final Messenger mMessenger = new Messenger(new IncomingHandler());
	// variable to hold the string
	public String LastEID ;

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case eidService.MSG_UPDATE_STATUS:
				Bundle b1 = msg.getData();

				break;
			case eidService.MSG_NEW_EID_FOUND:
				Bundle b2 = msg.getData();

				LastEID = (b2.getString("info1"));
				//We have a good whole EID number	
				Log.i ("in handler case" , "got eid of " + LastEID);
				gotEID ();	
				break;			
			case eidService.MSG_UPDATE_LOG_APPEND:
				//Bundle b3 = msg.getData();
				//Log.i("Convert", "Add to Log.");

				break;
			case eidService.MSG_UPDATE_LOG_FULL:
				//Log.i("Convert", "Log Full.");

				break;
			case eidService.MSG_THREAD_SUICIDE:
				Log.i("Convert", "Service informed Activity of Suicide.");
				doUnbindService();
				stopService(new Intent(AddLamb.this, eidService.class));

				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	public ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);
			Log.i("Convert", "At Service.");
			try {
				//Register client with service
				Message msg = Message.obtain(null, eidService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);

				//Request a status update.
				//msg = Message.obtain(null, eidService.MSG_UPDATE_STATUS, 0, 0);
				//				mService.send(msg);

				//Request full log from service.
				//				msg = Message.obtain(null, eidService.MSG_UPDATE_LOG_FULL, 0, 0);
				//				mService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been unexpectedly disconnected - process crashed.
			mService = null;
		}
	};    	

	private void CheckIfServiceIsRunning() {
		//If the service is running when the activity starts, we want to automatically bind to it.
		Log.i("Convert", "At isRunning?.");
		if (eidService.isRunning()) {
			//Log.i("Convert", "is.");
			doBindService();
		} else {
			//Log.i("Convert", "is not, start it");
			startService(new Intent(AddLamb.this, eidService.class));
			doBindService();
		}
		//Log.i("Convert", "Done isRunning.");
	} 	

	void doBindService() {
		// Establish a connection with the service.  We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.
		//Log.i("Convert", "At doBind1.");
		bindService(new Intent(this, eidService.class), mConnection, Context.BIND_AUTO_CREATE);
		//Log.i("Convert", "At doBind2.");

		mIsBound = true;

		if (mService != null) {
			//Log.i("Convert", "At doBind3.");
			try {
				//Request status update
				Message msg = Message.obtain(null, eidService.MSG_UPDATE_STATUS, 0, 0);
				msg.replyTo = mMessenger;
				mService.send(msg);
				Log.i("Convert", "At doBind4.");
				//Request full log from service.
				msg = Message.obtain(null, eidService.MSG_UPDATE_LOG_FULL, 0, 0);
				mService.send(msg);
			} catch (RemoteException e) {}
		}
		//Log.i("Convert", "At doBind5.");
	}
	void doUnbindService() {
		//Log.i("Convert", "At DoUnbindservice");
		if (mService != null) {
			try {
				//Stop eidService from sending tags
				Message msg = Message.obtain(null, eidService.MSG_NO_TAGS_PLEASE);
				msg.replyTo = mMessenger;
				mService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
		if (mIsBound) {
			// If we have received the service, and hence registered with it, then now is the time to unregister.
			if (mService != null) {
				try {
					Message msg = Message.obtain(null, eidService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service has crashed.
				}
			}
			// Detach our existing connection.
			unbindService(mConnection);
			mIsBound = false;
		}
	}    	

	public void gotEID( )
	{		
		View v = null;
		//	make the scan eid button red
		btn = (Button) findViewById( R.id.scan_eid_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));

		Log.i("goteid", "before go to add new tag" );
		addNewTag(v );
		Log.i("goteid", "after add new tag" );
		// 	Display the EID number
		TextView TV = (TextView) findViewById (R.id.inputText);
		TV.setText( LastEID );
		Log.i("in gotEID ", "with LastEID of " + LastEID);
				
	  	// Put the EID Tag data into the add tag section of the screen
	  	tag_type_spinner2 = (Spinner) findViewById(R.id.tag_type_spinner2);
	  	tag_color_spinner = (Spinner) findViewById(R.id.tag_color_spinner);
	  	tag_location_spinner = (Spinner) findViewById(R.id.tag_location_spinner);
	  	TV = (TextView) findViewById( R.id.new_tag_number );
	  	TV.setText( LastEID );
	  	//	Set tag type, color and location to defaults for EID tags
	  	//	Electronic, Yellow, Left Ear
	  	tag_type_spinner2.setSelection(1);
//	  	Log.i("updateTag", "Tag type is " + tag_type_label);
	  	tag_color_spinner.setSelection(eid_tag_color_male);
//	  	Log.i("updateTag", "Tag color is " + tag_color_label);
	  	tag_location_spinner.setSelection(eid_tag_location);
//	  	Log.i("updateTag", "Tag location is " + tag_location_label);
	  	// Put the last EID into the new tag number
	  	new_tag_number = LastEID;	  	
	}	
	/////////////////////////////////////////////////////
	
	@Override
    public void onCreate(Bundle savedInstanceState)	
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_lamb);
        Log.i("AddLamb", " after set content view");
        View v = null;
        String 	dbfile = getString(R.string.real_database_file) ;
        Log.i("AddLamb", " after get database file");
    	dbh = new DatabaseHandler( this, dbfile );
    	Object crsr;
    	ArrayList radiobtnlist, radiobtnsucklist;
    	String[] radioBtnText, radioBtnSuckText;
       	TextView TV;
		int tempint;
       	float temp_ram_in, temp_ram_out;
       	double temp_julian_today;
       	int [] jintdate = new int[] {0,0,0};
		String text_date_ram_out;
   	 	////////////////////////////////////
		CheckIfServiceIsRunning();
		Log.i("Convert", "back from isRunning");  	
		////////////////////////////////////

		//  Fill in default values
		filldefaults (v);

		// Fill the Tag Type Spinner
		tag_type_spinner = (Spinner) findViewById(R.id.tag_type_spinner);
		tag_types = new ArrayList<String>();
		tag_types_id_order= new ArrayList<String>();
		tag_types_display_order = new ArrayList<String>();
		// Select All fields from id types to build the spinner
		cmd = "select * from id_type_table order by id_type_display_order";
		Log.i("fill tag spinner", "command is " + cmd);
		crsr = dbh.exec( cmd );
		tagcursor   = ( Cursor ) crsr;
		dbh.moveToFirstRecord();
		tag_types.add("Select a Type");
		tag_types_id_order.add("tag_types_id");
		tag_types_display_order.add("tag_type_display_order");
		Log.i("fill tag spinner", "added first option ");
		// looping through all rows and adding to list
		for (tagcursor.moveToFirst(); !tagcursor.isAfterLast(); tagcursor.moveToNext()){
			tag_types_id_order.add(tagcursor.getString(0));
			Log.i("LookUpSheep", "tag_types_id_order " + tagcursor.getString(0));
			tag_types.add(tagcursor.getString(1));
			Log.i("LookUpSheep", "tag_type name" + tagcursor.getString(1));
			tag_types_display_order.add(tagcursor.getString(3));
			Log.i("LookUpSheep", "tag_types_display_order " + tagcursor.getString(3));
		}
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, tag_types);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tag_type_spinner.setAdapter (dataAdapter);
		//	set initial tag type to look for to be electronic it's based on the display order
		tag_type_spinner.setSelection(1);

	    //	make the scan eid button red
	    btn = (Button) findViewById( R.id.scan_eid_btn );
	    btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
	
	    // make the alert button normal and disabled
	   	btn = (Button) findViewById( R.id.alert_btn );
	   	btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
	   	btn.setEnabled(false);  
	   	
		//	Disable the Next Record and Prev. Record button until we have multiple records
	   	btn = (Button) findViewById( R.id.next_rec_btn );
	   	btn.setEnabled(false); 
	   	btn = (Button) findViewById( R.id.prev_rec_btn );
	   	btn.setEnabled(false);
	
	 	//	Disable the Take Note button
		btn = (Button) findViewById( R.id.take_note );
		btn.setEnabled(false);
		
		//	Disable the Look Up Sheep Button
		btn = (Button) findViewById( R.id.look_up_sheep_btn );
		btn.setEnabled(false);
		
		//	Disable the bottom update tag button until we choose to add or update`
	   	btn = (Button) findViewById( R.id.update_display_btn );
		btn.setEnabled(false); 
	
		// Select All fields from sheep sex to fill the lamb sex radio group
		radiobtnlist = new ArrayList();
	   	cmd = "select * from sex_table";
	   	crsr = dbh.exec( cmd );  
		cursor   = ( Cursor ) crsr;
		tempint = dbh.getSize();
	   	dbh.moveToFirstRecord();
	    // looping through all rows and adding to list
	   	for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
	   		radiobtnlist.add(cursor.getString(1));
	   	}
	    radioBtnText = (String[]) radiobtnlist.toArray(new String [radiobtnlist.size()]);
		// Build the radio buttons here
		radioGroup = ((RadioGroup) findViewById(R.id.radioGroupSex));
		addRadioButtons(tempint, radioBtnText, 1);
		radiobtnlist.clear ();

		//	Fill the rear type spinner
		rear_type_spinner = (Spinner) findViewById(R.id.rear_type_spinner);
		rear_types = new ArrayList<String>();
		cmd = "select * from birth_type_table";
		crsr = dbh.exec( cmd );
		cursor   = ( Cursor ) crsr;
		tempint = dbh.getSize();
		dbh.moveToFirstRecord();
		rear_types.add("Select a Rear Type");
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
			rear_types.add(cursor.getString(1));
		}
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, rear_types);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		rear_type_spinner.setAdapter (dataAdapter);
		//	set initial tag type to look for to be name it's based on the display order
		// TODO: 2/25/19
		rear_type_spinner.setSelection(2);

    	//	Get the text for the lamb ease buttons  
		lamb_ease_spinner = (Spinner) findViewById(R.id.lamb_ease_spinner);
    	cmd = String.format("select custom_evaluation_traits_table.custom_evaluation_item, custom_evaluation_traits_table.id_custom_traitid " +
    			" from custom_evaluation_traits_table " +
    			" where custom_evaluation_traits_table.id_traitid = '%s' "+
    			" order by custom_evaluation_traits_table.custom_evaluation_order ASC ", 24);
    	//    	Log.i("evaluate2", " cmd is " + cmd);	    	
    	crsr = dbh.exec( cmd );
        cursor   = ( Cursor ) crsr;
        dbh.moveToFirstRecord();
        lambing_ease = new ArrayList<String>();
        lambing_ease.add("Lambing Ease");
        lambeaseid = new ArrayList <Integer>();
        lambeaseid.add(0);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
        	lambing_ease.add (cursor.getString(0));
//	    	Log.i("addlamb", " Lambing ease text is " + cursor.getString(0));
	    	lambeaseid.add (cursor.getInt(1));
//	    	Log.i("addlamb", " Lambing ease id is " + String.valueOf(cursor.getInt(1)));
    	}        

    	// Creating adapter for lambease spinner
     	dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, lambing_ease);
     	dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
     	lamb_ease_spinner.setAdapter (dataAdapter);
		lamb_ease_spinner.setSelection(0);
		Log.i("addlamb", " after set selection" );
		
		//	Fill birth_weight with 0 until we get a weight
		birth_weight = 0.0f;		
		//	Fill the lambs born and rear type fields to have no lamb until we start
	    lambs_born = 0;
		rear_type = 0;
		//	The lambing_history record is empty until we create or find one
		lambing_historyid = 0;
		//	Fill lambing_notes with empty string until we read them from the history
		lambing_notes = "";
		//	Fill the mark ewe box default to checked 
		markewebox = (CheckBox) findViewById(R.id.checkBoxMarkEwe);
		markewebox.setChecked (true);

			radiobtnsucklist = new ArrayList();
			cmd = String.format("select custom_evaluation_traits_table.custom_evaluation_item " +
					" from custom_evaluation_traits_table " +
					" where custom_evaluation_traits_table.id_traitid = '37' "+
					" order by custom_evaluation_traits_table.custom_evaluation_order ASC ");
	    	Log.i("evaluate2", " ready to get button text cmd is " + cmd);
			crsr = dbh.exec( cmd );
			cursor   = ( Cursor ) crsr;
			nRecs4    = dbh.getSize();
	        Log.i ("getting button", " text have " + String.valueOf(nRecs4) + " buttons to build");
			dbh.moveToFirstRecord();
		// looping through all rows and adding to list
			for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
				radiobtnsucklist.add (cursor.getString(0));
		    	Log.i("2nd for loop", " radio button text is " + cursor.getString(0));
			}

		radioBtnSuckText = (String[]) radiobtnsucklist.toArray(new String [radiobtnsucklist.size()]);
		// Build the radio buttons here
		radioGroup2 = ((RadioGroup) findViewById(R.id.radioGroupSuck));
		Log.i("before", " call to addRadioButtons " );
		addRadioButtons(nRecs4, radioBtnSuckText,2);
		Log.i("after ", " call to addRadioButtons " );
		radiobtnsucklist.clear ();

		Bundle extras = getIntent().getExtras();
		// get extras here from the lambing screen. Mostly ewe's data for scrapie genetics	
		if (extras!= null){
			dam_id = extras.getInt("dam_id");
			dam_name = extras.getString("dam_name");
			TV = (TextView) findViewById( R.id.damName );
            TV.setText(dam_name); 
            Log.i("add a lamb ", " dam is " + dam_name);
//			dam_sheepbreed = extras.getInt("sheepbreed");
//			Log.i("add a lamb ", String.format(" dam breed is %d", dam_sheepbreed));
//            dam_codon171 = extras.getInt("codon171");
//			Log.i("in add lamb activity", " dam 171 is "+ dam_codon171);
//            // If there is no data set dam_codon171 to unknown
//			if (dam_codon171 == 0 ) {
//				dam_codon171 = 6;
//			}
//            dam_codon154 = extras.getInt("codon154");
//			Log.i("in add lamb activity", " dam 154 is "+ dam_codon154);
//			if (dam_codon154 == 0 ) {
//				dam_codon154 = 1;
//			}
//            dam_codon136 = extras.getInt("codon136");
//			Log.i("in add lamb activity", " dam 136 is "+ dam_codon136);
//			if (dam_codon136 == 0 ) {
//				dam_codon136 = 6;
//			}
		}
		cmd = String.format( "select sheep_table.sheep_name, sheep_table.sheep_id,  " +
					" sheep_table.codon171, sheep_table.codon154, sheep_table.codon136 , sheep_table.id_sheepbreedid " +
					"from sheep_table  " +
					"where sheep_table.sheep_id ='%s' " +
					" ", dam_id);
			Log.i("addLamb", " comand is " + cmd);
			crsr = dbh.exec( cmd );
			Log.i("addLamb", " after run the command");
			cursor   = ( Cursor ) crsr;
			cursor.moveToFirst();
			Log.i("addLamb", " the cursor is of size " + String.valueOf(dbh.getSize()));

			//	Get the Codon 171, Codon 154 and Codon 136 values for this sheep
			dam_codon171 = dbh.getInt(2);
			Log.i("in add lamb activity", " dam 171 is "+ dam_codon171);
			dam_codon154 = dbh.getInt(3);
			Log.i("in add lamb activity", " dam 154 is "+ dam_codon154);
			dam_codon136 = dbh.getInt(4);
			Log.i("in add lamb activity", " dam 136 is "+ dam_codon136);
			dam_sheepbreed = dbh.getInt (5);
			Log.i("addLamb", "after get codon data ");



		//	Now need to figure out who the sire is based on date and breeding records.
		//  First put an empty string in as sire name
		sire_name = "Sire not found";
		//  Start to get the dates info to figure out who sire is based on gestation times
		Calendar calendar = Calendar.getInstance();
		Log.i("add a lamb ", " after getting a calendar");
			jintdate [0] = calendar.get(Calendar.YEAR);
			jintdate [1] = calendar.get(Calendar.MONTH) +1;
			jintdate [2] = calendar.get(Calendar.DAY_OF_MONTH);
		
//			Log.i("add a lamb ", " before getting julian of today");
		temp_julian_today = Utilities.toJulian(jintdate);
		Log.i("addlamb", " julian today is " + String.valueOf(temp_julian_today));

//  set breeding record to empty until we get one
		correct_breeding_record = 0;
		//	Then go get the breeding records for this ewe.
		cmd = String.format("select ewe_breeding_table.ewe_id, " +
				" ram_breeding_table.id_rambreedingid, " +
				" ram_breeding_table.ram_id, " +
				" sheep_table.sheep_name, " +
				" julianday(ram_breeding_table.date_ram_in), " +
    			" julianday(ram_breeding_table.date_ram_out),  " +
    			" ram_breeding_table.id_servicetypeid, " +
    			" service_type_table.service_abbrev " +
				" , date_ram_out " +
    			" from ram_breeding_table " +
    			" left outer join ewe_breeding_table on " +
    			" ewe_breeding_table.id_rambreedingid = ram_breeding_table.id_rambreedingid " +
    			" inner join sheep_table on sheep_id = ram_id " +
    			" inner join service_type_table on " +
    			" ram_breeding_table.id_servicetypeid = service_type_table.id_servicetypeid "+
    			" where ewe_breeding_table.ewe_id = '%s' ", dam_id);
    	Log.i("add a lamb ", " cmd is " + cmd);	    	
    	crsr = dbh.exec( cmd );
        cursor   = ( Cursor ) crsr;
        nRecs    = cursor.getCount();
        Log.i("addlamb", "number of breeding records is " + String.valueOf (nRecs));
        dbh.moveToFirstRecord();
	    //  loop to see which breeding record matches the lamb birth based on gestation
        if (nRecs > 0) {
	        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
	        	Log.i("addlamb", " in for loop checking breeding dates ");
	        	Log.i("addlamb", " in for loop this breeding record is cursor " + String.valueOf(cursor.getPosition()));	
	        	Log.i("addlamb", " in for loop this breeding record is id " + String.valueOf(dbh.getInt(1)));	
	        	Log.i("addlamb", " in top loop sire is " + sire_name);	
	        	Log.i("addlamb", " in top loop sire_id is " + String.valueOf(sire_id));	
	        	// Check the dates and see if this is the right record
	        	// Get the date ram in and date ram out
	        	temp_ram_in = dbh.getReal(4);
				text_date_ram_out = dbh.getStr(8) ;
	        	Log.i("addlamb", " julian ram in " + String.valueOf(temp_ram_in));
	        	temp_ram_out = dbh.getReal(5);
	        	//	if there is no ram out date then we can't figure out who the sire is.
				//	throw a notification and tell them to edit ram record
				if (text_date_ram_out == "" ){
					//	Need to require a value for ram out here
					//  Missing data so  display an alert
					AlertDialog.Builder builder = new AlertDialog.Builder( this );
					Log.i("in alert", " need to fix ram out date ");
					builder.setMessage( R.string.fix_ram_out_date )
							.setTitle( R.string.fix_ram_out_date );
					builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById( R.id.update_database_btn );
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
							btn.setEnabled(true);
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				}
				else {
					Log.i("ram out ", String.valueOf(temp_ram_out));
				}

	        	Log.i("addlamb", " julian ram out " + String.valueOf(temp_ram_out));
	        	//	Calculate the first possible and last possible for this breeding record
	        	//	Should make these dates a preference or settings in LambTracker
	        	first_gestation_possible = temp_ram_in + early_gestation_length;
	        	Log.i("addlamb", " julian first gestation is " + String.valueOf(first_gestation_possible));
	        	last_gestation_possible = temp_ram_out + late_gestation_length;
	        	Log.i("addlamb", " julian last gestation is " + String.valueOf(last_gestation_possible));        	
	        	// First calculate how many days gestation this is from date ram in
	        	gestation_length = temp_julian_today - temp_ram_in;
	        	Log.i("addlamb", " calculated gestation length is " + String.valueOf(gestation_length));
	        	if  (temp_julian_today > first_gestation_possible && temp_julian_today < last_gestation_possible) {
	        		//	This is the correct record so save the data and bump out
	        		Log.i("addlamb", " correct breeding record is this cursor " + String.valueOf(cursor.getPosition()));
	        		correct_breeding_record = dbh.getInt(1);
					sire_name = dbh.getStr(3);
	        		sire_id = dbh.getInt(2);
	        		service_type = dbh.getInt(6);
	        		service_abbrev = dbh.getStr(7);
	        		//	make the gestation length an integer so it can be added to the lambing history record. 
	        		real_gestation_length = (int)gestation_length;
	        		Log.i("addlamb", " gestation length is " + String.valueOf(real_gestation_length));
	        		Log.i("addLamb" , "before break out of for loop");
	        		break;
	        	}        	
        	// The sire we have is 
        	Log.i("addlamb", " in for loop sire is " + sire_name);	
        	Log.i("addlamb", " in for loop sire_id is " + String.valueOf(sire_id));	
	        }        
		//	Handle the sire data here
        TV = (TextView) findViewById( R.id.sireName );
        TV.setText(sire_name); 
        TV = (TextView) findViewById( R.id.serviceType );
        TV.setText(service_abbrev); 
        Log.i("addlamb", " after set display of sire name " + sire_name);
        //	Go get the sire Codon171,154 and 136 values
        if (sire_id == 0){
        	//	Sire not found so set codon values to unknown
            sire_codon171 = 6;
        	sire_codon154 = 1;
        	sire_codon136 = 6;
        }else{
	        cmd = String.format("select sheep_table.codon171, sheep_table.codon154, " +
	        		" sheep_table.codon136, sheep_table.id_sheepbreedid from sheep_table where sheep_id = '%s' ", sire_id);
	        Log.i("addlamb", " getting codon data cmd is " + cmd);	
	        crsr = dbh.exec( cmd );
	        cursor   = ( Cursor ) crsr;
	        dbh.moveToFirstRecord();
	        sire_codon171 = dbh.getInt(0);
	        Log.i("addlamb", " sire codon171 " + String.valueOf(sire_codon171));
	        if (sire_codon171 == 0 ) {
		        sire_codon171 = 6;
	        }
	        sire_codon154 = dbh.getInt(1);
	        Log.i("addlamb", " sire codon154 " + String.valueOf(sire_codon154));
	        if (sire_codon154 == 0 ) {
		        sire_codon154 = 1;
	        }
	        sire_codon136 = dbh.getInt(2);  
	        Log.i("addlamb", " sire codon136 " + String.valueOf(sire_codon136));
	        if (sire_codon136 == 0 ) {
		        sire_codon136 = 6;
	        }
	        sire_sheepbreed = dbh.getInt(3);
			Log.i("add a lamb ", String.format(" sire breed is %d", sire_sheepbreed));
		}
        }
		//	Set breed based on sire and dam breed
		// if different set breed to be Crossbred
		//	TODO 5/14/20
		// 		will need mods for flocks with crossbred in a different record i sheep_breed_table
		Log.i("before ", "set breed based on sire and dam");
		Log.i("add a lamb ", String.format(" dam breed is %d", dam_sheepbreed));
		Log.i("add a lamb ", String.format(" sire breed is %d", sire_sheepbreed));
		if (dam_sheepbreed == sire_sheepbreed){
			lamb_sheepbreed = dam_sheepbreed;
			Log.i("add a lamb ", String.format(" sheepbreedid is %d", lamb_sheepbreed));
		}else{
			lamb_sheepbreed = 2 ;
		}
	    //	For now set lamb scrapie codons ?? because we don't know what this lamb is
	    lamb_codon171 = 6;
	    lamb_codon154 = 1;
	    lamb_codon136 = 6;

	    //  Go get the actual lamb codons based on sire and dam
        calculatecodon171 (v);
	    calculatecodon136 (v);
	    calculatecodon154 (v);
		Log.i("addlamb", " sire codon171 " + String.valueOf(sire_codon171));
		Log.i("addlamb", " sire codon154 " + String.valueOf(sire_codon154));
		Log.i("addlamb", " sire codon136 " + String.valueOf(sire_codon136));
		Log.i("addlamb", " dam codon171 " + String.valueOf(dam_codon171));
		Log.i("addlamb", " dam codon154 " + String.valueOf(dam_codon154));
		Log.i("addlamb", " dam codon136 " + String.valueOf(dam_codon136));
		Log.i("addlamb", " lamb codon171 " + String.valueOf(lamb_codon171));
		Log.i("addlamb", " lamb codon154 " + String.valueOf(lamb_codon154));
		Log.i("addlamb", " lamb codon136 " + String.valueOf(lamb_codon136));

    }
    public void calculatecodon171 (View v) {
	    //	Calculate codon171 value based on sire and dam if possible
	    //  In the current LambTracker Database the coding is set up like this
	    //  There is also a display order but not needed for here
	    //  id_codon171id   codon171alleles
	    //          1           QQ
	    //          2           Q?
	    //          3           QR
	    //          4           R?
	    //          5           RR
	    //          6           ??
	    //          7           HH
	    //          8           QH
	    //          9           H?
	    //          10          RH
	    //          11          KK
	    //          12          QK
	    //          13          RK
	    //          14          K?
	    //          15          HK
	    //  Although we can code for all the alleles the US only concerns itself with Q and R
	    //  Because current research is that H and K are like Q in terms of scrapie susceptibility
	    //  that is what we are testing for now.
	    //  Further research could allow for changes and the code can be modified then

	    if (dam_codon171 == 1) {
		    // Dam is QQ so test the sire with a case statement
		    Log.i("codon171 ", "Starting case statement for Dam is QQ");
		    switch (sire_codon171){
			    case 1:
				    //	we have dam is QQ and Sire is QQ so lamb is QQ
				    Log.i("codon171 ", "Case 1: dam is QQ and Sire is QQ so lamb is QQ");
				    lamb_codon171 = 1;
				    break;
			    case 2:
			    case 3:
			    case 4:
				    // 	Dam is QQ but sire is Q? or QR or R? so lamb is Q?
				    Log.i("codon171 ", "Case 2,3,4: Dam is QQ but sire is Q? or QR or R? so lamb is Q?");
				    lamb_codon171 = 2;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
				    break;
			    case 5:
				    //	Dam is QQ but sire is RR so lamb is QR
				    Log.i("codon171 ", "Case 5: Dam is QQ but sire is RR so lamb is QR");
				    lamb_codon171 = 3;
				    break;
			    case 6:
				    //	Dam is QQ but sire is ?? so lamb is Q?
				    Log.i("codon171 ", "Case 6: Dam is QQ but sire is ?? so lamb is Q?");
				    lamb_codon171 = 2;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
					break;
			    default:
				    //	We do not test for H or K alleles so the rest of the options are all set to do nothing
				    break;
		    }
	    }
	    if (dam_codon171 == 5) {
		    // Dam is RR so test the sire with a case statement
		    Log.i("codon171 ", "In case statement with Dam is RR");
		    switch (sire_codon171){
			    case 1:
				    //	we have Dam is RR and Sire is QQ so lamb is QR
				    Log.i("codon171 ", "Case 1: Dam is RR and Sire is QQ so lamb is QR");
				    lamb_codon171 = 3;
				    break;
			    case 2:
			    case 3:
			    case 4:
				    // 	Dam is RR but sire is Q? or QR or R? so lamb is R?
				    Log.i("codon171 ", "Case 2,3,4: Dam is RR but sire is Q? or QR or R? so lamb is R?");
				    lamb_codon171 = 4;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
				    break;
			    case 5:
				    //	Dam is RR and sire is RR so lamb is RR
				    Log.i("codon171 ", "Case 5: Dam is RR and sire is RR so lamb is RR");
				    lamb_codon171 = 5;
				    break;
			    case 6:
				    //	Dam is RR but sire is ?? so lamb is R?
				    Log.i("codon171 ", "Case 6: Dam is RR but sire is ?? so lamb is R?");
				    lamb_codon171 = 4;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
					break;
			    default:
				    //	We do not test for H or K alleles so the rest of the options are all set to do nothing
				    break;
		    }
	    }
	    if (dam_codon171 == 2 || dam_codon171 == 4) {
		    // Dam is Q? or R?
		    Log.i("codon171 ", "Dam is Q? or R? so test sire with case statement");
		    switch (sire_codon171){
			    case 1:
				    //	we have Dam is Q? or R? and Sire is QQ so lamb is Q?
				    Log.i("codon171 ", "Case 1: Dam is Q? or R? and Sire is QQ so lamb is Q?");
				    lamb_codon171 = 2;
				    //	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
				    break;
			    case 2:
			    case 3:
			    case 4:
				    // 	Dam is Q? or R? but sire is Q? or QR or R? so lamb is ??
				    Log.i("codon171 ", "Case 2,3,4: Dam is Q? or R? but sire is Q? or QR or R? so lamb is ??");
				    lamb_codon171 = 6;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
				    break;
			    case 5:
				    //	Dam is Q? or R? and sire is RR so lamb is R?
				    Log.i("codon171 ", "Case 5: Dam is Q? or R? and sire is RR so lamb is R?");
				    lamb_codon171 = 4;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
				    break;
			    case 6:
				    //	Dam is RR but sire is ?? so lamb is R?
				    Log.i("codon171 ", "Case 6: Dam is RR but sire is ?? so lamb is R?");
				    lamb_codon171 = 4;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
					break;
			    default:
				    //	We do not test for H or K alleles so the rest of the options are all set to do nothing
				    break;
		    }
	    }
	    if (dam_codon171 == 3) {
		    // Dam is QR
		    Log.i("codon171 ", "Dam is QR so test sire with case statement");
		    switch (sire_codon171){
			    case 1:
				    //	we have Dam is QR and Sire is QQ so lamb is Q?
				    Log.i("codon171 ", "Case 1: Dam is QR and Sire is QQ so lamb is Q?");
				    lamb_codon171 = 2;
				    //	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
				    break;
			    case 2: //sire is Q?
			    case 3: // sire is QR
			    case 4:	// sire is R?
				    // 	Dam is QR but sire is Q? or QR or R? so lamb is ??
				    Log.i("codon171 ", "Case 2,3,4: Dam is QR but sire is Q? or QR or R? so lamb is ??");
				    lamb_codon171 = 6;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
				    break;
			    case 5:
				    //	Dam is QR and sire is RR so lamb is R?
				    Log.i("codon171 ", "Case 5: Dam is QR and sire is RR so lamb is R?");
				    lamb_codon171 = 4;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
				    break;
			    case 6:
				    //	Dam is QR but sire is ?? so lamb is ??
				    Log.i("codon171 ", "Case 6: Dam is QR but sire is ?? so lamb is ??");
				    lamb_codon171 = 6;
				    // 	next need to set alert text to get scrapie blood for this lamb.
				    if (lamb_alert_text != null){
					    lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 171" + "\n" ;
				    }else{
					    //	Alert has null so just reset it to be the new one
					    lamb_alert_text = "Scrapie Blood Codon 171" + "\n" ;
				    }
					break;
			    default:
				    //	We do not test for H or K alleles so the rest of the options are all set to do nothing
				    break;
		    }
	    }
	    Log.i("after ", "Case statement Codon 171");
		Log.i("lamb_codon171 is  ",String.valueOf(lamb_codon171));
	}

	public void calculatecodon136 (View v) {
		// 		Start of Codon 136
		//	Calculate codon136 value based on sire and dam if possible
		//  In the current LambTracker Database the coding is set up like this
		//  There is also a display order but not needed for here
		//  id_codon136id   codon171alleles
		//          1           AA
		//          2           A?
		//          3           AV
		//          4           V?
		//          5           VV
		//          6           ??

		if (dam_codon136 == 1) {
			// Dam is QQ so test the sire with a case statement
			Log.i("codon136 ", "Starting case statement for Dam is AA");
			switch (sire_codon136){
				case 1:
					//	we have dam is AA and Sire is AA so lamb is AA
					Log.i("codon136 ", "Case 1: dam is AA and Sire is AA so lamb is QQ");
					lamb_codon136 = 1;
					break;
				case 2:
				case 3:
				case 4:
					// 	Dam is AA but sire is A? or AV or V? so lamb is A?
					Log.i("codon136 ", "Case 2,3,4: Dam is AA but sire is A? or AV or V? so lamb is A?");
					lamb_codon136 = 2;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				case 5:
					//	Dam is AA but sire is VV so lamb is AV
					Log.i("codon136 ", "Case 5: Dam is AA but sire is VV so lamb is AV");
					lamb_codon136 = 3;
					break;
				case 6:
					//	Dam is AA but sire is ?? so lamb is A?
					Log.i("codon136 ", "Case 6: Dam is AA but sire is ?? so lamb is A?");
					lamb_codon136 = 2;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				default:
					//	No other codons tested
					break;
			}
		}
		if (dam_codon136 == 5) {
			// Dam is VV so test the sire with a case statement
			Log.i("codon136 ", "In case statement with Dam is VV");
			switch (sire_codon136){
				case 1:
					//	we have Dam is VV and Sire is AA so lamb is AV
					Log.i("codon136 ", "Case 1: Dam is VV and Sire is AA so lamb is AV");
					lamb_codon136 = 3;
					break;
				case 2:
				case 3:
				case 4:
					// 	Dam is VV but sire is A? or AV or V? so lamb is V?
					Log.i("codon136 ", "Case 2,3,4: Dam is VV but sire is A? or AV or V? so lamb is V?");
					lamb_codon136 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				case 5:
					//	Dam is VV and sire is VV so lamb is VV
					Log.i("codon136 ", "Case 5: Dam is VV and sire is VV so lamb is VV");
					lamb_codon136 = 5;
					break;
				case 6:
					//	Dam is VV but sire is ?? so lamb is V?
					Log.i("codon136 ", "Case 6: Dam is VV but sire is ?? so lamb is V?");
					lamb_codon136 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				default:
					//	No other codons tested
					break;
			}
		}
		if (dam_codon136 == 2 || dam_codon136 == 4) {
			// Dam is A? or V?
			Log.i("codon136 ", "Dam is A? or V? so test sire with case statement");
			switch (sire_codon136){
				case 1:
					//	we have Dam is A? or V? and Sire is AA so lamb is A?
					Log.i("codon136 ", "Case 1: Dam is A? or V? and Sire is AA so lamb is A?");
					lamb_codon136 = 2;
					//	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				case 2:
				case 3:
				case 4:
					// 	Dam is A? or V? but sire is A? or AV or V? so lamb is ??
					Log.i("codon136 ", "Case 2,3,4: Dam is A? or V? but sire is A? or AV or V? so lamb is ??");
					lamb_codon136 = 6;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood";
					}
					break;
				case 5:
					//	Dam is A? or V? and sire is VV so lamb is V?
					Log.i("codon136 ", "Case 5: Dam is A? or V? and sire is VV so lamb is V?");
					lamb_codon136 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				case 6:
					//	Dam is VV but sire is ?? so lamb is V?
					Log.i("codon136 ", "Case 6: Dam is VV but sire is ?? so lamb is V?");
					lamb_codon136 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood";
					}
					break;
				default:
					//	No other codons tested
					break;
			}
		}
		if (dam_codon136 == 3) {
			// Dam is QR
			Log.i("codon136 ", "Dam is AV so test sire with case statement");
			switch (sire_codon136){
				case 1:
					//	we have Dam is AV and Sire is AA so lamb is A?
					Log.i("codon136 ", "Case 1: Dam is AV and Sire is AA so lamb is A?");
					lamb_codon136 = 2;
					//	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				case 2: //sire is A?
				case 3: // sire is AV
				case 4:	// sire is V?
					// 	Dam is AV but sire is A? or AV or V? so lamb is ??
					Log.i("codon136 ", "Case 2,3,4: Dam is AV but sire is A? or AV or V? so lamb is ??");
					lamb_codon136 = 6;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136";
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				case 5:
					//	Dam is AV and sire is VV so lamb is V?
					Log.i("codon136 ", "Case 5: Dam is AV and sire is VV so lamb is V?");
					lamb_codon136 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				case 6:
					//	Dam is AV but sire is ?? so lamb is ??
					Log.i("codon136 ", "Case 6: Dam is AV but sire is ?? so lamb is ??");
					lamb_codon136 = 6;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 136" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 136" + "\n" ;
					}
					break;
				default:
					//	No other codons tested
					break;
			}
		}
		Log.i("after ", "Case statement Codon 136");
		Log.i("lamb_codon136 is  ",String.valueOf(lamb_codon136));
	}
	public void calculatecodon154 (View v) {
// 		Start of Codon 154
		//	Calculate codon154 value based on sire and dam if possible
		//  In the current LambTracker Database the coding is set up like this
		//  There is also a display order but not needed for here
		//  id_codon136id   codon171alleles
		//          1           ??
		//          2           RR
		//          3           RH
		//          4           R?
		//          5           HH
		//          6           H?

		if (dam_codon154 == 1) {
			// Dam is ?? so test the sire with a case statement
			Log.i("codon154 ", "Starting case statement for Dam is ??");
			switch (sire_codon154){
				case 1:
//						we have Dam is ?? and Sire is ?? so lamb is ??
					Log.i("codon154 ", "Case 1: Dam is ?? and Sire is ?? so lamb is ??");
					lamb_codon154 = 1;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 2:
					// 	Dam is ?? and sire is RR so lamb is R?
					Log.i("codon154 ", "Case 2: Dam is ?? and sire is RR so lamb is R?");
					lamb_codon154 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 3:
				case 4:
					// 	Dam is ?? but sire is RH or R? so lamb is ??
					Log.i("codon154 ", "Case 3,4: Dam is RR but sire is RH or R? so lamb is ??");
					lamb_codon154 = 1;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 5:
					//	Dam is ?? but sire is HH so lamb is H?
					Log.i("codon154 ", "Case 5: Dam is ?? but sire is HH so lamb is H?");
					lamb_codon154 = 6;
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 6:
					//	Dam is ?? but sire is H? so lamb is ??
					Log.i("codon154 ", "Case 6: Dam is ?? but sire is H? so lamb is ??");
					lamb_codon154 = 1;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				default:
					//	No other codons tested
					break;
			}
		}
		if (dam_codon154 == 2) {
			// Dam is RR so test the sire with a case statement
			Log.i("codon154 ", "Starting case statement for Dam is RR");
			switch (sire_codon154){
				case 1:
					//	we have dam is RR and Sire is ?? so lamb is R?
					Log.i("codon154 ", "Case 1: dam is RR and Sire is ?? so lamb is R?");
					lamb_codon154 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 2:
					// 	Dam is RR and sire is RR so lamb is RR
					Log.i("codon154 ", "Case 2: Dam is RR and sire is RR so lamb is RR");
					lamb_codon154 = 2;
				case 3:
				case 4:
					// 	Dam is RR but sire is RH or R? so lamb is R?
					Log.i("codon154 ", "Case 3,4: Dam is RR but sire is RH or R? so lamb is R?");
					lamb_codon154 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 5:
					//	Dam is RR but sire is HH so lamb is HR
					Log.i("codon154 ", "Case 5: Dam is RR but sire is HH so lamb is HR");
					lamb_codon154 = 3;
					break;
				case 6:
					//	Dam is RR but sire is H? so lamb is R?
					Log.i("codon154 ", "Case 6: Dam is RR but sire is H? so lamb is R?");
					lamb_codon154 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				default:
					//	No other codons tested
					break;
			}
		}
		if (dam_codon154 == 5) {
			// Dam is HH so test the sire with a case statement
			Log.i("codon154 ", "In case statement with Dam is HH");
			switch (sire_codon154){
				case 1:
					//	we have Dam is HH and Sire is ?? so lamb is H?
					Log.i("codon154 ", "Case 1: Dam is HH and Sire is ?? so lamb is H?");
					lamb_codon154 = 6;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 2:
					// 	Dam is HH but sire is RR so lamb is HR
					Log.i("codon154 ", "Case 5: Dam is HH but sire is RR so lamb is HR");
					lamb_codon154 = 3;
					break;
				case 3:
				case 4:
					// 	Dam is HH but sire is R? or RH so lamb is H?
					Log.i("codon154 ", "Case 3,4: Dam is HH but sire is R? or RH so lamb is H?");
					lamb_codon154 = 6;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 5:
					//	Dam is HH and sire is HH so lamb is HH
					Log.i("codon154 ", "Case 5: Dam is HH and sire is HH so lamb is HH");
					lamb_codon154 = 5;
					break;
				case 6:
					//	Dam is HH but sire is H? so lamb is H?
					Log.i("codon154 ", "Case 6: Dam is HH but sire is H? so lamb is H?");
					lamb_codon154 = 6;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				default:
					//	No other codons tested
					break;
			}
		}
		if (dam_codon154 == 4 || dam_codon154 == 6) {
			// Dam is A? or V?
			Log.i("codon154 ", "Dam is H? or R? so test sire with case statement");
			switch (sire_codon154){
				case 1:
					//	we have Dam is H? or R?  and Sire is ?? so lamb is ??
					Log.i("codon154 ", "Case 1: Dam is H? or R?  and Sire is ?? so lamb is ??");
					lamb_codon154 = 1;
					//	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 2:
					//	we have Dam is H? or R?  and Sire is RR so lamb is R?
					Log.i("codon154 ", "Case 2: Dam is H? or R?  and Sire is RR so lamb is R?");
					lamb_codon154 = 4;
					//	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 3:
				case 4:
					// 	Dam is R? or H? but sire is R? so lamb is ??
					Log.i("codon154 ", "Case 3,4: Dam is R? or H? but sire is R? so lamb is ??");
					lamb_codon154 = 1;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 5:
					//	Dam is R? or H? and sire is HH so lamb is H?
					Log.i("codon154 ", "Case 5: Dam is R? or H? and sire is HH so lamb is H?");
					lamb_codon154 = 6;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 6:
					//	Dam is R? or H? but sire is H? so lamb is ??
					Log.i("codon154 ", "Case 6: Dam is R? or H? but sire is H? so lamb is ??");
					lamb_codon154 = 1;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				default:
					//	No other codons tested
					break;
			}
		}
		if (dam_codon154 == 3) {
			// Dam is RH
			Log.i("codon154 ", "Dam is RH so test sire with case statement");
			switch (sire_codon154){
				case 1:
					//	we have Dam is RH and Sire is ?? so lamb is ??
					Log.i("codon154 ", "Case 1: Dam is RH and Sire is ?? so lamb is ??");
					lamb_codon154 = 1;
					//	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 2: //sire is RR
					//	we have Dam is RH and Sire is RR so lamb is R?
					Log.i("codon154 ", "Case 2: Dam is RH and Sire is RR so lamb is R?");
					lamb_codon154 = 4;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;

				case 3: // sire is RH
				case 4:	// sire is R??
					// 	Dam is RH but sire is RH or R? so lamb is ??
					Log.i("codon154 ", "Case 3,4: Dam is RH but sire is RH or R? so lamb is ??");
					lamb_codon154 = 1;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 5:
					//	Dam is RH and sire is HH so lamb is H?
					Log.i("codon154 ", "Case 5: Dam is RH and sire is HH so lamb is H?");
					lamb_codon154 = 6;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				case 6:
					//	Dam is RH but sire is H? so lamb is ??
					Log.i("154 ", "Case 6: Dam is RH but sire is H? so lamb is ??");
					lamb_codon154 = 1;
					// 	next need to set alert text to get scrapie blood for this lamb.
					if (lamb_alert_text != null){
						lamb_alert_text = lamb_alert_text + "Scrapie Blood Codon 154" + "\n" ;
					}else{
						//	Alert has null so just reset it to be the new one
						lamb_alert_text = "Scrapie Blood Codon 154" + "\n" ;
					}
					break;
				default:
					//	No other codons tested
					break;
			}
		}
		Log.i("after ", "Case statement Codon 154");
		Log.i("lamb_codon154 is  ",String.valueOf(lamb_codon154));
	}

	public void filldefaults (View v){
		//	Fill all the misc variables for the sheep record
		//		Get defaults for gestation etc for this flock
		//	TODO
		//		Get who this flocks defaults are need to select the person whose flock this is here and then get their defaults
		// set breeder to Desert Weyr
		id_breederid = 1;
		cmd = String.format("select * from lambtracker_default_settings where id_contactsid = %s", id_breederid) ;
		crsr = dbh.exec( cmd );
		cursor   = ( Cursor ) crsr;
		dbh.moveToFirstRecord();
		//	Set the owner_id to be this flock and the lambing id to be this flock
		id_ownerid = dbh.getInt(1);
		lambing_contact = dbh.getInt(1);
		//	Set the flock_prefix to be this flock prefix
		flock_prefix = dbh.getInt(15);
		//	Set the birth_weight_units to be the default for this flock
		birth_weight_units = dbh.getInt(16);
		//	Set the gestation length for this flock
		early_gestation_length = dbh.getInt(17);
		late_gestation_length = dbh.getInt(18);
		//	Fill in the default tag colors for this flock
		farm_tag_color_male = dbh.getInt(6);
		farm_tag_color_female = dbh.getInt(7);
		eid_tag_color_male = dbh.getInt(2);
		eid_tag_color_female = dbh.getInt(3);
		federal_tag_color_male = dbh.getInt(8);
		federal_tag_color_female = dbh.getInt(9);
		paint_tag_color = dbh.getInt(21);
		//	fill in the default tag locations
		eid_tag_location = dbh.getInt(10);
		farm_tag_location = dbh.getInt(11);
		federal_tag_location = dbh.getInt(12);
		farm_tag_on_eid_tag = dbh.getInt(4); // This is a boolean 0 = false 1 = true
		farm_tag_number_digits_from_eid = dbh.getInt(5);
		LastEID = "000_000000000000";
//		//	Set breed based on sire and dam breed
//		// if different set breed to be Crossbred
//		//	TODO 5/14/20
//		// 		will need mods for flocks with crossbred in a different record i sheep_breed_table
//		Log.i("before ", "set breed based on sire and dam");
//		Log.i("add a lamb ", String.format(" dam breed is %d", dam_sheepbreed));
//		Log.i("add a lamb ", String.format(" sire breed is %d", sire_sheepbreed));
//		if (dam_sheepbreed == sire_sheepbreed){
//			lamb_sheepbreed = dam_sheepbreed;
//			Log.i("add a lamb ", String.format(" sheepbreedid is %d", lamb_sheepbreed));
//		}else{
//			lamb_sheepbreed = 2 ;
//		}
		// TODO: 5/3/19
		//		need to set a farm location record for this lamb that matches where the ewe is but no farm location
		//		data have been entered yet for any sheep
		//	    The following things should be modified to be the value from default settings
		//	    but I haven't implemented settings yet so hard coding them

		// Fill empty data that we don't know yet
		nsip_id = "";
		weaned_date= "";
		inbreeding= "";
		//	added this to set alerts and death date and death reason to empty so we don't get a false null
		lamb_alert_text = "";
		death_date = "";
		death_reason = "";
		lambease = 0;
		//	Set the lamb name to be year plus dam name until we change it
		lamb_name = Utilities.YearIs()+ "_" + dam_name + "_" + "lamb";
		Log.i("add a lamb ", " dam is " + dam_name);
		Log.i("add a lamb ", " lamb is " + lamb_name);
		//	Get the date and time to add to the lamb record these are strings not numbers
		mytoday = Utilities.TodayIs();
//		Log.i("add a lamb ", " today is " + mytoday);
		mytime = Utilities.TimeIs();
//		Log.i("add a lamb ", " time is " + mytime);
	}

	public void updateDatabase( View v ){
    	RadioGroup 	rg, rg2;
    	TextView 	TV;
  		TextView temp_tag_num, temp_tag_color, temp_tag_loc, temp_tag_type;
  		String tag_num;
  		int tag_color, tag_loc, tag_type, tag_flock;
  		Integer suck_data;
  		Integer temp_id;
		String blank_space;
  		String year = Utilities.YearIs();
  		TableLayout tl;
    	Object crsr;
    	Context context = this;

		blank_space = "";
		// Disable Update Database button and make it red to prevent getting 2 records at one time
    	btn = (Button) findViewById( R.id.update_database_btn );
    	btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
    	btn.setEnabled(false);
    	//	Get the data for this lamb
		//	Get the rear type
		Log.i("before rear type", " getting ready to get rear type ");
		rear_type_spinner = (Spinner) findViewById(R.id.rear_type_spinner);
		rear_type = rear_type_spinner.getSelectedItemPosition();
		Log.i("rear type", " spinner position is " + String.valueOf(rear_type));
		Log.i("rear_type ", String.valueOf(rear_type));
		if (rear_type == 0){
			//	Need to require a value for rearing type here Nothing selected to set default to be single
			// TODO Tried to get an alert here but can't have 2 in one activity 
//			title = getResources().getString(R.string.add_lamb_fill_title);
//			message = getResources().getString(R.string.add_lamb_fill_reartype);;
//			Utilities.alertDialogShow( context, title, message);
			// make update database button normal and enabled so we can try again
       		btn = (Button) findViewById( R.id.update_database_btn );
       		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
        	btn.setEnabled(true);
        	//	Defaulted to rear-type of 1 so the later query won't crash
			rear_type = 1;	        	
		}
			
  		//	Get the radio group selected for the sex
		Log.i("before radio group", " getting ready to get the sex ");
		rg=(RadioGroup)findViewById(R.id.radioGroupSex);
		sex = rg.getCheckedRadioButtonId();
		Log.i("sex radio button id ", String.valueOf(rg.getCheckedRadioButtonId()));		
		if (sex == -1){
			//	Set the sex to be unknown if not filled in
			sex_abbrev = "U";
			sex = 4;
		}else{
			sex = sex + 1;
			Log.i("sex ", String.valueOf(sex));
	 		cmd = String.format("select sex_table.sex_abbrev from sex_table " +
	 		"where sex_table.sex_sheepid = %s", sex);
	 		crsr = dbh.exec( cmd );  
			cursor   = ( Cursor ) crsr;
	  		dbh.moveToFirstRecord();
	  		sex_abbrev = dbh.getStr(0);	
		}			
		Log.i("sex abbrev ", sex_abbrev);
		//	Set the lamb name to be year plus dam name until we change it
		lamb_name = Utilities.YearIs()+ "_" + dam_name + "_" + "lamb";
		Log.i("add a lamb ", " dam is " + dam_name);
		Log.i("add a lamb ", " lamb is " + lamb_name);

		//	Get the value of the checkbox for stillborn
		Log.i("before checkbox", " getting ready to get stillborn or not ");
		stillbornbox = (CheckBox) findViewById(R.id.checkBoxStillborn);
		if (stillbornbox.isChecked()){
			//	Set the values for death dates for stillborn lambs.
			//	Set the remove reason to be the value for Died Stillborn
			stillborn = true;
			// hard coded for us needs work
			death_reason = "6";
			//	added rear_type 0 for stillborns
			rear_type = 0;
			Log.i("stillborn ", String.valueOf(stillborn));
			death_date = mytoday;
			lamb_name = Utilities.YearIs()+ "_" + dam_name + "_Stillborn";
			Log.i("add a lamb ", " dam is " + dam_name);
			Log.i("add a lamb ", " lamb is " + lamb_name);
		}

		//	Get the value of the checkbox for mark ewe
		Log.i("before checkbox", " getting ready to get mark ewe or not ");
		markewebox = (CheckBox) findViewById(R.id.checkBoxMarkEwe);
		if (markewebox.isChecked()){
			markewe = true;
			Log.i("markewe ", String.valueOf(markewe));
			// When we get to adding tags for the lamb we'll add a paint brand for ewe if there is one
			//	for the lamb.
		}else {
			markewe = false;
		}
		
		//	Get the Birth Weight
		Log.i("before weight", " getting ready to get birth weight ");
		TV = (TextView) findViewById(R.id.birth_weight);
		try {
			birth_weight = Float.valueOf(TV.getText().toString());
		} catch (Exception e) {
			//	set birth_weight to 0 because nothing filled in
			birth_weight = 0.0f;
		}	
		Log.i("birth_weight ", String.valueOf(birth_weight));
		
		//	Get the lambease score
		Log.i("before lambease", " getting ready to get lambease ");
		lamb_ease_spinner = (Spinner) findViewById(R.id.lamb_ease_spinner);
		lambease = lamb_ease_spinner.getSelectedItemPosition();
		Log.i("lambease", " spinner position is " + String.valueOf(lambease));
		lambease = lambeaseid.get(lambease);
		Log.i("lambease", " database ID for Lambease position is " + String.valueOf(lambease));
		if (lambease == 0){
			//	Need to require a value for lambease here
			//  Missing data so  display an alert
    		AlertDialog.Builder builder = new AlertDialog.Builder( this );
			Log.i("in alert", " need to get lambease ");
			builder.setMessage( R.string.add_lamb_fill_lambease )
			        .setTitle( R.string.add_lamb_fill_lambease );
    		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int idx) {
    	               	// User clicked OK button 
    	         		// make update database button normal and enabled so we can try again
    	           		btn = (Button) findViewById( R.id.update_database_btn );
    	           		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
    	            	btn.setEnabled(true);
    	               }
    	       });		
    		AlertDialog dialog = builder.create();
    		dialog.show();
    		return;
		}
		else {
			Log.i("lambease ", String.valueOf(lambease));
		}

		if (!stillborn) {
			// Set the management group based on sex of lamb
			// TODO: 2019-05-19
			// needs to change based on how the groups are set up hard coded for us
			if (sex == 1){management_group = 2;}
			if (sex == 2){management_group = 1;}
			if (sex == 4){management_group = 0;}
		} else {
			// If Stillborn clear out management_group
			management_group = 0;
			// If Stillborn clear out alerts
			lamb_alert_text = "";
		}
		Log.i("addlamb", " sire codon171 " + String.valueOf(sire_codon171));
		Log.i("addlamb", " sire codon154 " + String.valueOf(sire_codon154));
		Log.i("addlamb", " sire codon136 " + String.valueOf(sire_codon136));
		Log.i("addlamb", " dam codon171 " + String.valueOf(dam_codon171));
		Log.i("addlamb", " dam codon154 " + String.valueOf(dam_codon154));
		Log.i("addlamb", " dam codon136 " + String.valueOf(dam_codon136));
		Log.i("addlamb", " lamb codon171 " + String.valueOf(lamb_codon171));
		Log.i("addlamb", " lamb codon154 " + String.valueOf(lamb_codon154));
		Log.i("addlamb", " lamb codon136 " + String.valueOf(lamb_codon136));
		Log.i("add a lamb ", String.format(" lamb breed is %d", lamb_sheepbreed));
		Log.i("add a lamb ", "before insert cmd ");
		//	Ready to build the insert statement for this lamb.
		cmd = String.format("insert into sheep_table (sheep_name, id_flockprefixid, sex, " +
			"birth_date, birth_time, birth_type, birth_weight, rear_type, death_date, " +
			" lambease, sire_id, dam_id, alert01, sheep_birth_record, " +
			"codon171, codon154, codon136, nsip_id, id_sheepbreedid, birth_weight_units, " +
			"id_breederid, weaned_date, inbreeding, management_group " +
			", death_reason) values " +
			"('%s', %s, %s,'%s','%s', %s, %s, %s,'%s', %s, %s, %s,'%s', %s, %s,%s,%s,'%s',%s,%s, %s," +
			"'%s', '%s', %s, '%s') ",
			lamb_name, flock_prefix, sex, mytoday, mytime, birth_type, birth_weight, 
			rear_type, death_date, lambease, sire_id, dam_id,
			lamb_alert_text, lamb_birth_record,
			lamb_codon171, lamb_codon154, lamb_codon136, nsip_id, lamb_sheepbreed,
			birth_weight_units, id_breederid, weaned_date, inbreeding, management_group, death_reason );
		
		Log.i("add a lamb ", "cmd is " + cmd);
		dbh.exec(cmd);
		Log.i("add a lamb ", "after insert ");
		//  now we have a sheep record for the lamb. 
		//	We need to get the sheep_id of this lamb for use in the id, birth, ownership and location records
		cmd = String.format("select last_insert_rowid()");
		crsr = dbh.exec( cmd );  
		cursor   = ( Cursor ) crsr;
  		dbh.moveToFirstRecord();
  		lamb_id = dbh.getInt(0);		
		Log.i("add a lamb ", "the lamb_id is " + String.valueOf(lamb_id));
		
		// Set the flock ID to be nothing initially
		tag_flock = 0;
		
		//	Go get all the tag data for this lamb
		Log.i("before ", "getting the lamb tag info");
  		tl = (TableLayout) findViewById(R.id.tag_table);
  		Log.i("in table ", "after get the table view");
  		int i;
  		for (i = 0; i < tl.getChildCount(); i++){
  			Log.i("in for loop ", "after start" + String.valueOf(tl.getChildCount()));
	  		TableRow tr = (TableRow) tl.getChildAt(i);
	  		Log.i("in table ", "after get the table row");
	  		temp_tag_num = (TextView) tr.getChildAt (0);
	  		temp_tag_color = (TextView) tr.getChildAt(1);
	  		temp_tag_loc = (TextView) tr.getChildAt(2);
	  		temp_tag_type = (TextView) tr.getChildAt(3);
	  		Log.i("in table ", "after get the children of that row values");
	  		//	Get what tag number this is
	  		tag_num = temp_tag_num.getText().toString();
	  		//	Get the color for the tag
	  		tag_color_label = temp_tag_color.getText().toString();
	  		Log.i("before ", "getting tag color looking for " + tag_color_label);
	  		cmd = String.format("select tag_colors_table.id_tagcolorsid from tag_colors_table " +
	     				"where tag_color_name='%s'", tag_color_label);
	     	crsr = dbh.exec( cmd );
	  		cursor   = ( Cursor ) crsr;
	  		dbh.moveToFirstRecord();
	  		tag_color = dbh.getInt(0);
	  		Log.i("after ", "getting tag color");
	  		//	Get the location for the tag
	  		tag_location_label = temp_tag_loc.getText().toString();
	  		Log.i("before ", "getting tag location looking for " + tag_location_label);
	  		cmd = String.format("select id_location_table.id_locationid, id_location_table.id_location_abbrev from id_location_table " +
				"where id_location_abbrev='%s'", tag_location_label);
	  		crsr = dbh.exec( cmd );
	  		cursor   = ( Cursor ) crsr;
	  		dbh.moveToFirstRecord();
	  		tag_loc = dbh.getInt(0);
	  		//	Get what tag type it is
	  		tag_type_label = temp_tag_type.getText().toString(); 		
	  		cmd = String.format("select id_type_table.id_typeid from id_type_table " +
				"where idtype_name='%s'", tag_type_label);
	  		crsr = dbh.exec( cmd );
	  		cursor   = ( Cursor ) crsr;
	  		dbh.moveToFirstRecord();
	  		tag_type = dbh.getInt(0);
	  		Log.i("after ", "getting tag type" + tag_type_label );

	  		// Decided to try a switch statement here since the multiple if's are not working
	  		
	  		switch (tag_type){		
			case 1: //	Tag is a federal tag
				//	Set the flock to be the flock ID Should be a preference default not a fixed number
				lamb_name = lamb_name + "_" + tag_num;
				Log.i("add a lamb ", " lamb is " + lamb_name);
				tag_flock = 1;
	  			cmd = String.format("insert into id_info_table (sheep_id, tag_type, tag_color_male," +
		  				" tag_color_female, tag_location, tag_date_on, tag_number, id_flockid) values " +
		  				" (%s, %s, %s,%s,%s, '%s', '%s', %s) ", lamb_id, tag_type, tag_color, tag_color, 
		  				tag_loc, mytoday, tag_num, tag_flock);
		  		Log.i("add fed tag ", "db cmd is " + cmd);
				dbh.exec(cmd);
				Log.i("add fed tag ", "after insert into id_info_table");
				break;
			case 2:	//	Tag is an electronic tag 
					// test for USA official tag	
				tag_country_code = tag_num.substring(0, 3);
				Log.i("tag_country_code ", "is " + tag_country_code);
				if (tag_country_code.equals("840")){
					Log.i("official_id ", " supposed to be USA" );
					official_id = 1;
					}else 
					{official_id = 0;}
				cmd = String.format("insert into id_info_table (sheep_id, tag_type, tag_color_male," +
		  				" tag_color_female, tag_location, tag_date_on, tag_number, official_id) values " +
		  				" (%s, %s, %s,%s,%s, '%s', '%s', %s) ", lamb_id, tag_type, tag_color, tag_color, 
		  				tag_loc, mytoday, tag_num, official_id);
		  		Log.i("add tag to ", "db cmd is " + cmd);
				dbh.exec(cmd);
				Log.i("add tag ", "after insert into id_info_table");
				break;
			case 3:	// Tag is a paint brand
		  		if (markewe){
		  			// We have a paint mark for the lamb and the checkbox for the ewe is true
		  			// Go see if there is a current active paint mark for the ewe
		  			try {
		  				cmd = String.format("select sheep_id from id_info_table where sheep_id = '%s' and " +
			  					"tag_type = 3 and tag_date_off is null", dam_id);
		  				// if command works we have a tag already so break out
		  				Log.i("in try ewe tag ", "db cmd is " + cmd);
		  				crsr2 = dbh.exec( cmd );  
		  				Log.i("in try ewe tag ", "after execute db command" + cmd);
		  				cursor2   = ( Cursor ) crsr2;
		  		  		dbh.moveToFirstRecord();
		  		  		temp_id = dbh.getInt(0);
		  		  		Log.i("in try ewe tag ", "after try to read a record");
		  			}
		  				 catch (Exception e) {
		  					//	No record found so insert one 
		  					Log.i("in catch ", " need to add a ewe tag");
		  					cmd = String.format("insert into id_info_table (sheep_id, tag_type, tag_color_male," +
		  			  				" tag_color_female, tag_location, tag_date_on, tag_number) values " +
		  			  				" (%s, %s, %s,%s,%s, '%s', '%s') ", dam_id, tag_type, tag_color, tag_color, 
		  			  				tag_loc, mytoday, tag_num);
		  			  		Log.i("add ewe tag to ", "db cmd is " + cmd);
		  					dbh.exec(cmd);
		  					Log.i("add tag ", "after insert into id_info_table");		 
		  			} 	
		  		}
  				//	Add a lamb paint brand record for sure no matter what. 
  				cmd = String.format("insert into id_info_table (sheep_id, tag_type, tag_color_male," +
  		  				" tag_color_female, tag_location, tag_date_on, tag_number) values " +
  		  				" (%s, %s, %s,%s,%s, '%s', '%s') ", lamb_id, tag_type, tag_color, tag_color, 
  		  				tag_loc, mytoday, tag_num);
  		  		Log.i("add tag to ", "db cmd is " + cmd);
  				dbh.exec(cmd);
  				Log.i("add tag ", "after insert into id_info_table");
		  		break;
			case 4:	// Tag is a farm tag
				//	Set the lamb name to be the farm tag plus the year of birth
				lamb_name = lamb_name + "_" + tag_num;
				Log.i("add a lamb ", " lamb is " + lamb_name);
				cmd = String.format("insert into id_info_table (sheep_id, tag_type, tag_color_male," +
		  				" tag_color_female, tag_location, tag_date_on, tag_number) values " +
		  				" (%s, %s, %s,%s,%s, '%s', '%s') ", lamb_id, tag_type, tag_color, tag_color, 
		  				tag_loc, mytoday, tag_num);
		  		Log.i("add tag to ", "db cmd is " + cmd);
				dbh.exec(cmd);
				Log.i("add tag ", "after insert into id_info_table");
				break;
			case 5:	// Tag is a tattoo
				cmd = String.format("insert into id_info_table (sheep_id, tag_type, tag_color_male," +
		  				" tag_color_female, tag_location, tag_date_on, tag_number) values " +
		  				" (%s, %s, %s,%s,%s, '%s', '%s') ", lamb_id, tag_type, tag_color, tag_color, 
		  				tag_loc, mytoday, tag_num);
		  		Log.i("add tag to ", "db cmd is " + cmd);
				dbh.exec(cmd);
				Log.i("add tag ", "after insert into id_info_table");
				break;
			case 6:	// Tag is a split
				cmd = String.format("insert into id_info_table (sheep_id, tag_type, " +
		  				"  tag_location, tag_date_on, tag_number) values " +
		  				" (%s, %s, %s, '%s', '%s') ", lamb_id, tag_type, tag_loc, mytoday, tag_num);
		  		Log.i("add tag to ", "db cmd is " + cmd);
				dbh.exec(cmd);
				Log.i("add tag ", "after insert into id_info_table");
				break;
			case 7:	// Tag is a notch
				cmd = String.format("insert into id_info_table (sheep_id, tag_type, " +
						"  tag_location, tag_date_on, tag_number) values " +
						" (%s, %s, %s, '%s', '%s') ", lamb_id, tag_type, tag_loc, mytoday, tag_num);
				Log.i("add tag to ", "db cmd is " + cmd);
				dbh.exec(cmd);
				Log.i("add tag ", "after insert into id_info_table");
				break;
	  		}
  		}
		//	End of what has to loop through all IDs for the lamb being added 
		
		//	Now update the sheep record with the new sheep name
		Log.i("add a lamb ", " lamb is " + lamb_name);
		cmd = String.format("update sheep_table set sheep_name = '%s' " +
  		  		" where sheep_id = %s ", lamb_name, lamb_id);
		Log.i("add sheep_name ", "to db cmd is " + cmd);
		dbh.exec(cmd);
		Log.i("add name ", "after update into sheep_table");

		// add a location record for this lamb
		// TODO: 5/3/19
		// hard coded for our location needs to be a preference

	    if (stillborn){
		    // hard coded for our location
		    cmd = String.format("insert into sheep_location_history_table (sheep_id, movement_date, " +
				    " from_id_contactsid, to_id_contactsid) values " +
				    " (%s, '%s', '%s','%s') ", lamb_id, mytoday, "1", " " );
		    Log.i("add location to ", "db cmd is " + cmd);
		    dbh.exec(cmd);
		    Log.i("add tag ", "after add location");
	    }else{
		    cmd = String.format("insert into sheep_location_history_table (sheep_id, movement_date, " +
				    " from_id_contactsid, to_id_contactsid) values " +
				    " (%s, '%s', '%s','%s') ", lamb_id, mytoday, " ", "1");
			Log.i("add location to ", "db cmd is " + cmd);
			dbh.exec(cmd);
			Log.i(" not stillborn ", "after add location");
	    }

		// add an owner record for this lamb
		// hard coded for our location
		cmd = String.format("insert into sheep_ownership_history_table (sheep_id, transfer_date, " +
				" from_id_contactsid, to_id_contactsid, id_transferreasonid, sell_price, sell_price_units) values " +
				" (%s, '%s', '%s', %s, %s, '%s', '%s') ", lamb_id, mytoday, " ", 1 , 4, " ", " ");
		Log.i("add owner to ", "db cmd is " + cmd);
		dbh.exec(cmd);
		Log.i("add tag ", "after add owner");


		//	Get the radio group selected for the suck reflex
		Log.i("before radio group", " getting ready to get the suck data ");
		rg2=(RadioGroup)findViewById(R.id.radioGroupSuck);
		suck_data = rg2.getCheckedRadioButtonId();
		Log.i("suck radio button id ", String.valueOf(rg2.getCheckedRadioButtonId()));
		// put correct reference into variable
		//todo
		// hard coded for our suck data will need to be fixed for the general case
		//	set suck to not tested unless it's been explicitly changed
		// the button labels start at 0 so have to add one to get the case to work properly

		suck_data = suck_data + 1;
		Log.i("suck data is ", String.valueOf(suck_data));
		switch (suck_data) {
			case 1:
				suck_data = 24;
				break;
			case 2:
				suck_data = 25;
				break;
			case 3:
				suck_data = 26;
				break;
			case 4:
				suck_data = 28;
				break;
			case 5:
				suck_data = 27;
				break;
		}
		Log.i("suck data is ", String.valueOf(suck_data));

		// create a sheep evaluation for this lamb's suck data
		cmd = String.format("insert into sheep_evaluation_table (sheep_id, " +
						"trait_name01, trait_score01, trait_name02, trait_score02, trait_name03, trait_score03, " +
						"trait_name04, trait_score04, trait_name05, trait_score05, trait_name06, trait_score06," +
						"trait_name07, trait_score07, trait_name08, trait_score08, trait_name09, trait_score09, " +
						"trait_name10, trait_score10, trait_name11, trait_score11, trait_name12, trait_score12, " +
						"trait_name13, trait_score13, trait_name14, trait_score14, trait_name15, trait_score15, " +
						"trait_name16, trait_score16, trait_name17, trait_score17, trait_name18, trait_score18, " +
						"trait_name19, trait_score19, trait_name20, trait_score20, sheep_rank, number_sheep_ranked, " +
						"trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, eval_date, eval_time, age_in_days) " +
						"values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s," +
						"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'%s','%s', %s,%s,%s,%s,%s,'%s','%s',%s) ",
				lamb_id, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				37, suck_data, 0, 0, 0, 0, 0, 0, 0, 0,
				blank_space, blank_space,
				0, 0, 0, 0, 0, mytoday, mytime, 1);

		Log.i("save suck data eval ", cmd);
		dbh.exec( cmd );

		//	Create a lambing history record by first seeing if there is one already for this year
		//	If so then update the existing one
		//	If not then insert a new one
		
		// Make the year string able to be used in a query to get this year's lambing records. 
		year = year + "%";		
		Log.i("before try block ", " lambing year is " + year);
		
		try {
			//	We have a record so need to add this lamb and update the lambing_notes
			cmd = String.format("select * from lambing_history_table where lambing_history_table.dam_id = %s and " +
					" lambing_history_table.lambing_date like '%s' ", dam_id, year);
			Log.i("in try block ", " cmd is " + cmd);
			crsr = dbh.exec( cmd );
//			Log.i("in try block ", "after try the first DB select ");
			cursor   = ( Cursor ) crsr;
//			Log.i("in try block ", "after cursor ");
	  		dbh.moveToFirstRecord();
//	  		Log.i("in try block ", "after move to first ");
	  		lambing_historyid = dbh.getInt(0);
	  		Log.i("in try block ", "after get first variable ");
	  		lambing_date = dbh.getStr(1);
	  		lambing_time = dbh.getStr(10);
	  		lambing_notes = dbh.getStr(4);
	  		lambs_born = dbh.getInt(5);
	  		if (stillborn){
	  			lambing_notes = lambing_notes + "S";
	  		}else{
	  			lambing_notes = lambing_notes + sex_abbrev;
	  		}
	  		Log.i("add a lamb ", "the lambing_notes are " + lambing_notes);

	  		// now need to update the lambing_history_table
			lambs_born = lambs_born +1;
			Log.i("add a lamb ", "the number lambs born is " + String.valueOf(lambs_born));
			cmd = String.format("update lambing_history_table set " +
							"lambing_notes = '%s', lambs_born = %s " +
							"where id_lambinghistoryid = %s",
					lambing_notes, lambs_born,  lambing_historyid);
			Log.i("in try block ", " cmd is " + cmd);
			dbh.exec( cmd );
			cmd = String.format("update sheep_table set sheep_birth_record = %s," +
					"birth_type = %s, rear_type = %s , sheep_name = '%s' " +
					" where sheep_id = %s ", lambing_historyid, lambs_born, rear_type, lamb_name, lamb_id);
			dbh.exec( cmd );
			// after update lambs' rear type

			// now add the lamb to the lambs_born_table
			cmd = String.format("insert into lambs_born_table (id_lambinghistoryid, " +
							"sheep_id, birth_order, foster_eweid, hand_reared) " +
							"values (%s, %s, %s,'%s', %s) ",
					lambing_historyid, lamb_id, lambs_born, " ", 0);
			Log.i("in try block ", " cmd is " + cmd);
			dbh.exec(cmd);
			Log.i("in try block ", "after cmd to create a new record");

			// now go update all lambs to have the new birth_type
            cmd = String.format("select * from lambs_born_table where id_lambinghistoryid = %s", lambing_historyid);
            Log.i("in try block ", " cmd is " + cmd);
            crsr = dbh.exec( cmd );
            cursor   = ( Cursor ) crsr;
            Log.i("in try block ", "after cmd to get lambs for update");
            dbh.moveToFirstRecord();
	  		Log.i("in try block ", "after move to first ");
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
                temp_id = cursor.getInt(2);
                Log.i("try block", "lamb id is " + String.valueOf(temp_id));
                //	Now update the sheep record with the new birth type
                cmd = String.format("update sheep_table set birth_type = '%s' " +
                        " where sheep_id = %s ", lambs_born, temp_id);
                Log.i("add sheep_name ", "to db cmd is " + cmd);
                dbh.exec(cmd);
                Log.i("add name ", "after update into sheep_table");
            }
		} catch (Exception e) {
			//	No record found so insert one
			lambing_date = mytoday;
			lambing_time = mytime;
//			Update the lambs born a
			lambs_born = lambs_born + 1;
			Log.i("in catch block ", "need new record after setting date and time");
			if (stillborn) {
				lambing_notes = "S";
			} else {
				lambing_notes = sex_abbrev;
			}
			Log.i("in catch block ", " after setting lambing_notes " + lambing_notes);

			cmd = String.format("insert into lambing_history_table (lambing_date, dam_id, sire_id, " +
							"lambing_notes, lambs_born, lambing_time, gestation_length, lambing_contact, id_breedingid) " +
							"values ('%s', %s, %s,'%s', %s, '%s','%s', %s, %s) ",
					mytoday, dam_id, sire_id, lambing_notes, lambs_born, mytime, real_gestation_length, lambing_contact, correct_breeding_record);
			Log.i("in catch block ", " cmd is " + cmd);
			dbh.exec(cmd);
			Log.i("in catch block ", "after cmd to create a new record");
			cmd = String.format("select last_insert_rowid()");
			crsr = dbh.exec(cmd);
			cursor = (Cursor) crsr;
			dbh.moveToFirstRecord();
			lambing_historyid = dbh.getInt(0);
			Log.i("add a lamb ", "the lambing_historyid is " + String.valueOf(lambing_historyid));
			//	Now need to go back and add this birth record reference to the lamb record
			cmd = String.format("update sheep_table set sheep_birth_record = %s," +
					"birth_type = %s, rear_type = %s, sheep_name = '%s' " +
					" where sheep_id = %s ", lambing_historyid, lambs_born, rear_type, lamb_name, lamb_id);
			Log.i("in catch block ", " cmd is " + cmd);
			dbh.exec(cmd);
			// now add the lamb to the lambs_born_table
			cmd = String.format("insert into lambs_born_table (id_lambinghistoryid, " +
							"sheep_id, birth_order, foster_eweid, hand_reared) " +
							"values (%s, %s, %s,'%s', %s) ",
					lambing_historyid, lamb_id, "1", " ", 0);
			Log.i("in catch block ", " cmd is " + cmd);
			dbh.exec(cmd);
			Log.i("in catch block ", "after cmd to create a new record");
			}
		Log.i("addlamb", " sire codon171 " + String.valueOf(sire_codon171));
		Log.i("addlamb", " sire codon154 " + String.valueOf(sire_codon154));
		Log.i("addlamb", " sire codon136 " + String.valueOf(sire_codon136));
		Log.i("addlamb", " dam codon171 " + String.valueOf(dam_codon171));
		Log.i("addlamb", " dam codon154 " + String.valueOf(dam_codon154));
		Log.i("addlamb", " dam codon136 " + String.valueOf(dam_codon136));
		Log.i("addlamb", " lamb codon171 " + String.valueOf(lamb_codon171));
		Log.i("addlamb", " lamb codon154 " + String.valueOf(lamb_codon154));
		Log.i("addlamb", " lamb codon136 " + String.valueOf(lamb_codon136));

		// Enable Update Database button and make it normal 
    	btn = (Button) findViewById( R.id.update_database_btn );
    	btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
    	btn.setEnabled(true);

		//	done with this lamb so force back to the ewe screen
		backBtn(v);
    	
    }
    public void addNewTag( View v ){
    	Object crsr;   	
       	btn = (Button) findViewById( R.id.update_display_btn );
    	btn.setEnabled(true); 
    	new_tag_number = null;
       	// Fill the Tag Type Spinner
     	tag_type_spinner2 = (Spinner) findViewById(R.id.tag_type_spinner2);
    	tag_types = new ArrayList<String>();      	

		tag_types_id_order= new ArrayList<String>();
		tag_types_display_order = new ArrayList<String>();
		// Select All fields from id types to build the spinner
		cmd = "select * from id_type_table order by id_type_display_order";
		Log.i("fill tag spinner", "command is " + cmd);
		crsr = dbh.exec( cmd );
		tagcursor   = ( Cursor ) crsr;
		dbh.moveToFirstRecord();
		tag_types.add("Select a Type");
		tag_types_id_order.add("tag_types_id");
		tag_types_display_order.add("tag_type_display_order");
		Log.i("fill tag spinner", "added first option ");
		// looping through all rows and adding to list
		for (tagcursor.moveToFirst(); !tagcursor.isAfterLast(); tagcursor.moveToNext()){
			tag_types_id_order.add(tagcursor.getString(0));
			Log.i("addTag", "tag_types_id_order " + tagcursor.getString(0));
			tag_types.add(tagcursor.getString(1));
			Log.i("addTag", "tag_type name" + tagcursor.getString(1));
			tag_types_display_order.add(tagcursor.getString(3));
			Log.i("addTag", "tag_types_display_order " + tagcursor.getString(3));
		}

		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, tag_types);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tag_type_spinner2.setAdapter (dataAdapter);
		tag_type_spinner2.setSelection(2);
		Log.i("addTag", "after set tag type spinner" );
    	// Fill the Tag Color Spinner
    	tag_color_spinner = (Spinner) findViewById(R.id.tag_color_spinner);
     	tag_colors = new ArrayList<String>();
		tag_color_id_order = new ArrayList<String>();
		tag_color_display_order = new ArrayList<String>();

        // Select All fields from tag colors to build the spinner
        cmd = "select * from tag_colors_table order by tag_color_display_order";
        crsr = dbh.exec( cmd );  
        cursor   = ( Cursor ) crsr;
//        startManagingCursor(cursor);
    	dbh.moveToFirstRecord();
    	tag_colors.add("Select a Color");
		Log.i("add tag", " set first color " );
		tag_color_id_order.add("tag_color_id");
		tag_color_display_order.add("tag_color_display_order");
         // looping through all rows and adding to list

		Log.i("add tag", " before for loop " );
    	for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
    		tag_colors.add(cursor.getString(2));
			Log.i("addTag", "tag_colors " + cursor.getString(2));
			tag_color_id_order.add(cursor.getString(0));
			Log.i("addTag", "tag_color_id_order " + cursor.getString(0));
			tag_color_display_order.add(cursor.getString(3));
			Log.i("addTag", "tag_color_display_order " + cursor.getString(3));
    	}
		Log.i("add tag", " out of for loop " );
    	// Creating adapter for spinner
    	dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, tag_colors);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tag_color_spinner.setAdapter (dataAdapter);
		//	TODO fix to have different colors for male and female portion of tags
		//	Set the tag color to be the default should be from settings but not implemented yet.
		tag_color_spinner.setSelection(farm_tag_color_male); // set to defaults for farm tag
		Log.i("addTag", "after set tag color spinner" );

    	// Fill the Tag Location Spinner
		tag_location_spinner = (Spinner) findViewById(R.id.tag_location_spinner);
		tag_locations = new ArrayList<String>();        
		tag_locations.add("Select a Location");		
        // Select All fields from tag locations to build the spinner
        cmd = "select * from id_location_table";
        crsr = dbh.exec( cmd );  
        cursor   = ( Cursor ) crsr;
    	dbh.moveToFirstRecord();
    	
         // looping through all rows and adding to list
    	for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
    		tag_locations.add(cursor.getString(2));
    	}
    	// Creating adapter for spinner
    	dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, tag_locations);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tag_location_spinner.setAdapter (dataAdapter);
		//	TODO
		//	Set the tag location to be the right ear by default should be from settings
		tag_location_spinner.setSelection(farm_tag_location); //
		Log.i("addTag", "after set tag location spinner" );
    }

	private void addRadioButtons(int numButtons, String[] radioBtnText, int whichgroup ){
	  	  int i;
		Log.i("addradiobuttons", " start of add radio buttons");
		Log.i("addradiobuttons", String.valueOf(numButtons)	);
	  	  for(i = 0; i < numButtons; i++){
	  	    //instantiate...
	  	    RadioButton radioBtn = new RadioButton(this);

	  	    //set the values that you would otherwise hardcode in the xml...
			  Log.i("addradiobuttons", " set the xml layout ");
	  	  	radioBtn.setLayoutParams 
	  	      (new LayoutParams 
	  	      (LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

	  	    //label the button...
			  Log.i("addradiobuttons", " do the labels for the buttons " );
			  Log.i("addradiobuttons", String.valueOf(i));
			  Log.i("addradiobuttons", radioBtnText[i]);
	  	  	radioBtn.setText(radioBtnText[i]);
	  	  	Log.i("addradiobuttons", radioBtnText[i]);
	  	  	radioBtn.setId(i);

	  	    //add it to the group.
			  Log.i("addradiobuttons", " add button to the radio group");
			  if ( whichgroup == 1 ){
				  radioGroup.addView(radioBtn, i);
			  }else{
			  	radioGroup2.addView(radioBtn, i);
			  };
	  	  }
		Log.i("addradiobuttons", " out of add radio buttons");
	  	}


	public void helpBtn( View v )
    {
   	// Display help here   	
		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		builder.setMessage( R.string.help_add_lamb )
	           .setTitle( R.string.help_warning );
		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int idx) {
	               // User clicked OK button 
	        	  
	    		   clearBtn( null );
	               }
	       });		
		AlertDialog dialog = builder.create();
		dialog.show();		
    }
    
    // user clicked the 'back' button
    public void backBtn( View v )
	    {
    	doUnbindService();
		stopService(new Intent(AddLamb.this, eidService.class)); 
		Log.i("in add lamb", " in back btn after stop EID service");
		clearBtn( null );
		// Added this to close the database if we go back to the lambing activity  	
    	cursor.close();
    	Log.i("in add lamb", " in back btn after close cursor");
    	dbh.closeDB();   	
    	//Go back to ewe lambing data
    	Log.i("in add lamb", " before finish this activity");
      	finish();
      	Log.i("in add lamb", " after finish this activity");
	    }
 
    public void showAlert (View v){    		
    		// Display alerts here   	
    				AlertDialog.Builder builder = new AlertDialog.Builder( this );
    				Log.i("ShowAlert", "Alert Text is" + alert_text);
    				builder.setMessage( alert_text )
    			           .setTitle( R.string.alert_warning );
    				builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
    			           public void onClick(DialogInterface dialog, int idx) {
    			               // User clicked OK button   	  
    			               }
    			       });		
    				AlertDialog dialog = builder.create();
    				dialog.show();    		
    	}
   
    // user clicked 'clear' button
    public void clearBtn( View v )
	    {
		TextView TV ;
		RadioGroup rg;
		CheckBox cb;
		TableLayout tl;
  		
		TV = (TextView) findViewById( R.id.inputText );
		TV.setText( "" );		
		TV = (TextView) findViewById( R.id.sheepnameText );
		TV.setText( "" );
		//	Clear the radio group checks
//		Log.i("in add lamb", "in clear button ready to clear the radio groups ");
//		rg=(RadioGroup)findViewById(R.id.radioRearType);
//		rg.clearCheck();
		rg=(RadioGroup)findViewById(R.id.radioGroupSex);
		rg.clearCheck();
		Log.i("in add lamb", " in clear btn resetting the stillborn checkbox");
		//	Clear the stillborn box
		cb=(CheckBox)findViewById(R.id.checkBoxStillborn);
		cb.setChecked(false);
		TV = (TextView) findViewById( R.id.birth_weight);
		TV.setText( "" );
		lamb_ease_spinner = (Spinner) findViewById(R.id.lamb_ease_spinner);
		lamb_ease_spinner.setSelection(0);
     	tag_type_spinner2 = (Spinner) findViewById(R.id.tag_type_spinner2);
      	tag_color_spinner = (Spinner) findViewById(R.id.tag_color_spinner);
      	tag_location_spinner = (Spinner) findViewById(R.id.tag_location_spinner);
      	TV  = (TextView) findViewById( R.id.new_tag_number);
      	tag_type_spinner2.setSelection(0);
      	tag_color_spinner.setSelection(0);
      	tag_location_spinner.setSelection(0);
      	TV.setText( "" );	
      	tl = (TableLayout) findViewById(R.id.tag_table);
      	tl.removeAllViews();
      	Log.i("in add lamb", " in clear btn after removing the table views");
    }  
    public void doNote( View v )
    {	 
    	Utilities.takeNote(v, thissheep_id, this);
    }

//   user clicked 'Scan' button    
  public void scanEid( View v){
  	// Here is where I need to get a tag scanned and put the data into the variable LastEID
		if (mService != null) {
		try {
			//Start eidService sending tags
			Message msg = Message.obtain(null, eidService.MSG_SEND_ME_TAGS);
			msg.replyTo = mMessenger;
			mService.send(msg);
		   	//	make the scan eid button green
	    	Button btn = (Button) findViewById( R.id.scan_eid_btn );
	    	btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));
		} catch (RemoteException e) {
			// In this case the service has crashed before we could even do anything with it
		}
		}    	    	
  }
 
  public void updateTag( View v ){
  	Object 			crsr;
  	String 			cmd;
  	TextView 		TV;
  	Boolean 	tagok;
  	//	Initially set tag to not ok until we verify tag entry is good
  	tagok = false;
  	// Get the data from the add tag section of the screen
  	tag_type_spinner2 = (Spinner) findViewById(R.id.tag_type_spinner2);
  	tag_color_spinner = (Spinner) findViewById(R.id.tag_color_spinner);
  	tag_location_spinner = (Spinner) findViewById(R.id.tag_location_spinner);
  	
  	tag_type_label = tag_type_spinner2.getSelectedItem().toString();
  	Log.i("updateTag", "Tag type is " + tag_type_label);
  	tag_color_label = tag_color_spinner.getSelectedItem().toString();
  	Log.i("updateTag", "Tag color is " + tag_color_label);
  	tag_location_label = tag_location_spinner.getSelectedItem().toString();
  	Log.i("updateTag", "Tag location is " + tag_location_label);
  	
  	TV  = (TextView) findViewById( R.id.new_tag_number);
  	new_tag_number = TV.getText().toString();
  	Log.i("before if", " new tag number " + new_tag_number);    	
   	if (tag_type_label == "Select a Type" || tag_location_label == "Select a Location" || tag_color_label == "Select a Color"
  			|| TV.getText().toString().isEmpty()) {
  		new_tag_type = 0;
  		// Missing data so  display an alert 	
  		AlertDialog.Builder builder = new AlertDialog.Builder( this );
  		builder.setMessage( R.string.convert_fill_fields )
  	           .setTitle( R.string.enter_tag_data );
  		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
  	           public void onClick(DialogInterface dialog, int idx) {
  	               // User clicked OK button 
   	    		   return;
  	               }
  	       });		
  		AlertDialog dialog = builder.create();
  		dialog.show();		   		
  	}else
  	{
  		Log.i("before ", "getting tag type looking for " + tag_type_label);
  		cmd = String.format("select id_type_table.id_typeid from id_type_table " +
			"where idtype_name='%s'", tag_type_label);
  		crsr = dbh.exec( cmd );
  		cursor   = ( Cursor ) crsr;
  		dbh.moveToFirstRecord();
  		new_tag_type = dbh.getInt(0);
  		Log.i("after ", "getting tag type" + String.valueOf(new_tag_type));  		
     	
  		Log.i("before ", "getting tag color looking for " + tag_color_label);
  		cmd = String.format("select tag_colors_table.id_tagcolorsid from tag_colors_table " +
     				"where tag_color_name='%s'", tag_color_label);
     	crsr = dbh.exec( cmd );
  		cursor   = ( Cursor ) crsr;
  		dbh.moveToFirstRecord();
  		new_tag_color = dbh.getInt(0);
  		Log.i("after ", "getting tag color" + String.valueOf(new_tag_color));
  		
  		Log.i("before ", "getting tag location looking for " + tag_location_label);
  		cmd = String.format("select id_location_table.id_locationid, id_location_table.id_location_abbrev from id_location_table " +
			"where id_location_abbrev='%s'", tag_location_label);
  		crsr = dbh.exec( cmd );
  		cursor   = ( Cursor ) crsr;
  		dbh.moveToFirstRecord();
  		new_tag_location = dbh.getInt(0);
  		Log.i("New Location ID ", String.valueOf(new_tag_location));
   		tag_location_label = dbh.getStr(1);
  		Log.i("New Location ", tag_location_label);
  		
  		//	Need to test if the type is an ear tag and the location is not an ear then must request change
  		switch (new_tag_type){		
		case 1:
		case 2:
		case 4:		
		case 6:
		case 7:
			// Tag Type is Federal, Electronic, Farm, Split or Notch so require an ear location
			switch (new_tag_location){
			case 1:
			case 2:
				// Ear locations so ok
				tagok = true;
				break;
			case 3:
			case 4:
			case 5:
				// flank or side not allowed for tags, split or notches
				tagok = false;
		  		break;
			}
			break;
		case 3:
			// paint brand only location allowed is side
			switch (new_tag_location){
			case 1:
			case 2:
			case 3:
			case 4:
				// Ear and flank locations not allowed
				tagok = false;
		  		break;
			case 5:
				// side so ok
				tagok = true;
		  		break;
			}
			break;
		case 5:
			// tattoo so all except side
			switch (new_tag_location){
			case 1:
			case 2:
			case 3:
			case 4:
				// Ear and flank locations ok
				tagok = true;
		  		break;
			case 5:
				// side not allowed
				tagok = false;
		  		break;
			}
			break;
  		}
  		if (tagok) {
  			// all tag data is ok so can put in the table
  	  		Log.i("Before tag ", "Before creating the tag table layout");
  	  		TableLayout tl;
  	  		TableRow tr;
  	  		tl = (TableLayout) findViewById(R.id.tag_table);
  	  		tr = new TableRow (this);

  	  		TV = new TextView(this);
  	   		TV.setText (new_tag_number);
  	   		TV.setWidth(200);
  	   		tr.addView(TV);

  	   		Log.i("Before ", "setting next text view TV2");  		
  	   		TV = new TextView(this);
  	  		TV.setText (tag_color_label);
  	  		TV.setWidth(75);
  	  		tr.addView(TV); 		
  	 
  	   		TV = new TextView(this);
  	   		TV.setText (tag_location_label);
  	   		TV.setWidth(50);
  	  		tr.addView(TV);

  	  		TV = new TextView(this);
  	  		TV.setText (tag_type_label); 
  	  		TV.setWidth(90);
  	  		tr.addView(TV);
  	 
  	  		tl.addView(tr);
  	  		Log.i("after tag ", "after creating the tag table layout");
  	  		if ((new_tag_type == 1) || (new_tag_type == 4)){
  	  			//	tag type is either Federal or Farm so set defaults to be paint, yellow on the side
  	  			//	TODO should be set via preferences or settings
  	 	      	tag_type_spinner2 = (Spinner) findViewById(R.id.tag_type_spinner2);
  	  	      	tag_color_spinner = (Spinner) findViewById(R.id.tag_color_spinner);
  	  	      	tag_location_spinner = (Spinner) findViewById(R.id.tag_location_spinner);
  	  	      	TV  = (TextView) findViewById( R.id.new_tag_number);
				Log.i("New tag is ", new_tag_number);
  	  	      	tag_type_spinner2.setSelection(6);	// Paint type
  	  	      	tag_color_spinner.setSelection(paint_tag_color);	//
  	  	      	tag_location_spinner.setSelection(5);	// Side 
  	  	      	TV.setText( "" ); 	  			
  	  		}else{
  	  			//	tag type is something else like electronic, tattoo or paint so set defaults for a farm tag
  	  			//	TODO fix to add male and female tag colors
  	  			//	Needs to be fixed to be defaults or settings. 
	   	      	tag_type_spinner2 = (Spinner) findViewById(R.id.tag_type_spinner2);
	  	      	tag_color_spinner = (Spinner) findViewById(R.id.tag_color_spinner);
	  	      	tag_location_spinner = (Spinner) findViewById(R.id.tag_location_spinner);
	  	      	TV  = (TextView) findViewById( R.id.new_tag_number);
				Log.i("New tag is ", new_tag_number + " before set farm ");
	  	      	tag_type_spinner2.setSelection(2);	// Farm type
	  	      	tag_color_spinner.setSelection(farm_tag_color_male);
	  	      	tag_location_spinner.setSelection(farm_tag_location);
	  	      	if (farm_tag_on_eid_tag == 1){
	  	      		//base the farm tag number on the eid tag number
					//get the number of digits to use
					new_tag_number = LastEID.substring (16-farm_tag_number_digits_from_eid); // last x digits of previous EID tag
				}else{
					new_tag_number = TV.getText().toString();
				}
				Log.i("New tag is ", new_tag_number + " after set farm ");
	  	      	TV.setText( new_tag_number);
  	  		}
  		}else{
  			//	bad tag location to ask for a new one
	  		AlertDialog.Builder builder = new AlertDialog.Builder( this );
	  		builder.setMessage( R.string.wrong_id_location )
	  	           .setTitle( R.string.enter_tag_data );
	  		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
	  	           public void onClick(DialogInterface dialog, int idx) {
	  	               // User clicked OK button 
	   	    		   return;
	  	               }
	  	       });		
	  		AlertDialog dialog = builder.create();
	  		dialog.show();
  			
  		}
  	
      	}
   	}
}
