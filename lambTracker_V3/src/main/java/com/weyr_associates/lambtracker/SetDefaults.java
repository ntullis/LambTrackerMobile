package com.weyr_associates.lambtracker;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class SetDefaults extends Activity {
	private DatabaseHandler dbh;
	public Cursor 	cursor, cursor2, cursor3, cursor4;
	public Object	crsr;
	public Spinner eid_tag_type_spinner, eid_tag_location_spinner, eid_tag_male_color_spinner , eid_tag_female_color_spinner ;
	public Spinner federal_tag_type_spinner, federal_tag_location_spinner, federal_tag_male_color_spinner , federal_tag_female_color_spinner ;
	public Spinner farm_tag_type_spinner, farm_tag_location_spinner,  farm_tag_male_color_spinner , farm_tag_female_color_spinner  ;
	public Spinner flock_spinner, breed_spinner, registry_spinner ;
	public List<String> tag_types, tag_locations, tag_colors;
	ArrayAdapter<String> dataAdapter;
	public String cmd;
	public Button btn;
	public List<String> flock_name_list, registry_name_list, breed_name_list;
	public List<Integer> flock_id_list, registry_id_list, breed_id_list;
	public SimpleCursorAdapter myadapter;
	public RadioGroup radioGroup, radioGroup2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.set_defaults);
		Log.i("SetDefaults", " after set content view");
		String dbname = getString(R.string.real_database_file);
		dbh = new DatabaseHandler( this, dbname );
		Log.i("database is ",  dbname );

		// Disable Save defaults button and make it red to prevent getting 2 records at one time
		btn = (Button) findViewById( R.id.save_defaults_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
		btn.setEnabled(false);

		// Fill the Flock Owner Spinner
		Log.i("in create ",  " ready to get owner list" );
		cmd = "select id_contactsid, contact_first_name, contact_middle_name, contact_last_name " +
				",contact_company, contact_physical_city from contacts_table order by contact_last_name ";
		Log.i("fill owner spinner ", "command is " + cmd);
		crsr = dbh.exec(cmd);
		Log.i("in create ", "after execute comand");
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		flock_spinner = (Spinner) findViewById(R.id.flock_spinner);
		flock_name_list  = new ArrayList<String>();
		flock_id_list = new ArrayList<Integer>();
		flock_name_list.add("Select Your Flock");
		flock_id_list.add(0);
		Log.i("fill owner spinner ", "added first option ");
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			flock_id_list.add(cursor.getInt(0));
			Log.i("setDefaults", "owner id order " + cursor.getString(0));
			flock_name_list.add(cursor.getString(1) + " " + cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4) + " " + cursor.getString(5));
			Log.i("setDefaults", "owner_name " + cursor.getString(1) + " " + cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4) + " " + cursor.getString(5));
		}
		// Creating adapter for spinner
		Log.i("before array adapters ", "first one ");
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, flock_name_list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		flock_spinner.setAdapter(dataAdapter);
		Log.i("in create ", " after flock list array adapter ");
		//	set initial to be selection message
		flock_spinner.setSelection(0);

		// Fill the Registry Spinner
		Log.i("in create ",  " ready to get registry list" );
		cmd = "select id_contactsid, contact_first_name, contact_middle_name, contact_last_name " +
				",contact_company, contact_physical_city from contacts_table order by contact_last_name ";
		Log.i("fill registry spinner ", "command is " + cmd);
		crsr = dbh.exec(cmd);
		Log.i("in create ", "after execute comand");
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		registry_spinner = (Spinner) findViewById(R.id.registry_spinner);
		registry_name_list  = new ArrayList<String>();
		registry_id_list = new ArrayList<Integer>();
		registry_name_list.add("Select Your Flock");
		registry_id_list.add(0);
		Log.i("fill registry spinner ", "added first option ");
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			registry_id_list.add(cursor.getInt(0));
			Log.i("setDefaults", "registry id order " + cursor.getString(0));
			registry_name_list.add(cursor.getString(1) + " " + cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4) + " " + cursor.getString(5));
			Log.i("setDefaults", "registry name  " + cursor.getString(1) + " " + cursor.getString(2) + " " + cursor.getString(3) + " " + cursor.getString(4) + " " + cursor.getString(5));
		}
		// Creating adapter for spinner
		Log.i("before array adapters ", "first one ");
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, registry_name_list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		registry_spinner.setAdapter(dataAdapter);
		Log.i("in create ", " after registry list array adapter ");
		//	set initial to be selection message
		registry_spinner.setSelection(0);

		// Fill the breed Spinner
		// TODO: 2020-05-23
		//	This needs to be after the registry has been selected.
		//	currently defaulted to Black Welsh
		Log.i("in create ",  " ready to get breed list" );
		cmd = "select id_sheepbreedid, sheep_breed_name, sheep_breed_display_order from sheep_breed_table " +
				"where registry_id_contactsid = 25 " +
				"order by sheep_breed_display_order ";
		Log.i("fill breed spinner ", "command is " + cmd);
		crsr = dbh.exec(cmd);
		Log.i("in create ", "after execute comand");
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		breed_spinner = (Spinner) findViewById(R.id.breed_spinner);
		breed_name_list  = new ArrayList<String>();
		breed_id_list = new ArrayList<Integer>();
		breed_name_list.add("Select Primary Breed");
		breed_id_list.add(0);
		Log.i("fill breed spinner ", "added first option ");
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			breed_id_list.add(cursor.getInt(0));
			Log.i("setDefaults", "registry id order " + cursor.getString(0));
			breed_name_list.add(cursor.getString(1) );
			Log.i("setDefaults", "registry name  " + cursor.getString(1) );
		}
		// Creating adapter for spinner
		Log.i("before array adapters ", "first one ");
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, breed_name_list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		breed_spinner.setAdapter(dataAdapter);
		Log.i("in create ", " after registry list array adapter ");
		//	set initial to be selection message
		breed_spinner.setSelection(0);
	}
	public void saveDefaults( View v ) {
		TextView TV;
		String temp_string;
		Log.i("in save ", " defaults stub ");
		//	set initial to be selection message
	}
	private void addRadioButtons(int numButtons, String[] radioBtnText) {
		int i;

		for(i = 0; i < numButtons; i++){
			//instantiate...
			RadioButton radioBtn = new RadioButton(this);

			//set the values that you would otherwise hardcode in the xml...
			radioBtn.setLayoutParams
					(new LinearLayout.LayoutParams
							(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

			//label the button...
			radioBtn.setText(radioBtnText[i]);
//			  	  	Log.i("addradiobuttons", radioBtnText[i]);
			radioBtn.setId(i);

			//add it to the group.
			radioGroup.addView(radioBtn, i);
		}
	}

	public void backBtn( View v )
	{
		// Added this to close the database if we go back to the main activity
		try {
			cursor.close();
		}catch (Exception r)
		{
			Log.i("back btn", "cursor RunTimeException: " + r);
		}
		dbh.closeDB();
		//Go back to main
		this.finish();
	}

	public void helpBtn( View v )
	{
		// Display help here
		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		builder.setMessage( R.string.help_management )
				.setTitle( R.string.help_warning );
		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int idx) {
				// User clicked OK button

			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
